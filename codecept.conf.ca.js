const commonConf = require('./commonConf.js');
commonConf.include.data = "./test/acceptance/testDataCA.js";

var ALLURE_REPORT_PATH = commonConf.allureReportDir();

exports.config = {
  output: './test/acceptance/reports',
  helpers: {
    WebDriver: {
      url: commonConf.environment.stageCAURL,

      //browser: 'safari',
      //desiredCapabilities: commonConf.caps_macOSX_monterey_safari,

      browser: 'chrome',
      desiredCapabilities: commonConf.caps_win11_chrome_latest,

      smartWait: 5000,
      waitForTimeout: 30000,
      windowSize: 'maximize',
    },
    ChaiWrapper: {
      require: "codeceptjs-chai"
    },
    WebDriverHelper: {
      require: './test/acceptance/helpers/customHelperCA.js'
    }
  },
  include: commonConf.include,
  mocha: {},
  bootstrap: null,
  timeout: null,
  teardown: null,
  hooks: [],
  gherkin: {
    features: './test/acceptance/features/**/*.feature',
    // features: ['./test/acceptance/features/productDetailsPage/*.feature',
    //   './test/acceptance/features/productListPage/*.feature',
    //   './test/acceptance/features/staticPage/*.feature'],
    steps: commonConf.gherkin_steps
  },
  plugins: {
    wdio: {
      enabled: true,
      // services: ['selenium-standalone'],
      services: ['browserstack'],
      user: process.env.BROWSERSTACK_USERNAME || 'taushifahamad_QhjVH3',
      key: process.env.BROWSERSTACK_ACCESS_KEY || 'zHvApcqRsLJNXCzQDWNU'
    },
    tryTo: {
      enabled: true
    },
    retryTo: {
      enabled: true,
      pollInterval: 500,
    },
    retryFailedStep: {
      enabled: true,
      retries: 2
    },
    allure: {
      enabled: true,
      require: '@codeceptjs/allure-legacy',
      outputDir: ALLURE_REPORT_PATH
    }
  },
  // tests: './test/acceptance/tests/**/*_test.js',
  name: 'tumi-sfcc-tests'
}