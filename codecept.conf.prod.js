const commonConf = require('./commonConf.js');

var ALLURE_REPORT_PATH = commonConf.allureReportDirProd();

exports.config = {
  output: './test/prod/failed-screenshots',
  helpers: {
    WebDriver: {
      url: 'https://tumi.com/?no-geo',

      // browser: 'safari',
      // desiredCapabilities: commonConf.macVentura_safari_latest_for_prod,

      browser: 'chrome',
      desiredCapabilities: commonConf.macVentura_chrome_latest_for_prod,

      // desiredCapabilities: {
      //   "chromeOptions": {
      //     "args": ['--user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 13_0_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36 TumiCustomUA']
      //   }
      // },

      smartWait: 5000,
      waitForTimeout: 30000,
      windowSize: 'maximize',
    },
    ChaiWrapper: {
      require: "codeceptjs-chai"
    },
    WebDriverHelper: {
      require: './test/prod/helpers/customProdHelper.js'
    }
  },
  include: commonConf.include,
  mocha: {},
  bootstrap: null,
  timeout: null,
  teardown: null,
  hooks: [],
  gherkin: {
    features: './test/prod/features/*.feature',
    steps: commonConf.gherkin_steps
  },
  plugins: {
    wdio: {
      enabled: true,
      // services: ['selenium-standalone'],
      services: ['browserstack'],
      user: process.env.BROWSERSTACK_USERNAME || 'taushifahamad_QhjVH3',
      key: process.env.BROWSERSTACK_ACCESS_KEY || 'zHvApcqRsLJNXCzQDWNU'
    },
    tryTo: {
      enabled: true
    },
    retryTo: {
      enabled: true,
      pollInterval: 500,
    },
    retryFailedStep: {
      enabled: true,
      retries: 2
    },
    allure: {
      enabled: true,
      require: '@codeceptjs/allure-legacy',
      outputDir: ALLURE_REPORT_PATH
    }
  },
  // tests: './test/acceptance/tests/**/*_test.js',
  name: 'tumi-sfcc-tests'
}