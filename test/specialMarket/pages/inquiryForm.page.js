const { I, testDataSpecialMarket, utilities } = inject();
module.exports = {
    locators: {
        addToInquiry: "(//button[contains(@class,'add-to-cart')])[1]",
        cartFlyout: ".cart-flyout-heading",
        proceedToInquiry: "//div[@id='flyout-minicart']//a[contains(@class,'button--primary')]",
        inqDetailsFullName: "#inquiry-name",
        inqDetailsEmail: "#inquiry-email",
        inqDetailsPhNum: "#inquiry-phone",
        inqDetailsPriceRange: "#inquiry-price",
        inqDetailsQuantity: "#inquiry-quantity",
        inqDetailsDate: "#inquiry-date",
        inqDetailsRepresentative: "//select[@id='inquiry-representative']//option",
        inqDetailsCustomizationYes: "//div[contains(@class,'inquiry-customize')]/div/div/label[1]",
        inqDetailsCustomizationNo: "//div[contains(@class,'inquiry-customize')]/div/div/label[2]",
        inqDetailsCustomizationUnsure: "//div[contains(@class,'inquiry-customize')]/div/div/label[3]",
        inqDetailsCheckbox: "//input[@name='inquiryMarketingEmail']",
        sendInquiry: "//button[contains(@class,'submitInquiry')]",
        inqConfirmationPage: "//div[@class='hero-confirmation-note']//h1"
    },
    navigatetoPDP(ProductName) {
        I.retry(3).amOnPage(ProductName);
        I.wait(5);
    },
    addToInquiry() {
        I.click(this.locators.addToInquiry);
    },
    verifyMiniCart() {
        I.wait(2);
        I.see('Added to Product List', this.locators.cartFlyout)
    },
    clickProceedToInquiry() {
        I.click(this.locators.proceedToInquiry);
    },
    fillInquiryDetails(name, email, phone, priceRange, estimatedQty, targetDate, representative, customization) {
        I.fillField(this.locators.inqDetailsFullName, name);
        I.fillField(this.locators.inqDetailsEmail, email);
        I.fillField(this.locators.inqDetailsPhNum, phone);
        I.fillField(this.locators.inqDetailsPriceRange, priceRange);
        I.fillField(this.locators.inqDetailsQuantity, estimatedQty);
        I.fillField(this.locators.inqDetailsDate, targetDate);
        I.click("#inquiry-representative");
        I.click(this.locators.inqDetailsRepresentative + "[@value='" + representative + "']")
        I.click(this.locators.inqDetailsCustomizationYes);
        I.click(this.locators.inqDetailsCheckbox);
    },
    sendInquiry() {
        I.click(this.locators.sendInquiry);
    },
    inquirySentConfirmation() {
        I.see('', this.locators.inqConfirmationPage);
    }
}