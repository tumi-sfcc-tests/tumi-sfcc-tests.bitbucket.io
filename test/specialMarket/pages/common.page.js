const { I, testDataSpecialMarket, utilities } = inject();
module.exports = {
    locators: {
        //PDP
        stickyATI: "(//button[contains(@class,'add-to-cart')])[2]/span[1]",
        stickyATIPrice: "(//div[@class='price'])[2]/span/span/span",
        featuresTab: "#featuresTab",
        //globalNav
        productCatalogNav: "#productcatalog",
        corporateCollectionNav: "#corporate-collection",
        customizeNav: "#customize",
        repLocatorNav: "#replocator",
        onSiteFittingProgramNav: "#onSiteFittingProgram",
        luggageL2: "#luggage",
        backpacksL2: "#backpacks",
        bagsL2: "#bags",
        accessoriesL2: "#accessories",
        shopFullCatalogL2: "//a[contains(@class,'category-button order-2')]",
        //plp
        categoryTitle: ".category-title",
        customizeTitle: ".uppercase",
        repLocatorTitle: "//h1[contains(@class,'your-representative')]",

    },
    //Pdp
    navigatetoPdpUrl(productUrl) {
        I.retry(3).amOnPage(productUrl);
        I.wait(5);
    },
    verifyFeaturesTab() {
        I.seeElement(this.locators.featuresTab);
    },
    verifyStickyATI(price) {
        I.seeElement(this.locators.stickyATI);
        I.see(price, this.locators.stickyATIPrice);
    },
    //Home page
    clickOnCategoryL1(category) {
        switch (category.toUpperCase()) {
            case 'PRODUCT CATALOG':
                I.click(this.locators.productCatalogNav);
                break;
            case 'CORPORATE COLLECTION':
                I.click(this.locators.corporateCollectionNav);
                break;
            case 'CUSTOMIZE':
                I.click(this.locators.customizeNav);
                break;
            case 'REP LOCATOR':
                I.click(this.locators.repLocatorNav);
                break;
        }

    },
    verifyCategoryL1navigation(links) {
        switch (links) {
            case 'Product Catalog':
                I.see(links, this.locators.categoryTitle);
                break;
            case 'Corporate Collection':
                I.see(links, this.locators.categoryTitle);
                break;
            case 'CUSTOMIZE':
                I.see(links, this.locators.customizeTitle);
                break;
            case 'REP LOCATOR':
                I.see("Find Your Nearest TUMI Representative", this.locators.repLocatorTitle);
                break;
        }
    },
    clickOnCategoryL2(category) {
        I.moveCursorTo(this.locators.productCatalogNav);
        switch (category.toUpperCase()) {
            case 'LUGGAGE':
                I.click(this.locators.luggageL2);
                break;
            case 'BACKPACKS':
                I.click(this.locators.backpacksL2);
                break;
            case 'BAGS':
                I.click(this.locators.bagsL2);
                break;
            case 'ACCESSORIES':
                I.click(this.locators.accessoriesL2);
                break;
            case 'SHOPFULLCATALOG':
                I.click(this.locators.shopFullCatalogL2);
                break;
        }
    }
}