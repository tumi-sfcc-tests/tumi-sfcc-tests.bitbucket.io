const testDataSpecialMarket = require("../testDataSpecialMarket");

const { I, commonPage, productDetailsPage } = inject();

//PDP
Then('I navigate to special market accent product page', () => {
    commonPage.navigatetoPdpUrl(testDataSpecialMarket.pdpUrls.accentedProduct);
});

Then('I navigate to special market product', () => {
    commonPage.navigatetoPdpUrl(testDataSpecialMarket.pdpUrls.sampleProduct);
});

Then('I click on features tab', () => {
    commonPage.verifyFeaturesTab();
});

Then('Verify the product price on sticky ATI', () => {
    commonPage.verifyStickyATI(testDataSpecialMarket.pdpUrls.sampleProductPrice);
});

//HomePage

Then('I navigate to {string} category page in Special market', (SubCategory) => {
    commonPage.clickOnCategoryL2(SubCategory);
});
Then('I click on any {string} on global nav', (navigationLinks) => {
    commonPage.clickOnCategoryL1(navigationLinks);
});
Then('I should land to respective {string} plps', (navigationLinks) => {
    commonPage.verifyCategoryL1navigation(navigationLinks);
});