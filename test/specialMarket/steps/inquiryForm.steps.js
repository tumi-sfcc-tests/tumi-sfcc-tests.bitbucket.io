const testDataSpecialMarket = require("../testDataSpecialMarket");

const { I, inquiryFormPage } = inject();

Then('I navigate to a pdp', () => {
    inquiryFormPage.navigatetoPDP(testDataSpecialMarket.inquiryForm.pdpURL);
});

Then('I click on add to inquiry', () => {
    inquiryFormPage.addToInquiry();
});
Then('I verify that mini-cart flyout opens', () => {
    inquiryFormPage.verifyMiniCart();
});
Then('I click on proceed to inquiry', () => {
    inquiryFormPage.clickProceedToInquiry();
});
Then('I fill valid Inquiry Details', () => {
    inquiryFormPage.fillInquiryDetails(
        testDataSpecialMarket.inquiryForm.name, testDataSpecialMarket.inquiryForm.email,
        testDataSpecialMarket.inquiryForm.phone, testDataSpecialMarket.inquiryForm.PriceRange, testDataSpecialMarket.inquiryForm.Quantity,
        testDataSpecialMarket.inquiryForm.TargetDate, testDataSpecialMarket.inquiryForm.Representative, testDataSpecialMarket.inquiryForm.customization);
});
Then('I click on send Inquiry', () => {
    inquiryFormPage.sendInquiry();
});
Then('I verify that Inquiry is successfully sent', () => {
    inquiryFormPage.inquirySentConfirmation();
});