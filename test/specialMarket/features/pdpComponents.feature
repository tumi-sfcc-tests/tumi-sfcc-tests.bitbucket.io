Feature: PDP on special market
    Verify components and functionality on pdp

    @pdpAccentSM
    Scenario: Verify accenting functionality from pdp
        When I navigate to special market accent product page
        When I add an accent for the product
        Then I should see added accents
        Then I should see updated product price
        Then I remove added accent

    @pdpComponentsFeaturesSM
    Scenario: Verify components and functionality of pdp
        When I navigate to special market product
        And I click on specifications of product page
        Then Verify the specification Details
        And I click on MaintenanceAndCare of product page
        Then Verify the MaintenanceAndCare Details
        Then I click on features tab
        Then Verify product 360 degree view is available
        Then Verify the product price on sticky ATI
        Then Verify the View option button is functional

    #Social icons redirection
    @footerSM  @pdpSM
    Scenario Outline: Verify TUMI social pages
        When I navigate to special market product
        When I click <SocialPage> icon in footer
        Then TUMI <SocialPage> social page should launch in new tab
        Examples:
            | SocialPage  |
            | "FACEBOOK"  |
            | "TWITTER"   |
            | "PINTEREST" |
            | "INSTAGRAM" |
            | "YOUTUBE"   |


