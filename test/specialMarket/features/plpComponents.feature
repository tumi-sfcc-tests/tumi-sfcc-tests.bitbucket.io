Feature: Verify plp and its functionality on Special Market
    As a shopper, verify PLP page sorting, filters, compare functionality is working fine on special market

    Background:
        Given I am on home page
        When I navigate to "luggage" category page in Special market

    @plpSM
    Scenario: Verify filters are working fine
        Then I see product list available
        Then Verify collection filter is working

    @comparebarSM
    Scenario: Verify products getting added to the compare bar
        When I hover over "1st Product" I can see compare checkbox
        And I can click on compare for "1st Product"
        Then I can see "1st Product" added in compare bar
        When I hover over "2nd Product" I can see compare checkbox
        And I can click on compare for "2nd Product"
        Then I can see "2nd Product" added in compare bar
        When I hover over "3rd Product" I can see compare checkbox
        And I can click on compare for "3rd Product"
        Then I can see "3rd Product" added in compare bar

    @disabledCompareCTASM
    Scenario: Verify if compare button is disable if only one product is added in compare bar
        When I hover over "1st Product" I can see compare checkbox
        And I can click on compare for "1st Product"
        Then compare button is disable

    @removeItemSM @clearAllSM
    Scenario: Verify if user is able to remove item by clicking on clear all and remove item link
        When I hover over "1st Product" I can see compare checkbox
        And I can click on compare for "1st Product"
        Then I can see remove item link in compare bar
        When I click on remove item
        Then respective item gets removed
        When I hover over "1st Product" I can see compare checkbox
        And I can click on compare for "1st Product"
        When I hover over "2nd Product" I can see compare checkbox
        And I can click on compare for "2nd Product"
        Then I can see clear All link in compare bar
        When I click on clear All on compare bar
        Then All the items in compare bar should be removed

    @enabledCompareCTASM
    Scenario: Verify if compare button is disable if only one product is added in compare bar
        When I hover over "1st Product" I can see compare checkbox
        And I can click on compare for "1st Product"
        When I hover over "2nd Product" I can see compare checkbox
        And I can click on compare for "2nd Product"
        Then compare button is enable and user can click
        Then I navigate to compare page
