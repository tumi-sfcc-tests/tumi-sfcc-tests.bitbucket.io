Feature: Inquiry Form
    Shopper's able to submits the inquiry form with or without the products

    @inquiryForm
    Scenario: Verify inquiry form submission
        Given I am on home page
        Then I navigate to a pdp
        And I click on add to inquiry
        Then I verify that mini-cart flyout opens
        And I click on proceed to inquiry
        And I fill valid Inquiry Details
        Then I click on send Inquiry
        Then I verify that Inquiry is successfully sent
