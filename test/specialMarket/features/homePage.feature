Feature: Verify the components on home page in special market


    @homPageSM
    Scenario: Verify home page of special market
        Given I am on home page
        Then I should see TUMI logo

    @globalNavSM
    Scenario Outline: Verify link redirection from global nav in special market
        Given I am on home page
        When I click on any "<navigationLinks>" on global nav
        Then I should land to respective "<navigationLinks>" plps
        Examples:
            | navigationLinks      |
            | Product Catalog      |
            | Corporate Collection |
            | CUSTOMIZE            |
            | REP LOCATOR          |

    @CategoryL2SM
    Scenario Outline: Verify Sub category links in product catalog
        Given I am on home page
        When I navigate to "<SubCategory>" category page in Special market
        Examples:
            | SubCategory     |
            | Luggage         |
            | Backpacks       |
            | Bags            |
            | Accessories     |
            | ShopFullCatalog |