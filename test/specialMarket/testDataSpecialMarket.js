module.exports = {

    inquiryForm: {
        pdpURL: "/p/oxford-compact-carry-on-01354911041/",
        name: "Tumi User",
        email: "tumiautomation@yopmail.com",
        phone: "4049493930",
        PriceRange: "1000-1200",
        Quantity: "2",
        TargetDate: "10102023",
        Representative: "AL",
        customization: "Yes"
    },
    pdpUrls: {
        accentedProduct: "/p/extended-trip-expandable-4-wheeled-packing-case-01171671041/",
        sampleProduct: "/p/celina-backpack-01465662693/",
        sampleProductPrice: "$475.00"
    }

}