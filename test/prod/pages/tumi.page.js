const I = actor();
const { utilities } = inject();

module.exports = {
    locators: {
        categoryTitle: ".category-title",
        productList: "//div[@class='product-grid']/div/div",
        addToCartButton: "//div[@class='product-btn-wrapper']//button[text()='Add to Cart']",
        minicartFlyoutCheckout: "//div[@id='flyout-minicart']//a[contains(text(), 'Checkout')]",
        notifyMeButton: "//a[text()='Notify me when available']",
        specificationsTab: "//a[@id='specificationsTab']",
        minicartFlyoutCheckout: "//div[contains(@class,'global-modal show') and @id='miniCartModal']//a[contains(text(), 'Checkout')]",
        minicartFlyoutViewCart: "//div[contains(@class,'global-modal show') and @id='miniCartModal']//a[contains(text(), 'View My Cart')]"
    },

    verifyCategoryTitle(category) {
        // I.saveScreenshot(category + ".png");
        I.see(category, this.locators.categoryTitle);
    },

    verifyPresenceOfCategoryTitle() {
        I.seeElement(this.locators.categoryTitle);
    },

    async verifyCategoryPLPs() {
        const productCount = await I.grabNumberOfVisibleElements(this.locators.productList);
        I.assertNotEqual(productCount, 0, "Verify product count is not equal zero");

    },

    verifyProductIDOnCartFlyOut(sku) {
        I.seeElement("//div[@id='flyout-minicart']//img[@title='" + sku + "']");
        I.seeElement(this.locators.minicartFlyoutCheckout);
    },

    verifyNotifyMeButton() {
        I.waitForVisible(this.locators.notifyMeButton, 20);
    },

    verifyProductAddedToCart() {
        I.click(this.locators.addToCartButton);
        I.wait(2)
        I.seeElement(this.locators.minicartFlyoutCheckout);
    },

    scrollToSpecification() {
        I.scrollIntoView(this.locators.specificationsTab, { behavior: "smooth", block: "center", inline: "center" });
    },

    clickOnSpecifications() {
        this.scrollToSpecification();
        I.click(this.locators.specificationsTab);
    },

    verifySpecifications() {
        I.see('Weight', "#pdpTabContent");
    },

    verifyProductPriceOnSticky(price) {
        I.scrollPageToBottom();
        I.see(price, "//div[contains(@class,'sticky-container')]//div[@class='add-to-cart']//div[@class='price']");
    },

    verifyProductIDOnCartFlyOut(sku) {
        I.seeElement("//div[@id='flyout-minicart']//img[@title='" + sku + "']");
        I.seeElement(this.locators.minicartFlyoutCheckout);
        I.seeElement(this.locators.minicartFlyoutViewCart);
    },

    verifyContentPage(catURL) {
        switch (catURL) {
            case '/c/gift-ideas/':
                I.dontSee("This page is currently unavailable.");
                I.see("Customize & Monogram", "//div[@class='gifting-premium-service']");
                I.seeElement(".super-collection-card-component");
                I.see("Shop Gifts Under $300", "#myTab");
                I.seeElement("//a[text()='View All Gifts Under $300']");
                I.see("Shop Best Sellers Gifts", "#myTab");

                break;
            case '/c/personalization/':
                I.dontSee("This page is currently unavailable.");
                I.see("Make It Yours", ".text-collection");
                I.scrollPageToBottom();
                I.wait(2);
                I.seeElement("//h1[text()='Premium Embroidery']");
                I.seeElement("//h3[text()='Classic Embossed']");
                I.seeElement("//h3[text()='Premium Metal']");
                I.seeElement("//h3[text()='Color Accents']");
                I.seeElement("//h4[text()='Build Your Perfect Bag']");
                I.seeElement("//h2[text()='Shop Tumi+']");
                break;
            case '/c/collections/':
                I.dontSee("This page is currently unavailable.");
                I.seeElement('#myTabContent');
                I.see("Most Popular", "//ul[@id='myTab']/li[1]");
                I.see("Luggage", "//ul[@id='myTab']/li[2]");
                I.see("Bags & Backpacks", "//ul[@id='myTab']/li[3]");
                I.see("Accessories", "//ul[@id='myTab']/li[4]");
                I.scrollPageToBottom();
                I.wait(2);
                I.seeElement('.super-best-seller-container .product');
                I.seeElement("//a[text()='Shop All Best Sellers']");
                break;
            case '/c/gift-ideas/personalized-gifts/':
                I.dontSee("This page is currently unavailable.");
                break;
            case '/c/st-jude/':
                I.dontSee("This page is currently unavailable.");
                I.scrollPageToBottom();
                I.wait(2);
                I.see("Give to help them live.");
                I.seeElement("#donation-tiles-wrapper");
                break;
            default:
                I.dontSee("This page is currently unavailable.");
                break;
        }
    }



}
