const { I, tumiPage, utilities } = inject();

When('I navigate to PDP {string}', (pdpURL) => {
    I.amOnPage(pdpURL);
});

When('I navigate to product {string}', (sku) => {
    I.amOnPage('/p/' + sku);
    I.waitForElement("//div[@data-pid='" + sku + "']", 10);
});

Then('I should see added SKU {string} on cart flyout', (productURL) => {
    // const regex = /\d{3,}/;
    // const match = productURL.match(regex);
    // const sku = match ? match[0] : "Unable to fetch sku from url";
    const sku = productURL.substring(productURL.lastIndexOf("-") + 1).replace("/", "");
    I.say("SKU : " + sku);
    tumiPage.verifyProductIDOnCartFlyOut(sku);
});

Then('I should see product name {string}', (productName) => {
    I.see(productName, "//h1[@class='product-name']")
});

When('I click on specifications of product page', () => {
    tumiPage.clickOnSpecifications();
});

Then('Verify the specification Details', () => {
    tumiPage.verifySpecifications();
});

Then('Verify product price {string}', (productPrice) => {
    tumiPage.verifyProductPrice(productPrice);
});

Then('Verify the PDP status', async () => {
    // I.say(await I.grabNumberOfVisibleElements(tumiPage.locators.notifyMeButton));
    if (await I.grabNumberOfVisibleElements(tumiPage.locators.notifyMeButton) > 0) {
        I.say("Button Visible: Notify me when available");
        tumiPage.verifyNotifyMeButton();
    } else {
        tumiPage.verifyProductAddedToCart();
    }
});