const { I, tumiPage } = inject();

When('I navigate to category pages {string}', (catURL) => {
    I.amOnPage(catURL);
});
When('Verify the {string} page is loading fine', (catURL) => {
    tumiPage.verifyContentPage(catURL);
});

When('Verify all the category pages', async (table) => {
    const tableByHeader = table.parse().hashes();
    for (const row of tableByHeader) {
        const categoryName = row.categoryName;
        const categoryURL = row.categoryURL;
        console.log(categoryName + "|" + categoryURL)
        I.amOnPage(categoryURL);
        tumiPage.verifyCategoryTitle(categoryName);
        await tumiPage.verifyCategoryPLPs();
    }
});

Then('I see the category title {string} on the page', (categoryName) => {
    tumiPage.verifyCategoryTitle(categoryName);
});
Then('verify the presence of category title', () => {
    tumiPage.verifyPresenceOfCategoryTitle();
});

Then('Category products are available', async () => {
    await tumiPage.verifyCategoryPLPs();
});

