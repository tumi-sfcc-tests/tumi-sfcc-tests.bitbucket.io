Feature: PLP Automation
    Verify category pages on production

    @categoryPLP @prodTest
    Scenario Outline: CLP page verification
        When I navigate to category pages "<CategoryURL>"
        Then verify the presence of category title
        And Category products are available
        Examples:
            | CategoryURL                                |
            | /c/luggage/                                |
            | /c/backpacks/                              |
            | /c/bags/                                   |
            | /c/accessories/                            |
            | /c/luggage/carryon-luggage/                |
            | /c/luggage/checked-luggage/                |
            | /c/luggage/hardsided/                      |
            | /c/luggage/softsided/                      |
            | /c/luggage/titanium/                       |
            | /c/luggage/aluminum/                       |
            | /c/luggage/recycled-polycarbonate/         |
            | /c/luggage/tegris/                         |
            | /c/backpacks/active/                       |
            | /c/backpacks/commuter/                     |
            | /c/backpacks/compact/                      |
            | /c/backpacks/travel/                       |
            | /c/backpacks/13-inch-laptop-backpacks/     |
            | /c/backpacks/14-inch-laptop-backpacks/     |
            | /c/backpacks/15-inch-laptop-backpacks/     |
            | /c/backpacks/16-inch-laptop-backpacks/     |
            | /c/backpacks/fxt-ballistic/                |
            | /c/backpacks/leather/                      |
            | /c/backpacks/nylon/                        |
            | /c/backpacks/recycled/                     |
            | /c/bags/briefcases/                        |
            | /c/bags/crossbody-bags/                    |
            | /c/bags/duffels-weekenders/                |
            | /c/bags/slings-waistpacks/                 |
            | /c/bags/tote-bags-satchels/                |
            | /c/bags/active/                            |
            | /c/bags/13-inch-laptop-bags/               |
            | /c/bags/14-inch-laptop-bags/               |
            | /c/bags/15-inch-laptop-bags/               |
            | /c/bags/16-inch-laptop-bags/               |
            | /c/bags/fxt-ballistic/                     |
            | /c/bags/leather/                           |
            | /c/bags/nylon/                             |
            | /c/bags/recycled/                          |
            | /c/accessories/wallets-card-cases/         |
            | /c/accessories/phone-cases/                |
            | /c/accessories/mens-cologne/               |
            | /c/accessories/belts/                      |
            | /c/accessories/eyewear/                    |
            | /c/accessories/outerwear/                  |
            | /c/accessories/umbrellas/                  |
            | /c/accessories/passport-cases-covers/      |
            | /c/accessories/toiletry-cosmetic-bags/     |
            | /c/accessories/packing-cubes/              |
            | /c/accessories/travel-adaptors-chargers/   |
            | /c/accessories/face-masks/                 |
            | /c/accessories/hangers/                    |
            | /c/accessories/luggage-tags/               |
            | /c/accessories/product-care/               |
            | /c/accessories/laptop-covers/              |
            | /c/accessories/portfolios-letterpads/      |
            | /c/gift-ideas/best-sellers/                |
            | /c/gift-ideas/gifts-for-her/               |
            | /c/gift-ideas/gifts-for-him/               |
            | /c/gift-ideas/gifts-under-300/             |
            | /c/gift-ideas/premium-monogramming/        |
            | /c/luggage/carryon-luggage/compact/        |
            | /c/luggage/carryon-luggage/international/  |
            | /c/luggage/carryon-luggage/continental/    |
            | /c/luggage/checked-luggage/short-trip/     |
            | /c/luggage/checked-luggage/extended-trip/  |
            | /c/luggage/checked-luggage/worldwide-trip/ |
            | /c/19-degree/                              |
            | /c/19-degree-aluminum/                     |
            | /c/19-degree-titanium/                     |
            | /c/alpha/                                  |
            | /c/alpha-accessories/                      |
            | /c/alpha-bravo/                            |
            | /c/arrive/                                 |
            | /c/belden/                                 |
            | /c/eyewear/                                |
            | /c/georgica/                               |
            | /c/harrison/                               |
            | /c/mclaren/                                |
            | /c/mens-cologne/                           |
            | /c/mezzanine/                              |
            | /c/nassau/                                 |
            | /c/outerwear/                              |
            | /c/tegra-lite/                             |
            | /c/tumi-plus/                              |
            | /c/voyageur/                               |
    # removed CLPs
    # | /c/luggage/nylon/                          |
    # | /c/pavia/                                  |
    # | /c/tahoe/                                  |
    # | /c/donington/                              |
    # | /c/accessories/lanyards/                   |


    @categoryPLP @prodTest
    Scenario Outline: Verify the content pages loading fine
        When I navigate to category pages "<CategoryURL>"
        Then Verify the "<CategoryURL>" page is loading fine
        Examples:
            | CategoryURL                       |
            | /c/gift-ideas/                    |
            | /c/personalization/               |
            | /c/collections/                   |
            | /c/gift-ideas/personalized-gifts/ |
            | /c/st-jude/                       |


# @categoryPLPInLoop
# Scenario: Verify category pages are working fine on production
#     When Verify all the category pages
#         | categoryName                    | categoryURL                                                         |
#         | Luggage                         | /c/luggage/                                                         |
#         | Shop By Size                    | /c/luggage/size/                                                    |
#         | Carry-On                        | /c/luggage/carryon-luggage/                                         |
#         | Compact                         | /c/luggage/carryon-luggage/compact/                                 |
#         | International                   | /c/luggage/carryon-luggage/international/                           |