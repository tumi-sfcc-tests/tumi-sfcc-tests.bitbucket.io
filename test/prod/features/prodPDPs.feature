Feature: PDP Automation
    Verify a list of PDPs in-stock on production

    @prodPDP @prodTest
    Scenario Outline: Verify PDPs status
        When I navigate to PDP "<PDP_URL>"
        When I click on specifications of product page
        Then Verify the specification Details
        Then Verify the PDP status
        # Then I should see Add To Cart button
        # When I add product into the cart
        # Then I should see added SKU "<PDP_URL>" on cart flyout

        Examples:
            | PDP_URL                                                                  |
            | /p/continental-dual-access-4-wheeled-carry-on-01171611041/               |
            | /p/international-dual-access-4-wheeled-carry-on-01171601041/             |
            | /p/international-expandable-4-wheeled-carry-on-01396831041/              |
            | /p/extended-trip-expandable-4-wheeled-packing-case-01396861041/          |
            | /p/extended-trip-expandable-4-wheeled-packing-case-01171671041/          |
            | /p/extended-trip-expandable-4-wheeled-packing-case-01447941060/          |
            | /p/international-2-wheeled-duffel-backpack-carry-on-01466291041/         |
            | /p/international-front-pocket-expandable-4-wheeled-carry-on-01447951060/ |
            | /p/short-trip-expandable-4-wheeled-packing-case-01171651041/             |
            | /p/continental-front-pocket-expandable-4-wheeled-carry-on-01447961060/   |
            | /p/logistics-flap-lid-backpack-01424811041/                              |
            | /p/search-backpack-01424801041/                                          |
            | /p/navigation-backpack-01424791041/                                      |
            | /p/expedition-flap-backpack-01466911041/                                 |
            | /p/dynamic-backpack-01426141041/                                         |
            | /p/endurance-backpack-01466931041/                                       |
            | /p/renegade-backpack-01466841041/                                        |
            | /p/bradner-backpack-01305331041/                                         |
            | /p/halsey-backpack-0146567T522/                                          |
            | /p/barker-backpack-01173281041/                                          |
            | /p/larson-backpack-01173271041/                                          |
            | /p/sadler-brief-01173331041/                                             |
            | /p/celina-backpack-01465662693/                                          |
            | /p/atlanta-backpack-01465742693/                                         |
            | /p/just-in-case-backpack-0146588T522/                                    |
            | /p/persia-crossbody-0146579T522/                                         |
            | /p/tyler-crossbody-01465812693/                                          |
            | /p/just-in-case-duffel-0146590T522/                                      |
            | /p/oxford-compact-carry-on-01354911041/                                  |
            | /p/just-in-case-tote-01465892693/                                        |
            | /p/madeline-cosmetic-01465922693/                                        |
            | /p/mari-crossbody-01465431041/                                           |
            | /p/sycamore-slim-brief-01305471041/                                      |
            | /p/axle-slim-brief-01445541041/                                          |
            | /p/slim-deluxe-portfolio-01173011041/                                    |
            | /p/expandable-organizer-laptop-brief-01173051041/                        |
            | /p/small-compact-4-wheeled-brief-01396921041/                            |
            | /p/split-travel-kit-01172551041/                                         |
            | /p/hanging-travel-kit-01172541041/                                       |
            | /p/response-travel-kit-01425091041/                                      |
            | /p/electronic-cord-pouch-01172501041/                                    |
            | /p/slim-single-billfold-01356501041/                                     |
            | /p/double-billfold-01303976153/                                          |
            | /p/passport-case-01356291041/                                            |
            | /p/large-laptop-cover-01172521041/                                       |
            | /p/hanger---2-per-set-0052D/                                             |
            | /p/gregory-sling-01305421041/                                            |
            | /p/golf-bag-01171701041/                                                 |
            | /p/tennis-bag-01385521041/                                               |
            | /p/carryall-tote-01385551041/                                            |
            | /p/double-expansion-travel-satchel-01173441041/                          |
            | /p/academy-brief-01424881041/                                            |
            | /p/mason-duffel-01424861041/                                             |
            | /p/international-carry-on-036860SLV2/                                    |
            | /p/extended-trip-packing-case-036869SLV2/                                |


# @prodPDP @unused
# Scenario Outline: Verify a list of product is in-stock on prod
#     When I navigate to PDP <PDP_URL>
#     Then I should see product name <ProductName>
#     When I click on specifications of product page
#     Then Verify the specification Details
#     Then I should see Add To Cart button
#     When I add product into the cart
#     Then I should see added product <ProductName> on cart flyout
#     Examples:
#         | ProductName                                    | PDP_URL                                                        |
#         | "Continental Expandable 4 Wheeled Carry-On"    | "/p/continental-expandable-4-wheeled-carry-on-01171621041/"    |
#         | "International Expandable 4 Wheeled Carry-On"  | "/p/international-expandable-4-wheeled-carry-on-01171541041/"  |
#         | "Rolling Trunk"                                | "/p/rolling-trunk-01355981776/"                                |
#         | "International Expandable 4 Wheeled Carry-On"  | "/p/international-expandable-4-wheeled-carry-on-01396831954/"  |
#         | "Extended Trip Packing Case"                   | "/p/extended-trip-packing-case-01248529618/"                   |
#         | "Short Trip Expandable 4 Wheeled Packing Case" | "/p/short-trip-expandable-4-wheeled-packing-case-01171652693/" |
