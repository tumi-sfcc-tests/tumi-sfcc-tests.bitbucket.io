Feature: Verify the sale price of products on production environment


    @productSalePriceUS @unused
    Scenario Outline: US Prod | Verify product sale prices
        When I navigate to product "<SKUs>"
        Then Verify product price "<Price>"

        Examples:
            | SKUs        | ProductName              | Price |
            | 01356611041 | DBL-SIDED ZIP PKNG CUBE  | 59    |
            | 01450191041 | COSMETIC POUCH           | 75    |
            | 0146562A046 | ZIP CARD CASE            | 75    |
            | 01465621954 | ZIP CARD CASE            | 75    |
            | 0146565A046 | COSMETIC POUCH           | 99    |
            | 01465651041 | COSMETIC POUCH           | 99    |
            | 01465651954 | COSMETIC POUCH           | 99    |
            | 01416202693 | SPLIT TRAVEL KIT         | 99    |
            | 0146564A046 | SPLIT TRAVEL KIT         | 139   |
            | 01355121041 | ZIP-AROUND PASSPORT CASE | 139   |
            | 01465641954 | ZIP-AROUND PASSPORT CASE | 139   |
            | 0146560A046 | TRI-FOLD ZIP-AROUND      | 155   |
            | 01465601954 | TRI-FOLD ZIP-AROUND      | 155   |
            | 01355131041 | WALLET CROSSBODY         | 155   |
            | 0146559A046 | WALLET CROSSBODY         | 199   |
            | 01465591954 | WALLET CROSSBODY         | 199   |
            | 0146563A046 | TRAVEL WALLET            | 235   |
            | 01465631954 | TRAVEL WALLET            | 235   |
            | 01424911041 | RECRUIT CHEST PACK       | 259   |
            | 01356061073 | DOUBLE BILLFOLD          | 89    |
            | 01356261073 | MULTI WINDOW CARD CASE   | 65    |
            | 01356331073 | SLIM CARD CASE           | 65    |
            | 01356571041 | TRAVEL LANYARD           | 85    |
            | 01465191073 | ZIP CARD CASE            | 75    |
            | 01424901041 | TRANSPORT PACK           | 259   |
            | 01445911041 | SHOE BAG                 | 259   |
            | 0146696A027 | PLATOON SLING            | 275   |
            | 0146695A027 | LOGISTICS BACKPACK       | 439   |
            | 0142492A027 | NAVIGATION BACKPACK      | 439   |
            | 0146707A027 | SEARCH BACKPACK          | 499   |
            | 0139683A028 | INTL EXP 4 WHL C/O       | 599   |
            | 01396832245 | INTL EXP 4 WHL C/O       | 599   |
            | 01396831954 | INTL EXP 4 WHL C/O       | 599   |
            | 01447881548 | INTL EXP 4 WHL C/O       | 635   |
            | 01447881886 | INTL EXP 4 WHL C/O       | 635   |
            | 0139685A028 | ST EXP 4 WHL P/C         | 715   |
            | 01396852245 | ST EXP 4 WHL P/C         | 715   |
            | 01396851954 | ST EXP 4 WHL P/C         | 715   |
            | 01356511041 | DOUBLE BILLFOLD          | 119   |
            | 01356521041 | FOLDING CARD CASE        | 79    |
            | 01356541041 | MONEY CLIP CARD CASE     | 79    |
            | 01356551041 | SLIM CARD CASE           | 79    |
            | 01356731041 | SLIM SINGLE BILLFOLD     | 105   |
            | 0146552A038 | GRAZ TOTE                | 525   |
            | 0146548A038 | LINZ LARGE CARRYALL      | 555   |
            | 01465511041 | MARIGOT BACKPACK         | 525   |
            | 0146551A038 | MARIGOT BACKPACK         | 525   |
            | 01445001599 | CARD POUCH LANYARD       | 189   |
            | 0130397A050 | DOUBLE BILLFOLD          | 105   |
            | 01444941599 | DOUBLE BILLFOLD          | 105   |
            | 01444951599 | GLOBAL DOUBLE BILLFOLD   | 105   |
            | 0130412A050 | GUSSETED CARD CASE       | 75    |
            | 01444931599 | GUSSETED CARD CASE       | 75    |
            | 0130420A050 | SLIM CARD CASE           | 75    |
            | 01444921599 | SLIM CARD CASE           | 75    |
            | 01397161041 | ARONA SATCHEL            | 629   |
            | 01397171041 | CORI TOTE                | 629   |
            | 0139686A028 | EXT TRIP EXP 4 WHL P/C   | 759   |
            | 01396862245 | EXT TRIP EXP 4 WHL P/C   | 759   |
            | 01466986154 | BRADNER BACKPACK         | 435   |
            | 01466996154 | OSBORN ROLL TOP BACKPACK | 469   |
            | 01396861954 | EXT TRIP EXP 4 WHL P/C   | 759   |
            | 01467066154 | WILLIAM BACKPACK         | 435   |
            | 0146705A054 | BOZEMAN SLING            | 275   |
            | 0146704A054 | FINCH BACKPACK           | 399   |
            | 0146703A054 | NOTTAWAY BACKPACK        | 345   |
            | 0142380A028 | AIRPOD PRO POUCH         | 49    |
            | 0142380A030 | AIRPOD PRO POUCH         | 49    |
            | 0140119A028 | CARABINER                | 49    |
            | 0142628A028 | MOBILE ORGANIZER         | 65    |
            | 0142628A030 | MOBILE ORGANIZER         | 65    |
            | 0144501A028 | MODULAR ACCESSORY POUCH  | 49    |
            | 0144501A030 | MODULAR ACCESSORY POUCH  | 49    |
            | 0142623A028 | SMALL MODULAR POUCH      | 35    |
            | 0142623A030 | SMALL MODULAR POUCH      | 35    |
            | 0142626A028 | TUMI CARABINER           | 35    |
            | 0142844A028 | ZIP-AROUND CASE          | 65    |
            | 0142844A030 | ZIP-AROUND CASE          | 65    |
            | 01173321041 | HANNOVER SLIM BRIEF      | 955   |
            | 01465751954 | ADELAIDE HOBO CROSSBODY  | 245   |
            | 01447981041 | ADRIAN CARRYALL          | 435   |
            | 0146594A027 | ANKARA WRISTLET POUCH    | 85    |
            | 01465941954 | ANKARA WRISTLET POUCH    | 85    |
            | 0146566A027 | CELINA BACKPACK          | 329   |
            | 01465661954 | CELINA BACKPACK          | 329   |
            | 0146598A032 | CELINA BACKPACK          | 329   |
            | 01465971954 | CHARM POUCH              | 49    |
            | 0146601A034 | CHARM POUCH              | 49    |
            | 0146621A032 | CONTINE WEEKENDER        | 399   |
            | 0146615A036 | CORINTHIA BACKPACK       | 329   |
            | 0146616A036 | COSMETIC POUCH           | 105   |
            | 0146618A036 | DAVENAY CROSSBODY        | 315   |
            | 01465671954 | HALSEY BACKPACK          | 295   |
            | 01447991041 | HELENA CROSSBODY         | 275   |
            | 0146588A027 | JUST IN CASE BACKPACK    | 105   |
            | 01465881954 | JUST IN CASE BACKPACK    | 105   |
            | 0146588T316 | JUST IN CASE BACKPACK    | 105   |
            | 0146620A032 | JUST IN CASE BACKPACK    | 105   |
            | 01465901954 | JUST IN CASE DUFFEL      | 135   |
            | 0146590T316 | JUST IN CASE DUFFEL      | 135   |
            | 0146589A027 | JUST IN CASE TOTE        | 105   |
            | 01465891954 | JUST IN CASE TOTE        | 105   |
            | 0146589T316 | JUST IN CASE TOTE        | 105   |
            | 0146619A032 | JUST IN CASE TOTE        | 105   |
            | 0146611A044 | KAUAI YOGA SLING/TOTE    | 275   |
            | 0146607A044 | KILEEN CONVERTIBLE SLING | 245   |
            | 0146609A044 | LOHA SLIM HIP BAG        | 105   |
            | 0146592A027 | MADELINE COSMETIC        | 175   |
            | 01465921954 | MADELINE COSMETIC        | 175   |
            | 0146602A034 | MADELINE COSMETIC        | 175   |
            | 0146605A044 | MALTA DUFFEL/BACKPACK    | 385   |
            | 0146579A027 | PERSIA CROSSBODY         | 135   |
            | 01465791954 | PERSIA CROSSBODY         | 135   |
            | 01465951954 | SMALL ORGANIZER          | 85    |
            | 0146603A034 | SMALL ORGANIZER          | 85    |
            | 0146617A036 | SOLLER CROSSBODY/CLUTCH  | 245   |
            | 0146581A027 | TYLER CROSSBODY          | 205   |
            | 0146600A032 | TYLER CROSSBODY          | 205   |
            | 01465701954 | VALETTA LARGE TOTE       | 329   |
            | 01465731954 | VALETTA MICRO TOTE       | 205   |
            | 0146599A034 | VALETTA MICRO TOTE       | 205   |
            | 0146612A027 | TRAVEL WALLET            | 189   |
            | 0146614A027 | TRI-FOLD ZIP-AROUND      | 119   |
            | 0146613A027 | ZIP-AROUND CONTINENTAL   | 155   |


    @productSalePriceCA
    Scenario Outline: CA PROD | Verify product sale prices
        When I navigate to product "<SKUs>"
        Then Verify product price "<Price>"

        Examples:
            | SKUs        | ProductName              | Price |
            | 01356611041 | DBL-SIDED ZIP PKNG CUBE  | 79    |
            | 01450191041 | COSMETIC POUCH           | 99    |
            | 0146562A046 | ZIP CARD CASE            | 99    |
            | 01465621954 | ZIP CARD CASE            | 99    |
            | 0146565A046 | COSMETIC POUCH           | 129   |
            | 01465651041 | COSMETIC POUCH           | 129   |
            | 01465651954 | COSMETIC POUCH           | 129   |
            | 01416202693 | SPLIT TRAVEL KIT         | 129   |
            | 0146564A046 | SPLIT TRAVEL KIT         | 179   |
            | 01355121041 | ZIP-AROUND PASSPORT CASE | 179   |
            | 01465641954 | ZIP-AROUND PASSPORT CASE | 179   |
            | 0146560A046 | TRI-FOLD ZIP-AROUND      | 199   |
            | 01465601954 | TRI-FOLD ZIP-AROUND      | 199   |
            | 01355131041 | WALLET CROSSBODY         | 199   |
            | 0146559A046 | WALLET CROSSBODY         | 259   |
            | 01465591954 | WALLET CROSSBODY         | 259   |
            | 0146563A046 | TRAVEL WALLET            | 305   |
            | 01465631954 | TRAVEL WALLET            | 305   |
            | 01424911041 | RECRUIT CHEST PACK       | 339   |
            | 01356061073 | DOUBLE BILLFOLD          | 119   |
            | 01356261073 | MULTI WINDOW CARD CASE   | 85    |
            | 01356331073 | SLIM CARD CASE           | 85    |
            | 01356571041 | TRAVEL LANYARD           | 115   |
            | 01465191073 | ZIP CARD CASE            | 99    |
            | 01424901041 | TRANSPORT PACK           | 339   |
            | 01445911041 | SHOE BAG                 | 339   |
            | 0146696A027 | PLATOON SLING            | 359   |
            | 0146695A027 | LOGISTICS BACKPACK       | 569   |
            | 0142492A027 | NAVIGATION BACKPACK      | 569   |
            | 0146707A027 | SEARCH BACKPACK          | 649   |
            | 0139683A028 | INTL EXP 4 WHL C/O       | 779   |
            | 01396832245 | INTL EXP 4 WHL C/O       | 779   |
            | 01396831954 | INTL EXP 4 WHL C/O       | 779   |
            | 01447881548 | INTL EXP 4 WHL C/O       | 825   |
            | 01447881886 | INTL EXP 4 WHL C/O       | 825   |
            | 0139685A028 | ST EXP 4 WHL P/C         | 929   |
            | 01396852245 | ST EXP 4 WHL P/C         | 929   |
            | 01396851954 | ST EXP 4 WHL P/C         | 929   |
            | 01356511041 | DOUBLE BILLFOLD          | 159   |
            | 01356521041 | FOLDING CARD CASE        | 105   |
            | 01356541041 | MONEY CLIP CARD CASE     | 105   |
            | 01356551041 | SLIM CARD CASE           | 105   |
            | 01356731041 | SLIM SINGLE BILLFOLD     | 135   |
            | 0146552A038 | GRAZ TOTE                | 679   |
            | 0146548A038 | LINZ LARGE CARRYALL      | 719   |
            | 01465511041 | MARIGOT BACKPACK         | 679   |
            | 0146551A038 | MARIGOT BACKPACK         | 679   |
            | 01445001599 | CARD POUCH LANYARD       | 249   |
            | 0130397A050 | DOUBLE BILLFOLD          | 139   |
            | 01444941599 | DOUBLE BILLFOLD          | 139   |
            | 01444951599 | GLOBAL DOUBLE BILLFOLD   | 139   |
            | 0130412A050 | GUSSETED CARD CASE       | 99    |
            | 01444931599 | GUSSETED CARD CASE       | 99    |
            | 0130420A050 | SLIM CARD CASE           | 99    |
            | 01444921599 | SLIM CARD CASE           | 99    |
            | 01397161041 | ARONA SATCHEL            | 815   |
            | 01397171041 | CORI TOTE                | 815   |
            | 0139686A028 | EXT TRIP EXP 4 WHL P/C   | 985   |
            | 01396862245 | EXT TRIP EXP 4 WHL P/C   | 985   |
            | 01466986154 | BRADNER BACKPACK         | 569   |
            | 01466996154 | OSBORN ROLL TOP BACKPACK | 615   |
            | 01396861954 | EXT TRIP EXP 4 WHL P/C   | 985   |
            | 01467066154 | WILLIAM BACKPACK         | 569   |
            | 0146705A054 | BOZEMAN SLING            | 359   |
            | 0146704A054 | FINCH BACKPACK           | 525   |
            | 0146703A054 | NOTTAWAY BACKPACK        | 449   |
            | 0142380A028 | AIRPOD PRO POUCH         | 69    |
            | 0142380A030 | AIRPOD PRO POUCH         | 69    |
            | 0140119A028 | CARABINER                | 69    |
            | 0142628A028 | MOBILE ORGANIZER         | 85    |
            | 0142628A030 | MOBILE ORGANIZER         | 85    |
            | 0144501A028 | MODULAR ACCESSORY POUCH  | 69    |
            | 0144501A030 | MODULAR ACCESSORY POUCH  | 69    |
            | 0142623A028 | SMALL MODULAR POUCH      | 45    |
            | 0142623A030 | SMALL MODULAR POUCH      | 45    |
            | 0142626A028 | TUMI CARABINER           | 45    |
            | 0142844A028 | ZIP-AROUND CASE          | 85    |
            | 0142844A030 | ZIP-AROUND CASE          | 85    |
            | 01173321041 | HANNOVER SLIM BRIEF      | 1239  |
            | 01465751954 | ADELAIDE HOBO CROSSBODY  | 315   |
            | 01447981041 | ADRIAN CARRYALL          | 569   |
            | 0146594A027 | ANKARA WRISTLET POUCH    | 115   |
            | 01465941954 | ANKARA WRISTLET POUCH    | 115   |
            | 0146566A027 | CELINA BACKPACK          | 429   |
            | 01465661954 | CELINA BACKPACK          | 429   |
            | 0146598A032 | CELINA BACKPACK          | 429   |
            | 01465971954 | CHARM POUCH              | 69    |
            | 0146601A034 | CHARM POUCH              | 69    |
            | 0146621A032 | CONTINE WEEKENDER        | 525   |
            | 0146615A036 | CORINTHIA BACKPACK       | 429   |
            | 0146616A036 | COSMETIC POUCH           | 135   |
            | 0146618A036 | DAVENAY CROSSBODY        | 405   |
            | 01465671954 | HALSEY BACKPACK          | 385   |
            | 01447991041 | HELENA CROSSBODY         | 359   |
            | 0146588A027 | JUST IN CASE BACKPACK    | 135   |
            | 01465881954 | JUST IN CASE BACKPACK    | 135   |
            | 0146588T316 | JUST IN CASE BACKPACK    | 135   |
            | 0146620A032 | JUST IN CASE BACKPACK    | 135   |
            | 01465901954 | JUST IN CASE DUFFEL      | 175   |
            | 0146590T316 | JUST IN CASE DUFFEL      | 175   |
            | 0146589A027 | JUST IN CASE TOTE        | 135   |
            | 01465891954 | JUST IN CASE TOTE        | 135   |
            | 0146589T316 | JUST IN CASE TOTE        | 135   |
            | 0146619A032 | JUST IN CASE TOTE        | 135   |
            | 0146611A044 | KAUAI YOGA SLING/TOTE    | 359   |
            | 0146607A044 | KILEEN CONVERTIBLE SLING | 315   |
            | 0146609A044 | LOHA SLIM HIP BAG        | 135   |
            | 0146592A027 | MADELINE COSMETIC        | 225   |
            | 01465921954 | MADELINE COSMETIC        | 225   |
            | 0146602A034 | MADELINE COSMETIC        | 225   |
            | 0146605A044 | MALTA DUFFEL/BACKPACK    | 499   |
            | 0146579A027 | PERSIA CROSSBODY         | 175   |
            | 01465791954 | PERSIA CROSSBODY         | 175   |
            | 01465951954 | SMALL ORGANIZER          | 115   |
            | 0146603A034 | SMALL ORGANIZER          | 115   |
            | 0146617A036 | SOLLER CROSSBODY/CLUTCH  | 315   |
            | 0146581A027 | TYLER CROSSBODY          | 265   |
            | 0146600A032 | TYLER CROSSBODY          | 265   |
            | 01465701954 | VALETTA LARGE TOTE       | 429   |
            | 01465731954 | VALETTA MICRO TOTE       | 265   |
            | 0146599A034 | VALETTA MICRO TOTE       | 265   |
            | 0146612A027 | TRAVEL WALLET            | 249   |
            | 0146614A027 | TRI-FOLD ZIP-AROUND      | 159   |
            | 0146613A027 | ZIP-AROUND CONTINENTAL   | 205   |