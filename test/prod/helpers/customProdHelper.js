const Helper = require('@codeceptjs/helper');
const emailUtils = require('./../../utils/emailUtils.js')
const I = actor();

var currentdate = new Date();
var date = currentdate.getDate() + "/"
    + (currentdate.getMonth() + 1) + "/"
    + currentdate.getFullYear();

var pdpEmails = "Tumi_ecommerceit@samsonite.com,Jay.Carr@tumi.com,Kelly.Rubino@tumi.com,amit.kumar7@publicissapient.com,taushif.ahamad@publicissapient.com,abhishek.mishra1@publicissapient.com,faiz.sikandar@publicissapient.com";
var plpEmails = "Tumi_ecommerceit@samsonite.com,brittany.winston@tumi.com,christina.papa@tumi.com,amit.kumar7@publicissapient.com,taushif.ahamad@publicissapient.com,abhishek.mishra1@publicissapient.com,faiz.sikandar@publicissapient.com";

// var pdpEmails = "taushif.ahamad@publicissapient.com";
// var plpEmails = "taushif.ahamad@publicissapient.com";


let suiteObjects = [];

class Custom extends Helper {

    _before() {
        I.amOnPage('/?no-geo');
        // tryTo(() => I.waitForVisible("//a[contains(@id,'bx-close-inside')]", 40));
        // I.pressKey("Escape");

        // try {
        //         I.waitForVisible("//a[contains(@id,'bx-close-inside')]", 30);
        //         I.click("//a[contains(@id,'bx-close-inside')]");
        //     } catch (error) {
        //         I.say("Popup not found");
        //     }
    }

    _failed(test) {
        // console.log(test);
        test.retries(2);
    };

    //---------- PDP and CLP Report : START -----------//
    // async _afterSuite(suite) {
    //     suiteObjects.push(suite);
    // }

    // _finishTest() {
    //     // console.log(suiteObjects)
    //     let emailText = "";
    //     for (let i = 0; i < suiteObjects.length; i++) {
    //         const suite = suiteObjects[i];
    //         // console.log(suite.title)
    //         if (suite.title == 'PDP Automation') {
    //             emailText = emailText + this.pdpMail(suite);
    //         } else {
    //             emailText = emailText + this.plpMail(suite);
    //         }
    //         if (i == 0) {
    //             emailText = emailText + "\n\n" + "-------------------------------------------------------------------------------------------------------------------------------------------------------" + "\n\n";
    //         }
    //     }
    //     emailUtils.sendEmail("PDP & CLP Automation Test Report | " + date, emailText);
    // }

    //---------- PDP and CLP Report : END -----------//

    //---------- PDP and CLP Separate Report : START -----------//
    async _afterSuite(suite) {
        if (suite.title == 'PDP Automation') {
            this.pdpMail(suite);
        } else {
            this.plpMail(suite);
        }
    }

    //---------- PDP and CLP Separate Report : END -----------//

    pdpMail(suite) {
        let emailContent = "PDP Automation Checkpoints:\n" +
            "\n" +
            "\t" + "1. Verified successful loading of the PDP page\n" +
            "\t" + "2. Ensured the availability of product specification details\n" +
            "\t" + "3. Verified the functionality of adding the products to the cart\n" +
            "\t" + "4. Verified the Notify me button availability if it's OOS\n" +
            "\n" +
            "\n";

        let failedScenarios = "";
        let passedScenarios = "";
        let failCounter = 0;
        let passCounter = 0;
        for (let i = 0; i < suite.tests.length; i++) {
            const test = suite.tests[i];
            //console.log(test.title);
            if (test.err) {
                failedScenarios = failedScenarios + "\t" + ++failCounter + ". " + test.title + "\n";
            } else {
                passedScenarios = passedScenarios + "\t" + ++passCounter + ". " + test.title + "\n";
            }
        }

        if (failCounter > 0) {
            emailContent = emailContent + "Failed SKUs:  Experiencing issues with the following products on the production\n" + failedScenarios;
        } else {
            emailContent = emailContent + "No failed PDP pages observed.\n"
        }

        if (passCounter > 0) {
            emailContent = emailContent + "\n" + "Passed PDPs: Products are in-stock and loading fine\n" + passedScenarios;
        }

        // return emailContent;

        emailUtils.sendEmail("PDP Automation Test Report | " + date, emailContent, pdpEmails);
    }

    plpMail(suite) {
        let emailContent = "CLP Automation Checkpoints:\n" +
            "\n" +
            "\t" + "1. Verified successful loading of the CLP page\n" +
            "\t" + "2. Ensured the availability of products on the page\n" +
            "\n" +
            "\n";

        let failedScenarios = "";
        let passedScenarios = "";
        let failCounter = 0;
        let passCounter = 0;
        for (let i = 0; i < suite.tests.length; i++) {
            const test = suite.tests[i];
            if (test.err) {
                failedScenarios = failedScenarios + "\t" + ++failCounter + ". " + test.title + "\n";
            } else {
                passedScenarios = passedScenarios + "\t" + ++passCounter + ". " + test.title + "\n";
            }
        }

        if (failCounter > 0) {
            emailContent = emailContent + "Failed CLPs:  Experiencing issues with the following CLPs in production\n" + failedScenarios;
        } else {
            emailContent = emailContent + "No failed CLP pages observed.\n"
        }

        if (passCounter > 0) {
            emailContent = emailContent + "\n" + "Passed CLPs: CLPs are loading fine\n" + passedScenarios;
        }

        //return emailContent;

        emailUtils.sendEmail("CLP Automation Test Report | " + date, emailContent, plpEmails);
    }

    /**
     * @protected
     */
    // _after() {
    //     console.log('inside helper -- after test');
    // }

    // add custom methods here
    // If you need to access other helpers
    // use: this.helpers['helperName']

    // _init - before all tests
    // _finishTest - after all tests
    // _before - before a test
    // _after - after a test
    // _beforeStep - before each step
    // _afterStep - after each step
    // _beforeSuite - before each suite
    // _afterSuite - after each suite
    // _passed - after a test passed
    // _failed - after a test failed
}

module.exports = Custom;
