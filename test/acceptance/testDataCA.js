module.exports = {

    pageURLs: {
        homepage: "https://stage-ca.tumi.com/?yottaa-internal-passthru",
        cart: "/cart",
        plp: "/c/luggage/",
        pdp: "/p/garment-bag-tri-fold-carry-on-01171481041"
    },
    socialURLs: {
        facebook: "facebook.com/TumiTravel",
        twitter: "twitter.com/Tumitravel",
        pinterest: "pinterest.com/tumitravel/",
        instagram: "instagram.com/tumitravel/",
        youtube: "youtube.com/user/TumiTravel"
    },
    guestUser: {
        fName: "Guest",
        lName: "User",
        address1: "1212 Green Lake South Rd",
        country: "Canada",
        state: "British Columbia",
        stateAbr: "BC",
        city: "70 Mile House",
        zip: "V0K 2K2",
        phone: "9999551211",
        email: "catumiguest@yopmail.com"
    },
    registerUser: {
        fName: "TumiCA",
        lName: "User",
        address1: "213 Clearbrook Rd",
        country: "United States",
        state: "British Columbia",
        stateAbr: "BC",
        city: "Abbotsford",
        zip: "V2T 5X1",
        phone: "9123412345",
        email: "catumitest@yopmail.com",
        password: "Tumi@2022"
    },
    registerUserForOrder: {
        fName: "TumiCA",
        lName: "Test",
        address1: "02399 Cranberry Crt",
        country: "Canada",
        state: "British Columbia",
        stateAbr: "BC",
        city: "Abbotsford",
        zip: "V3G 2W8",
        phone: "9871234567",
        email: "catumiautomation@yopmail.com",
        password: "Tumi@2022"
    },
    userForSavedAddress: {
        fName: "Xyz",
        lName: "abc",
        email: "tumixyz@yopmail.com",
        password: "Tumi@2022"
    },
    taxFreeAddress: {
        fName: "Test",
        lName: "User",
        address1: "1960 Jerry Toth Drive",
        country: "United States",
        state: "Alaska",
        stateAbr: "AK",
        city: "Rest",
        zip: "99501",
        phone: "907-599-7871"
    },
    newRegisterAccount: {
        email: "tumitest11@xyz.com",
        password: "Tumi@123",
        fName: "John",
        lName: "Smith"
    },
    CC: {
        name: "John Smith",
        ccNum: "4111111111111111",
        ccSecCode: "555",
        expMonth: "08",
        expYear: "2030",
        ccExpDate: "8/2030",
        address1: "02399 Cranberry Crt",
        city: "Abbotsford",
        state: "British Columbia",
        zip: "V3G 2W8",
        phone: "7815551211"
    },
    paypalCred: {
        email: "jorge.hernandez@publicissapient.com",
        fName: "Jorge",
        lName: "Hernandez",
        password: "Tumi8864"
    },
    giftCard: {
        cardNum: "7018525193950000020",
        pin: "98779393"
    },
    defaultProduct: {
        productURL: "/p/international-expandable-4-wheeled-carry-on-01396831041/",
        name: "International Expandable 4 Wheeled Carry-On",
        collection: "19 DEGREE",
        color: "BLACK",
        itemPrice: "C$ 835.00"
    },
    productBelt: {
        productURL: "/p/monaco-leather-belt-0104842T60836/",
        name: "Monaco Leather Belt",
        collection: "BELTS",
        color: "Gun Metal/Black",
        size: "36",
        itemPrice: "$235.00"
    },
    productAccented: {
        productURL: "/p/search-backpack-0150189A320/",
        name: "Slim Three Way Brief",
        collection: "ALPHA",
        color: "BLACK",
        itemPrice: "C$ 715.00"
    },
    productOnOffer: {
        productURL: "/p/tumipax-charlotte-packable-travel-puffer-jacket-142070.html",
        name: "TUMIPAX Charlotte Packable Travel Puffer Jacket",
        color: "BLACK",
        size: "S",
        itemPrice: "$30.00",
        offerPrice: "$27.00",
        offerDesc: "10% OFF on selected products"
    },
    updateProfile: {
        fName: "John",
        lName: "Snow",
        email: "catumitest7@yopmail.com",
        password: "Tumi@2022",
        phone: "9235776655",
        DOB: "07/02",
        gender: "Male",
        newfName: "Ned",
        newlName: "Stark",
        newPhone: "9235554433",
        newDOB: "10/12",
        newGender: "Female",
        newPassword: "Tumi@2023"
    },
    newAddress: {
        fName: "TestN",
        lName: "UserN",
        address1: "8 Rue des Pres",
        country: "Canada",
        state: "Quebec",
        stateAbr: "QC",
        city: "Amqui",
        zip: "G5J 2B1",
        phone: "871-555-2121",
        ccNum: "4111111111111111",
        ccSecCode: "555",
        expMonth: "08",
        expYear: "2023",
        ccExpDate: "8/2023"
    },
    default: {
        fName: "TstDefault",
        lName: "UsrDefault",
        address1: "4200 Sann Rd",
        country: "Canada",
        state: "Ontario",
        stateAbr: "ON",
        city: "Lincoln",
        zip: "L3J 1K1",
        phone: "781-555-1212",
        ccNum: "4111111111111111",
        ccSecCode: "555",
        expMonth: "08",
        expYear: "2023",
        ccExpDate: "8/2023"
    }
};