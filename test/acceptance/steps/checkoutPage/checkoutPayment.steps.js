const { I, data, homepage, productListPage, productDetailsPage, cartPage, checkoutPage, orderConfPage } = inject();


When('I navigates to cart page and clicks checkout', () => {
    productDetailsPage.proceedToCart();
    cartPage.clickCheckout();
});

When('I clicks checkout on minicart flyout', () => {
    cartPage.clickFlyoutCheckout();
});

When('I clicks Buy It Now', () => {
    productDetailsPage.clickBuyItNow();
});

Then('I should redirect to the checkout page', () => {
    checkoutPage.userIsOnCheckoutPage();
});

Then('I should see saved address in shipping details', () => {
    checkoutPage.verifySavedAdressInShippingDetails(
        data.registerUser.fName,
        data.registerUser.lName,
        data.registerUser.address1,
        data.registerUser.country,
        data.registerUser.state,
        data.registerUser.city,
        data.registerUser.zip,
        data.registerUser.phone
    );
});

Then('I fill in the guest user shipping details', () => {
    checkoutPage.fillShippingInfo(
        data.guestUser.fName,
        data.guestUser.lName,
        data.guestUser.address1,
        data.guestUser.state,
        data.guestUser.city,
        data.guestUser.zip,
        data.guestUser.phone
    );
    checkoutPage.fillPersonalDataGuest(data.guestUser.email);
});

Then('As a guest, I fills in invalid shipping details', () => {
    checkoutPage.fillShippingInfo(
        data.guestUser.fName,
        data.guestUser.lName,
        "address1",
        data.guestUser.state,
        "bangalore",
        "12345",
        data.guestUser.phone
    );
});
Then('I fills in Alaska shipping details', () => {
    checkoutPage.fillShippingInfo(
        data.taxFreeAddress.fName,
        data.taxFreeAddress.lName,
        data.taxFreeAddress.address1,
        data.taxFreeAddress.state,
        data.taxFreeAddress.city,
        data.taxFreeAddress.zip,
        data.taxFreeAddress.phone
    );
});

Then('I fills in shipping details', () => {
    checkoutPage.fillShippingInfo(data.registeruser.fName,
        data.registerUser.lName,
        data.registerUser.address1,
        data.registerUser.state,
        data.registerUser.city,
        data.registerUser.zip,
        data.registerUser.phone
    );
});

Then('I fills in billing details', () => {
    checkoutPage.fillBillingAddress(data.registeruser.fName,
        data.registerUser.lName,
        data.registerUser.address1,
        data.registerUser.country,
        data.registerUser.state,
        data.registerUser.city,
        data.registerUser.zip
    );
});

Then('I click next: Payment', () => {
    checkoutPage.clickNextPayment();
});

Then('I click Continue shipping', () => {
    checkoutPage.clickContinueShipping();
});

Then('I click Continue billing', () => {
    checkoutPage.clickContinueBilling();
});

Then('I fills CVV for saved credit card', () => {
    checkoutPage.fillCVVForSavedCC(data.CC.ccSecCode);
});

Then('I fill in credit card details', () => {
    checkoutPage.fillCCPaymentInfo(
        data.CC.name,
        data.CC.ccNum,
        data.CC.expMonth,
        data.CC.expYear,
        data.CC.ccSecCode
    );
});
Then('I fill in gift card details', () => {
    checkoutPage.fillGiftCardInfo(
        data.giftCard.cardNum,
        data.giftCard.pin
    );
});

When('I remove the applied gift card', () => {
    checkoutPage.removeAppliedGC();
});

Then('Gift card should be removed', () => {
    checkoutPage.checkRemovedGC();
});

Then('I click next: Place order', () => {
    checkoutPage.clickNextPlaceOrder();
});

Then('I select PayPal tab', () => {
    checkoutPage.clickPayPalTab();
});

Then('I select klarna payment option', () => {
    checkoutPage.selectKlarnaPaymentOption();
});

Then('I click change payment method', () => {
    I.waitForClickable(checkoutPage.locators.changePaymentMethod, 10);
    I.click(checkoutPage.locators.changePaymentMethod);
});

Then('complete the PayPal account payment', async () => {
    await checkoutPage.completePayPalPayments(data.paypalCred.email, data.paypalCred.password);
});
Then('complete the PayPal Pay in 4 payment', async () => {
    await checkoutPage.completePayPalPayIn4(data.paypalCred.email, data.paypalCred.password);
});

Then('I click Place order', () => {
    checkoutPage.clickPlaceOrderFinal();
});

Then('I place klarna order', () => {
    checkoutPage.clickKlarnaPlaceOrderFinal();
});


Then('complete the Klarna payment', async () => {
    await checkoutPage.completeKlarnaPayment(data.klarna.phone, data.klarna.otp);
});

Then('I should see order confirmation screen', async () => {
    await orderConfPage.verifyOrderPlaced();
});

Then('I verify that zero tax on checkout page', () => {
    I.see("$0.00", checkoutPage.locators.taxTotal);
});

Then('I verify that non zero tax on checkout page', () => {
    I.dontSee("$0.00", checkoutPage.locators.taxTotal);
});


// Then('I should able to see order summery', () => {
//     // checkoutPage.userIsOnCheckoutPage();
//     // checkoutPage.verifyCheckoutInfo(
//     //     data.registeruser.fName,
//     //     data.registeruser.lName,
//     //     data.registeruser.address1,
//     //     data.registeruser.city,
//     //     data.registeruser.zip,
//     //     data.registeruser.phone,
//     //     data.CC.ccNum,
//     //     data.CC.ccExpDate,
//     //     data.productJacket.quantity,
//     //     data.productJacket.totalItemPrice,
//     //     data.productJacket.shipping,
//     //     data.productJacket.tax,
//     //     data.productJacket.estimatedTotal
//     // );

// });

// Then('As a guest, I should able to see order summery', () => {
//     checkoutPage.userIsOnCheckoutPage();
//     checkoutPage.verifyCheckoutInfo(
//         data.guestUser.fName,
//         data.guestUser.lName,
//         data.guestUser.address1,
//         data.guestUser.city,
//         data.guestUser.zip,
//         data.guestUser.phone, data.CC.ccNum,
//         data.CC.ccExpDate,
//         data.productJacket.quantity,
//         data.productJacket.totalItemPrice,
//         data.productJacket.shipping,
//         data.productJacket.tax,
//         data.productJacket.estimatedTotal
//     );
// });

// Then('As a {string}, I should verifies all the details after placing order', (user) => {
//     let billingFname, billingLname, billingEmail;
//     switch (user) {
//         case 'guest paypal user':
//             checkoutPage.verifyOrderConfirmation(
//                 data.guestUser.fName,
//                 data.guestUser.lName,
//                 data.guestUser.address1,
//                 data.guestUser.city,
//                 data.guestUser.zip,
//                 data.guestUser.phone,
//                 data.guestUser.email,
//                 data.paypalCred.fName,
//                 data.paypalCred.lName,
//                 data.paypalCred.email,
//                 "NA",
//                 "NA",
//                 data.productJacket.quantity,
//                 data.productJacket.itemPrice,
//                 data.productJacket.shipping,
//                 data.productJacket.tax,
//                 data.productJacket.estimatedTotal
//             );
//             break;
//         case 'registered paypal user':
//             checkoutPage.verifyOrderConfirmation(
//                 data.registeruser.fName,
//                 data.registeruser.lName,
//                 data.registeruser.address1,
//                 data.registeruser.city,
//                 data.registeruser.zip,
//                 data.registeruser.phone,
//                 data.registeruser.email,
//                 data.paypalCred.fName,
//                 data.paypalCred.lName,
//                 data.paypalCred.email,
//                 "NA",
//                 "NA",
//                 data.productJacket.quantity,
//                 data.productJacket.itemPrice,
//                 data.productJacket.shipping,
//                 data.productJacket.tax,
//                 data.productJacket.estimatedTotal
//             );
//             break;
//         case 'guest credit card user':
//             // checkoutPage.verifyOrderConfirmation(
//             //     data.guestUser.fName,
//             //     data.guestUser.lName,
//             //     data.guestUser.address1,
//             //     data.guestUser.city,
//             //     data.guestUser.zip,
//             //     data.guestUser.phone,
//             //     data.guestUser.email,
//             //     data.guestUser.fName,
//             //     data.guestUser.lName,
//             //     data.guestUser.email,
//             //     data.CC.ccNum,
//             //     data.CC.ccExpDate,
//             //     data.productJacket.quantity,
//             //     data.productJacket.itemPrice,
//             //     data.productJacket.shipping,
//             //     data.productJacket.tax,
//             //     data.productJacket.estimatedTotal
//             // );
//             I.see('Create Account !');
//             I.see('Order Number:');
//             break;
//         case 'registered credit card user':
//             // checkoutPage.verifyOrderConfirmation(
//             //     data.registeruser.fName,
//             //     data.registeruser.lName,
//             //     data.registeruser.address1,
//             //     data.registeruser.city,
//             //     data.registeruser.zip,
//             //     data.registeruser.phone,
//             //     data.registeruser.email,
//             //     data.registeruser.fName,
//             //     data.registeruser.lName,
//             //     data.registeruser.email,
//             //     data.CC.ccNum,
//             //     data.CC.ccExpDate,
//             //     data.productJacket.quantity,
//             //     data.productJacket.itemPrice,
//             //     data.productJacket.shipping,
//             //     data.productJacket.tax,
//             //     data.productJacket.estimatedTotal
//             // );
//             I.see('Create Account !');
//             I.see('Order Number:');
//             break;

//         case 'guest gift card user':
//             checkoutPage.verifyOrderConfirmation(
//                 data.guestUser.fName,
//                 data.guestUser.lName,
//                 data.guestUser.address1,
//                 data.guestUser.city,
//                 data.guestUser.zip,
//                 data.guestUser.phone,
//                 data.guestUser.email,
//                 data.guestUser.fName,
//                 data.guestUser.lName,
//                 data.guestUser.email,
//                 data.giftCard.cardNum,
//                 data.giftCard.pin,
//                 data.productJacket.quantity,
//                 data.productJacket.itemPrice,
//                 data.productJacket.shipping,
//                 data.productJacket.tax,
//                 data.productJacket.estimatedTotal
//             );
//             break;
//         case 'registered gift card user':
//             checkoutPage.verifyOrderConfirmation(
//                 data.registeruser.fName,
//                 data.registeruser.lName,
//                 data.registeruser.address1,
//                 data.registeruser.city,
//                 data.registeruser.zip,
//                 data.registeruser.phone,
//                 data.registeruser.email,
//                 data.registeruser.fName,
//                 data.registeruser.lName,
//                 data.registeruser.email,
//                 data.giftCard.cardNum,
//                 data.giftCard.pin,
//                 data.productJacket.quantity,
//                 data.productJacket.itemPrice,
//                 data.productJacket.shipping,
//                 data.productJacket.tax,
//                 data.productJacket.estimatedTotal
//             );
//             break;
//         default:
//             //code block
//             break;
//     }
// });



