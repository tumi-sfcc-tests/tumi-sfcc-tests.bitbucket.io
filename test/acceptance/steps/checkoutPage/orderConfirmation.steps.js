const { I, checkoutPage, orderConfPage } = inject();

Then('I should be able to see all details on checkout page', async () => {
    await orderConfPage.getAllDetailsCheckout();
});

Then('I should see order number on order confirmation screen', () => {
    orderConfPage.verifyOrderNumber();
});

Then('I should see correct shipping address displayed on order confirmation screen', async () => {
    await orderConfPage.verifyShippingOrderConf();
});

Then('I should see correct product and details on order confirmation screen', async () => {
    await orderConfPage.verifyProductDetailOrderConf();
});

Then('I should see correct order total on order confirmation screen', async () => {
    await orderConfPage.verifyOrderTotalOrderConf();
});

Then('I should see enable alert section on order confirmation screen', () => {
    orderConfPage.verifyAlertSectionOrderConf();
});