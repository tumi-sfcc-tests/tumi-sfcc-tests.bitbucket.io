const { I, data, checkoutPage } = inject();

Then('I am able to see {string} filled on shipping page', (field) => {
    checkoutPage.verifyShippingFields(field)
});
Then('I am able to see Shipping Details header on shipping page', () => {
    checkoutPage.verifyShippingHeader();
});
Then('I am able to check the email new arrival checkbox', () => {
    checkoutPage.checkBoxInShipping();
});
Then('I am able to see standard shipping method selected', () => {
    checkoutPage.verifyShippingMethod();
});
Then('I am able to see continue button', () => {
    checkoutPage.verifyContinueButton();
});
Then('I click on Choose expedited shipping link', () => {
    checkoutPage.clickChooseShipLink();
})
Then('I am able to see {string} with {string} and {string}', (method, days, charges) => {
    checkoutPage.seeShippingMethod(method, days, charges);
});
Then('I click to select {string} by clicking on radio button', (method) => {
    checkoutPage.selectShippingMethod(method);
});
Then('I see {string} gets selected with {string} and {string}', (method, days, charges) => {
    checkoutPage.verifyShippingMethod(method, days, charges);
});
Then('I click on address line 1', () => {
    checkoutPage.clickAddressLine1();
});
Then('I enter the search term to get auto-suggested address', () => {
    checkoutPage.enterAddressTerm(data.address.term);
});
Then('I am able to see expected address in auto suggestion list', () => {
    checkoutPage.verifyAutosuggestion(data.address.suggestedLine)
});
