const { I, data, homepage, productDetailsPage } = inject();

Then('I am able to switch between tabs', () => {
    homepage.verifyTheCategoryAndCollectionSwitch();
});

When('I click on the {string}', (linkName) => {
    homepage.footerLink(linkName);
});

Then('I see {string} component', (component) => {
    homepage.verifyHomePageComponents(component);
});

Then('I see components', async (table) => {
    // I.say(table);
    // for (const id in table.rows) {
    //     if (id < 1) {
    //         continue; // skip a header of a table
    //     }
    //     //I.say(id);
    //     const cells = table.rows[id].cells;
    //     // take values
    //     const component = cells[0].value;
    //     I.say(component);
    //     await homepage.verifyHomePageComponents(component);
    // }
    let comp = table.rows[1].cells[0].value;
    //homepage.verifyHomePageComponents(table.rows[1].cells[0].value);

    I.say("comp name : " + comp);
    if (comp == "HERO COMPONENT") {
        I.say("YESSSS");
    }


});