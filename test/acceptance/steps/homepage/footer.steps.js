const { I, data, homepage, utilities } = inject();

When('I click {string} icon in footer', (socialPage) => {
    homepage.launchSocialPages(socialPage);
});

Then('TUMI {string} social page should launch in new tab', async (socialPage) => {
    await homepage.verifySocialPageInNewTab(socialPage);
});

When('I enter the email {string}', (email) => {
    homepage.addEmailInNewsLetter(email);
});

Then('I enter the already existing email', async () => {
    newRandomEmail = await utilities.randomEmailGenerator();
    homepage.addEmailInNewsLetter(newRandomEmail);
});

Then('I should get a message {string}', (statusMsg) => {
    homepage.verifyNewsLetterMsg(statusMsg);
});

Then('I should see {string}', (str) => {
    str = String(str).toLowerCase();
    switch (str) {
        case 'footer area':
            I.scrollTo(homepage.locators.footer_TUMILogo);
            I.seeElement(homepage.locators.footerSection);
            break;
        case 'TUMI logo':
            I.seeElement(homepage.locators.footer_TUMILogo);
            break;
        case 'selected store image':
            I.seeElement(homepage.locators.footer_storeImg);
            break;
        case 'customer service':
            I.seeElement(homepage.locators.footer_CustomerService);
            break;
        case 'my account':
            I.seeElement(homepage.locators.footer_MyAccount);
            break;
        case 'about tumi':
            I.seeElement(homepage.locators.footer_AboutTUMI);
            break;
        case 'get the latest news':
            I.seeElement(homepage.locators.footer_GetLatestNews);
            break;
        case 'Register yout TUMI':
            I.seeElement(homepage.locators.footer_RegisterYourTUMI);
            break;
        case 'selected country':
            I.seeElement(homepage.locators.footer_ShipToCountry);
            break;
        case 'footer copyright links':
            I.seeElement(homepage.locators.footer_CopyrightLinks);
            break;
        case 'Setting Your TUMI Lock':
            I.seeElement(homepage.locators.footer_TumiLock);
            break;
        case 'product info and warranty':
            I.scrollTo(homepage.locators.footer_ProductInfo);
            I.seeElement(homepage.locators.footer_ProductInfo);
            break;
    }
});

Then('I click link {string} in the footer', async (linkname) => {
    I.scrollPageToBottom();
    I.wait(2);
    I.scrollIntoView(homepage.locators.footer_Sitemap);
    switch (linkname.toUpperCase()) {
        case 'TUMI LOGO':
            I.click(homepage.locators.footer_TUMILogo);
            break;
        case 'CUSTOMER SERVICE':
            I.click(homepage.locators.footer_CustomerService);
            break;
        case 'SHIPPING':
            I.click(homepage.locators.footer_Shipping);
            break;
        case 'RETURNS':
            I.click(homepage.locators.footer_Returns);
            break;
        case 'PAYMENT METHODS':
            I.click(homepage.locators.footer_PaymentMethods);
            break;
        case 'SERVICE & REPAIRS':
            I.click(homepage.locators.footer_ServiceRepairs);
            break;
        case 'PRODUCT INFO AND WARRANTY':
            I.click(homepage.locators.footer_ProductInfo);
            break;
        case 'AIRLINE CARRY-ON GUIDE':
            I.click(homepage.locators.footer_AirlineGuide);
            break;
        case 'FAQS':
            I.click(homepage.locators.footer_FAQs);
            break;
        case 'GIFT CARDS':
            I.click(homepage.locators.footer_GiftCards);
            break;
        case 'REPLACEMENT PARTS':
            I.click(homepage.locators.footer_ReplacementParts);
            break;
        case 'SETTING YOUR TUMI LOCK':
            I.click(homepage.locators.footer_TumiLock);
            break;
        case 'MY ACCOUNT':
            I.click(homepage.locators.footer_MyAccount);
            break;
        case 'SIGN IN':
            I.click(homepage.locators.footer_SignIn);
            break;
        case 'TRACK ORDERS':
            I.click(homepage.locators.footer_TrackOreder);
            break;
        case 'REGISTER YOUR TUMI':
            I.click(homepage.locators.footer_RegisterYourTUMI);
            break;
        case 'ABOUT TUMI':
            I.click(homepage.locators.footer_AboutTUMI);
            break;
        case 'TUMI DIFFERENCE':
            I.click(homepage.locators.footer_TumiDifference);
            break;
        case 'CORPORATE GIFTS & INCENTIVES':
            I.click(homepage.locators.footer_CorporateGift);
            break;
        case 'CAREERS':
            I.click(homepage.locators.footer_Career);
            break;
        case 'TUMI CANADA, ULC':
            I.click(homepage.locators.footer_CanadaULC);
            break;
        case 'TERMS & CONDITIONS':
            I.click(homepage.locators.footer_TermsCondition);
            break;
        case 'TRANSPARENCY IN SUPPLY CHAIN ACT':
            I.click(homepage.locators.footer_Transparency);
            break;
        case 'WEB ACCESSIBILITY STATEMENT':
            I.click(homepage.locators.footer_WebAccessibility);
            break;
        case 'MODERN SLAVERY STATEMENT':
            I.click(homepage.locators.footer_ModernSlavery);
            break;
        case 'PRIVACY POLICY':
            I.click(homepage.locators.footer_PrivacyPolicy);
            break;
        case 'SITEMAP':
            I.click(homepage.locators.footer_Sitemap);
            break;
        default:
            break;
    }
    I.wait(2);
});

Then('I am able to see store name in footer store details', () => {
    homepage.seeStoreName()
});
Then('I click on change cta', () => {
    homepage.clickChange();
});
Then('I navigate to store finder page', () => {
    homepage.NavStoreFinder();
});
Then('I enter a store title that i want to search', () => {
    homepage.clickLocationField();
});
Then('I click on find store', () => {
    homepage.clickFindStore();
});
Then('I verify the store title shown in list', () => {
    homepage.verifyStoreTitle();
});
Then('I am able to see and click on save as my store radio button', () => {
    homepage.clickSaveStore();
});
Then('I click on tumi logo to go to home page', () => {
    homepage.clickTumiLogo();
});
Then('I verify save as my store saved this new store', () => {
    homepage.verifyStoreNamefooter();
});
Then('I verify the store on row content', () => {
    homepage.verifyStoreNameBody();
});
Then('I hover over service in navigation bar', () => {
    homepage.hoverServiceNav();
});
Then('I can see selected store title on service nav container', () => {
    homepage.verifyStoreNameServiceNavCont();
});
