const { I, data, homepage, utilities } = inject();

When('I click on {string}', (superMenu) => {
    homepage.openSuperMenu(superMenu);
});

Then('I should see {string} in page current URL', (URL) => {
    I.seeInCurrentUrl(URL);
});

When('Tumi logo is visible', () => {
    I.seeElement(homepage.locators.tumiLogo);
});

When('I click on Tumi logo', () => {
    I.click(homepage.locators.tumiLogo);
});

Then('I should redirect to homepage', async () => {
    await homepage.verifyUserIsHomepage();
});

Then('I should see {string} drawer', async (superMenu) => {
    await homepage.verifySuperCategoryDrawers(superMenu);
});

Then('I should see all super categories menus', () => {
    homepage.verifyHeaderMenus();
});
Then('I should see TUMI logo', () => {
    I.seeElement(homepage.locators.tumiLogo);
});

Then('I should see My Account, Search and Cart icons', () => {
    homepage.verifyHeaderNavIcons();
});

When('I click cart icon', () => {
    homepage.clickCart();
});

When('I should navigate to cart page', () => {
    I.seeElement(".your-cart-header");
});

Then('I should see store under service drawer', () => {
    homepage.verifyStoreinServiceDrawer();
});
