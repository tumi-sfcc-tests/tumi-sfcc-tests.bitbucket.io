const { I, data, utilities, loginPage } = inject();

Then('I fill in registration form with new email', async () => {
    newRandomEmail = await utilities.randomEmailGenerator();
    // newRandomEmail = data.newRegisterAccount.email;
    loginPage.fillInRegisterForm(data.newRegisterAccount.fName, data.newRegisterAccount.lName, newRandomEmail, data.newRegisterAccount.password);
});

Then('I fill in registration form with existing email', async () => {
    loginPage.fillInRegisterForm(data.newRegisterAccount.fName, data.newRegisterAccount.lName, data.newRegisterAccount.email, data.newRegisterAccount.password);
});

Then('I add a new email to navigate on register page', async () => {
    newRandomEmail = await utilities.randomEmailGenerator();
    loginPage.loginContinueEmail(newRandomEmail);
});

Then('clicks on create account button', () => {
    loginPage.clickCreateAccount();
});

Then('I see email verification sent message', () => {
    loginPage.verifyRegistrationMailSentMessage();
});

Then('I see username is invalid error', () => {
    I.seeElement(loginPage.locators.registerEmailError);
});

Then('I should see create account form error messages', () => {
    loginPage.verifyCreateAccountErrorMessages();
});

Then('I switch to register tab', () => {
    loginPage.clickRegisterTab();
});
