const { I, data, homepage, loginPage, utilities } = inject();

// For going to the login landing page
Given('I navigate to the login Page', () => {
  homepage.navigateToLoginPage();
});
Given('I am on the login page', () => {
  homepage.navigateToLoginPage();
});

Given('I enter email and continue', () => {
  loginPage.loginContinueEmail(data.registerUser.email);
});

Given('I log in with valid username and password', async () => {
  loginPage.login(data.registerUser.email, data.registerUser.password);
  await loginPage.closeSignedInModal();
});

Given('I am logged into the website', async () => {
  homepage.navigateToLoginPage();
  loginPage.login(data.registerUser.email, data.registerUser.password);
  await loginPage.closeSignedInModal();
});

Given('I am logged into the website with a user to verify store location', async () => {
  homepage.navigateToLoginPage();
  loginPage.login(data.UserWithPurchase.email, data.UserWithPurchase.password);
  await loginPage.closeSignedInModal();
});

Given('I login with a user having multiple orders', async () => {
  homepage.navigateToLoginPage();
  loginPage.login(data.UserWithPurchase.email, data.UserWithPurchase.password);
  await loginPage.closeSignedInModal();
});

Given('I login with a user to verify purchase list', async () => {
  homepage.navigateToLoginPage();
  loginPage.login(data.UserWithPurchase.email, data.UserWithPurchase.password);
  await loginPage.closeSignedInModal();
});

Given('I sign in', () => {
  I.refreshPage();
  homepage.navigateToLoginPage();
  loginPage.login(data.registerUser.email, data.registerUser.password);
});

Given('I close the login flyout', async () => {
  await loginPage.closeSignedInModal();
});

Given('I am logged into the website using {string} and {string}', async (email, password) => {
  homepage.navigateToLoginPage();
  loginPage.login(email, password);
  await loginPage.closeSignedInModal();
});

Given('I attempt to log in with invalid username {string} and password {string}', (email, password) => {
  loginPage.login(email, password);
});
Given('I attempt to log in with blank username and password', () => {
  loginPage.login("", "");
});

Given('I am logged into the website for order placement', async () => {
  homepage.navigateToLoginPage();
  loginPage.login(data.registerUserForOrder.email, data.registerUserForOrder.password);
  await loginPage.closeSignedInModal();
});
Given('I am logged into the website to check address functionality', async () => {
  homepage.navigateToLoginPage();
  loginPage.login(data.userForSavedAddress.email, data.userForSavedAddress.password);
  await loginPage.closeSignedInModal();
});

Given('I am logged into the website to update profile', async () => {
  homepage.navigateToLoginPage();
  loginPage.login(data.updateProfile.email, data.updateProfile.password);
  await loginPage.closeSignedInModal();
});

Then('I complete the login to wishlist items', () => {
  loginPage.login(data.registerUser.email, data.registerUser.password);
});

When('I log into the site using google email {string} and password {string}', (email, password) => {
  homepage.navigateToLoginPage();
  loginPage.googleLogin(email, password);
});

Then('I should see my user firstname', () => {
  loginPage.verifyLoggedInUsername(data.registerUser.fName)
});

Then('I logout the website', () => {
  loginPage.logout();
});

Then('Verify user successfully logout', () => {
  loginPage.isUserLogout();
});


Then('I should see error messages for the blank fields', () => {
  loginPage.verifyErrorMessages();
});
Then('I should see an invalid user error message', () => {
  loginPage.verifyInvalidUserErrorMessages();
});

Then('test something', () => {
  I.waitForVisible(".accountheader", 3);
  I.click('.header-profile');
  pause();

});