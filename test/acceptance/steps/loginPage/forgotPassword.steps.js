const { I, data, loginPage } = inject();

When('I click forgot password', () => {
    I.waitForElement(loginPage.locators.forgotPassword);
    I.click(loginPage.locators.forgotPassword);
});

When('I fill out their recovery email address', () => {
    loginPage.forgotPassword(data.registerUser.email);
});

Then('I should see confirmation of password reset mail sent', () => {
    loginPage.verifyPasswordReset();
});

Then('I click back to sign in button', () => {
    I.click(loginPage.locators.backToSignin);
});

Then('I should navigate back to sign in page', () => {
    loginPage.verifyUserOnLoginStep();
});

Then('I should see cancel and return to login page CTA', () => {
    I.seeElement(loginPage.locators.cancelReturnToLogin);
});


