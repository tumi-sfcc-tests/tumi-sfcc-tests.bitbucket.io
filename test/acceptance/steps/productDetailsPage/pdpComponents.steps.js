const { I, data, productDetailsPage } = inject();

When('I click on MaintenanceAndCare of product page', () => {
    productDetailsPage.clickOnMaintenance();
});

Then('Verify the MaintenanceAndCare Details', () => {
    productDetailsPage.verifyMaintenanceAndCare();
});

When('I click on specifications of product page', () => {
    productDetailsPage.clickOnSpecifications();
});

Then('Verify the specification Details', () => {
    productDetailsPage.verifySpecifications();
});

When('I click on AirLineGuide of product page', () => {
    productDetailsPage.clickOnairLineGuide();
});

Then('Verify the AirLineGuide Details', () => {
    productDetailsPage.verifyairLineGuide();
});

Then('I am able to change size units', () => {
    productDetailsPage.selectSizeUnits();
});
Then('I am able to browse all the AirlineGuide list', () => {
    productDetailsPage.verifyBrowseAllAirlineGuidePopUp();
});

When('I navigate to the product page', () => {
    I.retry(3).amOnPage(data.defaultProduct.productURL);
    I.wait(5);
});
When('I navigate to 360 degree view product page', () => {
    I.retry(3).amOnPage(data.product360view.productURL);
    I.wait(5);
});

////////////////////////////////////////////////////

Then('Verify add to cart is functional', () => {
    productDetailsPage.verifyStickyAddToCart();
});
Then('Verify the product price on sticky ATC', () => {
    productDetailsPage.verifyProductPriceOnSticky(data.defaultProduct.itemPrice);
});
Then('Verify the View option button is functional', async () => {
    productDetailsPage.verifyStickyViewOption();
});

Then('Verify product 360 degree view is available', () => {
    productDetailsPage.verify360View();
});
Then('Verify product video is available', () => {
    productDetailsPage.verifyVideo();
});

Then('Verify the include with purchase', async () => {
    productDetailsPage.verifyIncludeWithPurchase();
});

Then('Verify pixlee section is available', async () => {
    productDetailsPage.verifyPixlee();
});
Then('Verify the recommendation component is available', async () => {
    productDetailsPage.verifyRecommendation();
});


