const { I, data, cartPage, productDetailsPage } = inject();

Then('I navigate to accent product page', () => {
    I.retry(3).amOnPage(data.productAccented.productURL);
    I.wait(5);
});

Then('I see accent customization option available', () => {
    I.seeElement(productDetailsPage.locators.pdpAccentSection);
});
When('I add an accent for the product', () => {
    productDetailsPage.addAccent();
});

Then('I should see added accents', () => {
    productDetailsPage.verifyAddedAccent();
});

Then('I remove added accent', () => {
    productDetailsPage.verifyRemoveAccent();
});

Then('I should see updated product price', () => {
    productDetailsPage.verifyAccentPriceChange(data.productAccented.itemPrice);
});

Then('I am able to add accented product to the cart', () => {
    productDetailsPage.addToCart();
    cartPage.verifyDetailsOnCartFlyOut(data.productAccented.name)
});

