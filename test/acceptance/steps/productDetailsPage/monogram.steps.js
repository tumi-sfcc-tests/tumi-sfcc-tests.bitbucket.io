const { I, data, cartPage, productDetailsPage } = inject();

When('I add classic monogram for the product', () => {
    productDetailsPage.addClassicMonogram();
});

Then('I navigate to monogrammable product page', () => {
    I.retry(3).amOnPage(data.productMonogrammed.productURL);
    I.wait(5);
});

When('I add premium monogram for the product', () => {
    productDetailsPage.addPremiumMonogram();
});

Then('I see added monogram', () => {
    productDetailsPage.verifyAddedClassincMonogram();
});

When('I remove added monogram', () => {
    productDetailsPage.verifyRemoveMonogram();
});
Then('Added monogram should be removed', () => {
    productDetailsPage.verifyMonogramRemoved();
});
Then('I should not see updated product price', () => {
    productDetailsPage.verifyPriceisNotChange(data.productMonogrammed.itemPrice);
});
Then('I see monogram customization option available', () => {
    I.seeElement(productDetailsPage.locators.pdpMonogramSection);
});

Then('I am able to add monogrammable product to the cart', () => {
    productDetailsPage.addToCart();
    cartPage.verifyDetailsOnCartFlyOut(data.productMonogrammed.name);
});