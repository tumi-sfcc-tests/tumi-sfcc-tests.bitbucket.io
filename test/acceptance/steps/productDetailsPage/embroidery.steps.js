const { I, data, cartPage, productDetailsPage, embroideryPage } = inject();

Then('I go to an Embroidery product', () => {
    I.retry(3).amOnPage(data.productembroidered.productURL);
    I.wait(5);
});
Then('I see embroidery added to product with initial {string}', (initial) => {
    embroideryPage.verifyEmbAdded(initial);
});
Then('I see embroidery modal open', () => {
    embroideryPage.verifyModalEmbroidery();
});
Then('I verify note for instore pickup on Embroidery modal', () => {
    embroideryPage.checkAddedNote(data.productembroidered.note);
});

Then('I click on add to add Embroidery', () => {
    embroideryPage.addEmbroidery();
});
Then('I choose style {string} font', (font) => {
    embroideryPage.chooseStyleEmb(font);
});
Then('I click on next button on {string}', (modalPageNum) => {
    embroideryPage.clickNextEmb(modalPageNum);
});
Then('I navigate to page {string} of embroidery modal', (modalPageNum) => {
    embroideryPage.verifyPageNavigationEmb(modalPageNum);
});
Then('I enter symbol {string} initials', (initial) => {
    embroideryPage.enterInitialEmb(initial);
});
Then('I select font color {string}', (color) => {
    embroideryPage.selectColorEmb(color);
});
Then('I click on Apply button to apply embroidery', () => {
    embroideryPage.applyEmbroidery();
});
Then('I click on cancel on embroidery modal', () => {
    embroideryPage.clickCancelEmb();
});
Then('I click on previous on embroidery modal on page {string}', (modalPageNum) => {
    embroideryPage.clickPreviousEmb(modalPageNum);
});
Then('I see embroidery is not added and modal is closed', () => {
    embroideryPage.verifyEmbroideryNotAdded();
});
Then('I can see Edit button for embroidery', () => {
    embroideryPage.seeEditEmbroidery();
});
Then('I click on Edit button', () => {
    embroideryPage.clickEditEmbroidery();
});
Then('I see remove button for embroidery', () => {
    embroideryPage.seeRemoveEmbroidery();
});
Then('I click on remove button', () => {
    embroideryPage.clickRemoveEmbroidery();
});
Then('I see embroidery is removed', () => {
    embroideryPage.embroideryRemoved();
});
Then('I am able to see cross button', () => {
    embroideryPage.crossOnEmbModal();
});
Then('I click on cross button', () => {
    embroideryPage.clickcrossOnEmbModal();
});
Then('I click on checkout from cart', () => {
    embroideryPage.clickCheckoutOnCart();
});
Then('I can see on checkout page that embroidery added to product with initial {string}', (initials) => {
    embroideryPage.verifyEmbroideryOnCheckout(initials);
});
Then('I can see on Order Confirmation page that embroidery added to product with initial {string}', (initials) => {
    embroideryPage.verifyEmbroideryOnOrderConfirmation(initials);
});
Then('I click on add to the cart', () => {
    embroideryPage.clickAddtoCart();
});
Then('I verify initial {string} on cart flyout and click on view my cart', (initial) => {
    embroideryPage.verifyEmbroideryOnCartFlyout(initial);
});
Then('I see correct name for font {string}', (font) => {
    embroideryPage.verifyNameOfFont(font);
});
Then('I enter alphanumeric value {string} for initial', (initial) => {
    embroideryPage.enterAlpNumInitial(initial);
});
Then('I see embroidery with initial {string} on PDP', (initial) => {
    embroideryPage.verifyAlpNumInitial(initial);
})