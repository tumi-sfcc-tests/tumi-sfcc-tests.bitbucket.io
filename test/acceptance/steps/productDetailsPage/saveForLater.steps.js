const { I, data, homepage, saveForLaterPage, accountPage } = inject();


Then('I see saved items icon in header and click', () => {
    saveForLaterPage.clickSavedItemsHeaderIcon();
});

When('I should see the added product on wishlist page', () => {
    saveForLaterPage.verifyProductInSaveForLater(data.defaultProduct.name);
});

Then('I am able to remove the product from wishlist', () => {
    saveForLaterPage.removeSavedproduct(data.defaultProduct.name);
});

Then('I clear the wishlist', async () => {
    accountPage.navigateToMySavedItem();
    await saveForLaterPage.clearWishlist();
});