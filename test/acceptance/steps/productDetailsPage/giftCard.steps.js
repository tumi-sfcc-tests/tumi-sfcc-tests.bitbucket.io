const { I, data, giftCardPage } = inject();

When('I navigate to promotional product page', () => {
    I.retry(3).amOnPage(data.productOnOffer.productURL);
});
Then('I hover over Gifting', () => {
    giftCardPage.hoverGifiting();
});
Then('I can see gift card in sub-category', () => {
    giftCardPage.verifySubCategoryGiftCard();
});
Then('I click on gift card', () => {
    giftCardPage.clickSubCategoryGiftCard();
});
Then('I move to gift card page', () => {

});
Then('I am able to see {string} on gift card page', (component) => {
    switch (component) {
        case 'gift card banner':
            giftCardPage.verifyOnGiftCardPage(component);
            break;
        case 'Tumi Gift Card header':
            giftCardPage.verifyOnGiftCardPage(component);
            break;
        case 'bread-crumbs':
            giftCardPage.verifyOnGiftCardPage(component);
            break;
        case 'all card values':
            giftCardPage.verifyOnGiftCardPage(component);
            break;
        case 'recipient name field':
            giftCardPage.verifyOnGiftCardPage(component);
            break;
        case 'sender name field':
            giftCardPage.verifyOnGiftCardPage(component);
            break;
        case 'add a message field':
            giftCardPage.verifyOnGiftCardPage(component);
            break;
        case 'add to cart cta':
            giftCardPage.verifyOnGiftCardPage(component);
            break;
        case 'Gift card balance header':
            giftCardPage.verifyOnGiftCardPage(component);
            break;
        case 'gift card num field':
            giftCardPage.verifyOnGiftCardPage(component);
            break;
        case 'pin field':
            giftCardPage.verifyOnGiftCardPage(component);
            break;
        case 'check balance cta':
            giftCardPage.verifyOnGiftCardPage(component);
            break;
        case 'gift card sample image':
            giftCardPage.verifyOnGiftCardPage(component);
            break;
    }
});
Then('I enter Gift Card Number', () => {
    giftCardPage.enterCardNum(data.giftCardDetails.giftCardNum);
});
Then('I enter Pin', () => {
    giftCardPage.enterCardPin(data.giftCardDetails.giftCardPin);
});
Then('I click on check balance button', () => {
    giftCardPage.clickCheckBalance();
});
Then('I can see gift card balance', () => {
    giftCardPage.verifyBalanace(data.giftCardDetails.balance)
});
