const { I, data, productDetailsPage } = inject();

When('I navigate to promotional product page', () => {
    I.retry(3).amOnPage(data.productOnOffer.productURL);
});

//Scene7
When('I navigate to PDP page', () => {
    I.retry(3).amOnPage(data.defaultProduct.productURL);
});

//Scene7
Then('I should see images displaying from Scene7', async () => {
    await productDetailsPage.verifyScene7Images();
});

//TurnTo
Then('I should see review and rating section', () => {
    I.scrollPageToBottom();
    I.wait(2);
    I.waitForVisible(productDetailsPage.locators.reviewRatingSection, 15);
    I.seeElement(productDetailsPage.locators.writeReview);
});

Then('Select offered product size', () => {
    productDetailsPage.selectSize(data.productOnOffer.size);
});

Then('I should see all product details', () => {
    productDetailsPage.verifyProductDetails();
});

Then('I should see discounted price of product', () => {
    productDetailsPage.verifyDiscountedProductPrice(
        data.productOnOffer.itemPrice,
        data.productOnOffer.offerPrice,
        data.productOnOffer.offerDesc
    );
});

Then('I should see Add To Cart button', () => {
    I.seeElement(productDetailsPage.locators.addToCartButton);
});

Then('I add product into the cart', () => {
    I.click(productDetailsPage.locators.addToCartButton);
    I.wait(2)
});

Then('I should see Buy It Now button', () => {
    I.seeElement(productDetailsPage.locators.buyItNowButton);
});

Then('I navigate to {string} product page and add it in the cart', (product) => {
    switch (product.toUpperCase()) {
        case 'LUGGAGE':
            I.amOnPage(data.productAccented.productURL);
            break;
        case 'LUGGAGE2':
            I.amOnPage(data.productMonogrammed.productURL);
            break;
        case 'BELT':
            I.amOnPage(data.productBelt.productURL);
            break;
        default:
            I.amOnPage(data.defaultProduct.productURL);
            break;
    }
    productDetailsPage.addToCart();
});

Then('I add product to the cart', () => {
    productDetailsPage.addToCart();
});

Then('I navigate to {string} product page', (product) => {
    switch (product.toUpperCase()) {
        case 'LUGGAGE':
            I.retry(3).amOnPage(data.productAccented.productURL);
            break;
        case 'LUGGAGE2':
            I.retry(3).amOnPage(data.defaultProduct.productURL);
            break;
        case 'BELT':
            I.retry(3).amOnPage(data.productBelt.productURL);
            break;
    }
    I.wait(2);
});

Then('I bookmarked the product', () => {
    productDetailsPage.PdpSavedItem();
});

Then('Verify product price {string}', (productPrice) => {
    productDetailsPage.verifyProductPrice(productPrice);
});


