const { I, utilities, data } = inject();

Given('I am on home page', async () => {
    I.amOnPage('/');
});

Then('I navigate to the cart page', (text) => {
    I.amOnPage(data.pageURLs.cart)
});

When('I accept the Consent Tracking Modal', () => {
    utilities.accept();
});

Then('I see the text {string}', (text) => {
    I.see(text);
});

Then('I check', () => {
    I.clickIfVisible(".user")
    I.wait(5);
});

