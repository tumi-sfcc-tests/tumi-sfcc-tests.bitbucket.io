const { I, data, utilities, searchListPage, searchList } = inject();

Then('I click on the search button on homepage', () => {
    I.click(searchListPage.locators.searchIcon);
});

Then('I enter search term', () => {
    searchListPage.searchProduct(data.searchTerm.name);
});

Then('Verify search flyout product suggestions', () => {
    searchListPage.verifyCategorySuggestion(data.searchTerm.name.toLowerCase());
});

Then('I click on the suggested item', () => {
    I.click(searchListPage.locators.suggestedItemNo1);
});

Then('I navigate to suggested slp page', () => {
    I.wait(5)
    I.see(data.searchTerm.name, searchListPage.locators.slpTitle);
});
