const { I, data, homepage, productListPage } = inject();

When('I navigate to luggage plp page', () => {
    I.click(homepage.locators.superCatgry_Luggege);
});

When('I navigate to bags plp page', () => {
    I.click(homepage.locators.superCatgry_Bags);
});

Then('I see product list available', () => {
    I.waitForVisible(productListPage.locators.productList, 10);
    I.seeElement(productListPage.locators.productList);
});

Then('Verify collection filter is working', async () => {
    await productListPage.applyCollectionFilter();
});

Then('I hover over the product and bookmarked the product', () => {
    productListPage.wishlistProduct();
});

Then('I hover over {string} I can see compare checkbox', async (product) => {
    await productListPage.verifyCompareCheckbox(product);
});
Then('I can click on compare for {string}', (product) => {
    productListPage.clickCompare(product);
});
Then('I can see {string} added in compare bar', async (product) => {
    await productListPage.verifyCompareBar(product);
});
Then('compare button is disable', () => {
    productListPage.compareDisable();
});
Then('compare button is enable and user can click', () => {
    productListPage.compareEnable();
});
Then('I navigate to compare page', () => {
    productListPage.navigateToComparePage();
});
Then('I can see remove item link in compare bar', () => {
    productListPage.verifyRemoveItemPresence();
});
Then('I click on remove item', () => {
    productListPage.clickRemoveItem();
});
Then('respective item gets removed', () => {
    productListPage.verifyRemoveItemFunction();
});
Then('I can see clear All link in compare bar', () => {
    productListPage.verifyClearAllPresence();
});
Then('I click on clear All on compare bar', () => {
    productListPage.clickClearAll();
});
Then('All the items in compare bar should be removed', () => {
    productListPage.verifyClearAllFunction();
});

//-----------Comapre Selection Page Steps------------------//

Then('I am able to same {string} on compare selection page', (product) => {
    productListPage.verifyProductOnCompare(product);
});
Then('I am able to see {string} content on compare selection page', (content) => {
    productListPage.verifyContentsOnCompare(content);
});
Then('I am able to see compared suggestion', () => {
    productListPage.verifyComparedSuggestion();
});
Then('I am able to see notes for compared suggestion', () => {
    productListPage.notesComparedSuggestion();
});
Then('I am able to add product from compared suggestion', async () => {
    await productListPage.addFromComparedSuggestion();
});
Then('I am able to see newly added product in compare selection', async () => {
    await productListPage.verifyProductAdded();
});
Then('I click on Remove item and newly added product is removed', () => {
    productListPage.removeAddedProduct();
});
Then('I am able to see continue shopping button on compare selection page', () => {
    productListPage.verifybackButton();
});
Then('I click on continue shopping', () => {
    productListPage.clickContinueShopping();
});
Then('I click on add to cart', async () => {
    await productListPage.clickAddToCart();
});
Then('Cart flyout opens', () => {
    productListPage.verifyCartFlyout();
});
Then('respective product is added to cart', async () => {
    await productListPage.productAdded();
});

Then('I click on {string} filter', async (facet) => {
    await productListPage.clickfacetedFilter(facet);
});
Then('I choose {string} {string} filter', async (facet, facetValue) => {
    await productListPage.clickfilterOption(facet, facetValue);
});
Then('I see the page header changes to {string} header', (facetValue) => {
    productListPage.verifyfacetedPageHeader(facetValue);
});
Then('I check If url contains {string} name in url', (facetValue) => {
    productListPage.verifyfacetedPageUrl(facetValue);
});
Then('I see {string} parameters in url and header remains same', (facetValue) => {
    productListPage.verifyfacetedPageHeader(facetValue);
    productListPage.verifyfacetedPageUrl(facetValue);
});
Then('I add a non facet filter', () => {
    productListPage.clickNonfacetedFilter();
});
Then('I see {string} name is removed from page header', (facetValue) => {
    productListPage.verifyPageTitle(facetValue);
});
Then('I see {string} name is removed from url', (facetValue) => {
    productListPage.verifyPageUrl(facetValue);
});
