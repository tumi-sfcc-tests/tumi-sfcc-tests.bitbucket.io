const { I, homepage, productWarranty } = inject();

Then('verify the title {string} on the page', (pageTitle) => {
    productWarranty.verifyPageTitle(pageTitle);
});