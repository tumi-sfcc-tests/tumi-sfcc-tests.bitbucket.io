const { I, data, privacyPolicyPage, homepage } = inject();

Given('I navigate to privacy policy page', () => {
    //I.retry(3).amOnPage(data.pageURLs.privacyPolicy);
    I.click(homepage.locators.footer_PrivacyPolicy);
});