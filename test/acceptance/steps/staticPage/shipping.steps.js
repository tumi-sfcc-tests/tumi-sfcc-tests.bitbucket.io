const { I, data, shippingPage } = inject();

Given('I navigate to shipping page', () => {
    I.retry(3).amOnPage(data.pageURLs.shipping);
});