const { I, data, faqPage } = inject();

Given('I navigate to the faq page', () => {
    faqPage.clickFAQinFooter();
});

When('I click each keywords in the left side panel', () => {
    faqPage.clickFAQsKeywords();
});

When('I should see relevant questions in right side panel', () => {

    I.click(faqPage.locators.keyStorePickup);
    I.seeElement(faqPage.locators.quesStorePickup);

    I.click(faqPage.locators.keyShipping);
    I.seeElement(faqPage.locators.quesShipping);

    I.click(faqPage.locators.keyOrdersStatus);
    I.seeElement(faqPage.locators.quesOrders);

    I.click(faqPage.locators.keyRepairs);
    I.seeElement(faqPage.locators.quesRepairs);

    I.click(faqPage.locators.keyTracerReg);
    I.seeElement(faqPage.locators.quesqaTracerReg);

    I.click(faqPage.locators.keyReturns);
    I.seeElement(faqPage.locators.quesReturns);

    I.click(faqPage.locators.keyProductInquiry);
    I.seeElement(faqPage.locators.quesProductInquiry);

    I.click(faqPage.locators.keyViewAll);
    I.seeElement(faqPage.locators.quesViewAll);
});

When('I should able to expand every questions', () => {
    faqPage.clickFAQsQuestions();
});