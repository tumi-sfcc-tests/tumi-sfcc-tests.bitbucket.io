const { I, data, transparencySupply } = inject();

Given('I navigate to Transparency in Supply Chain Act page', () => {
    transparencySupply.clickTransparencySupply();
});
