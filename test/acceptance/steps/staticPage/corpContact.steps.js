const { I, data, corpContactPage } = inject();

Given('I navigate to corporate contacts page', () => {
    I.retry(3).amOnPage(data.pageURLs.corpContacts);
});

Given('Verify corporate contacts redirection', () => {
    I.seeInCurrentUrl("specialmarkets");
});