const { I, data, corpContactPage, privacyPolicyPage, returnsPage, shippingPage, termsPage, transparencySupply } = inject();

Then('Verify {string} page title {string}', async (page, headerTitle) => {
    switch (page.toUpperCase()) {
        case 'CORPORATE CONTACTS':
            corpContactPage.verifyPageTitle(headerTitle);
            break;
        case 'PRIVACY POLICY':
            privacyPolicyPage.verifyPageTitle(headerTitle);
            break;
        case 'RETURNS':
            returnsPage.verifyPageTitle(headerTitle);
            break;
        case 'SHIPPING':
            shippingPage.verifyPageTitle(headerTitle);
            break;
        case 'TERMS AND CONDITIONS':
            await termsPage.verifyPageTitle(headerTitle);
            break;
        case 'TRANSPARENCY IN SUPPLY CHAIN ACT':
            transparencySupply.verifyPageTitle(headerTitle);
            break;
    }
});