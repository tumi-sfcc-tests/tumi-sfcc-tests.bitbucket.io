const { I, data, termsPage, homepage } = inject();

Given('I navigate to terms and conditions page', () => {
    // I.retry(3).amOnPage(data.pageURLs.termsAndCon);
    I.click(homepage.locators.footer_TermsCondition);
});
