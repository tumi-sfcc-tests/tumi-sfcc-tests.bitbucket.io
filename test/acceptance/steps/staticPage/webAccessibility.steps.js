const { I, data, webAccessibilityPage } = inject();

Given('I navigate to the Web Accessibility Statement page', () => {
    webAccessibilityPage.clickWebAccessibility();
});

Then('verify extrnal links on page', () => {
    webAccessibilityPage.verifyExternalLinks();
});

Then('I should see page title {string}', (pageTitle) => {
    I.waitForElement(webAccessibilityPage.locators.pageTitle, 10);
    webAccessibilityPage.verifyPageTitle(pageTitle);
});