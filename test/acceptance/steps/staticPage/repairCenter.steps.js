const { I, data, repairCenterPage } = inject();

Given('I navigate to repair centers page', () => {
    I.scrollTo(repairCenterPage.locators.footer_ServiceRepairs);
    I.click(repairCenterPage.locators.footer_ServiceRepairs);
});

Then('I should see the title in header {string}', async (headerTitle) => {
    await repairCenterPage.verifyPageTitle(headerTitle);
});
Then('I should see signin button and create account buttons', () => {
    I.seeElement(repairCenterPage.locators.signInBtn);
    I.seeElement(repairCenterPage.locators.createAccount);
});
Then('I should see request a new repair button', () => {
    I.seeElement(repairCenterPage.locators.requestRepairBtn);
});

Then('I am able to sign-in', () => {
    repairCenterPage.userSignIn(data.registerUser.email, data.registerUser.password);
    I.seeElement(repairCenterPage.locators.requestRepairBtn);
});

Then('I click create account', () => {
    I.wait(2);
    I.click(repairCenterPage.locators.createAccount);
});