const { I, data, modernSlaveryPage } = inject();

Given('I navigate to the Modern Slavery Statement page', () => {
    modernSlaveryPage.clickModernSlavery();
});

Then('Verify Slavery Statement PDF page should open', () => {
    I.seeInCurrentUrl("2023.pdf");
});  