const { I, tumiLockPage } = inject();

Then('I verify the the contents on tumi lock page', () => {
    tumiLockPage.verifyTumiLockPage();
});