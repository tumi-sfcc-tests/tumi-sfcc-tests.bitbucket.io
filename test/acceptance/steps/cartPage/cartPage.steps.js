const { I, data, cartPage } = inject();
let quant;

When('I am able to edit the cart and see details', () => {
    cartPage.checkEditCart();
    cartPage.closeEditCart();
});

When('I click cart icon in the header', () => {
    cartPage.clickHeaderCartIcon();
});

When('I click checkout now on the cart page', () => {
    cartPage.clickCheckout();
});

When('I click edit cart', () => {
    cartPage.checkEditCart();
});

Then('I select {string} color variant', (color) => {
    cartPage.editCartSelectColor(color);
});

Then('Verify color appears {string} for added products', async (color) => {
    await cartPage.verifyProductsColorOnCartPage(color);
});

When('I update product quantity by {int}', (count) => {
    cartPage.editCartAddProduct(count);
    I.say(count);
});

Then('Verify product count {string} on the cart page', async (count) => {
    cartPage.verifyCartPagePrductCount(count);
});

When('I close the cartFlyout', () => {
    cartPage.closeCartFlyOut();
});

Then('I should see the {string} product on cart flyout', async (product) => {
    switch (product.toUpperCase()) {
        case 'LUGGAGE':
            cartPage.verifyDetailsOnCartFlyOut(data.productAccented.name);
            break;
        case 'LUGGAGE2':
            cartPage.verifyDetailsOnCartFlyOut(data.productMonogrammed.name);
            break;
        case 'BELT':
            cartPage.verifyDetailsOnCartFlyOut(data.productBelt.name);
            break;
        default:
            cartPage.verifyDetailsOnCartFlyOut(data.defaultProduct.name);
            break;
    }
});

Then('I should see the {string} product in the cart', async (product) => {
    cartPage.clickHeaderCartIcon();
    switch (product.toUpperCase()) {
        case 'LUGGAGE':
            I.see(data.productAccented.name, cartPage.locators.cartProductsSection);
            break;
        case 'LUGGAGE2':
            I.see(data.productMonogrammed.name, cartPage.locators.cartProductsSection);
            break;
        case 'BELT':
            I.see(data.productBelt.name, cartPage.locators.cartProductsSection);
            break;
        default:
            I.see(data.defaultProduct.name, cartPage.locators.cartProductsSection);
            break;
    }
});

Then('I should see items added as guest in the cart', () => {
    cartPage.checKAddedItemOnCartFlyout(data.productAccented.name);
});

Then('I clear the existing cart', async () => {
    await cartPage.clearCart();
    //await cartPage.clearSaveForLater();
});

When('I click save for later', () => {
    cartPage.clickSaveForLater();
});

Then('I add a gift message on a product as given: to {string}, from {string} and gift message {string}', (toPerson, fromPerson, giftMessage) => {
    cartPage.addGiftOptions(toPerson, fromPerson, giftMessage);
});
Then('Gift message should be added to product', () => {
    cartPage.verifyAddedGiftOptions();
});
Then('I remove the added gift Options', () => {
    cartPage.removeGiftOptions();
});
Then('Gift message should be removed', () => {
    cartPage.verifyGiftOptionsRemoved();
});
Then('I click on create a account link on cart page', () => {
    cartPage.clickCreateAcc();
});
Then('I click on sign in link on cart page', () => {
    cartPage.clickSignIn();
});
Then('I clear the cart', () => {
    cartPage.cleanCart();
});



