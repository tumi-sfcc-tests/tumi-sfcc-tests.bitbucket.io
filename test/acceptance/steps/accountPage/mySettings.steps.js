const { data, accountPage } = inject();

Then('I update personal details fields', () => {
    accountPage.editPersonalDetails();
});

Then('I reset the personal details fields', () => {
    accountPage.resetPersonalDetails();
});

Then('I should see the updated personal details', () => {
    accountPage.verifyUpdatedDetails();
});

Then('shopper clicks edit password', () => {
    accountPage.clickEditPassword();
});

Then('shopper changes their password', () => {
    accountPage.changePassword(data.login.password, data.account.newPassword);
});

Then('I change the password', () => {
    accountPage.changePassword(data.updateProfile.password, data.updateProfile.newPassword);
});

Then('I reset the old password', () => {
    accountPage.changePassword(data.updateProfile.newPassword, data.updateProfile.password);
});
