const { I, myPurchasePage, utilities, data } = inject();

Then('I click on my account icon', () => {
    myPurchasePage.clickOnAccountIcon();
});
Then('I click on my purchase', () => {
    myPurchasePage.clickOnMyPurchase();
});
Then('I navigate to my purchase page', () => {
    myPurchasePage.navigationPurachsePage();
});
Then('I see list of orders on my purchase page', async () => {
    await myPurchasePage.verifyOrderList();
});
Then('I am able to see load more button', () => {
    myPurchasePage.viewLoadMore();
});
Then('I click on Load more button', async () => {
    await myPurchasePage.clickLoadMore();
});
Then('I see more orders are shown in purchase list', async () => {
    await myPurchasePage.verifyLoadMore();
});
Then('I am able to see product recommendation in purchase list page', () => {
    myPurchasePage.recommendationInPurchaseList();
});
Then('I click on view details of first order in list', () => {
    myPurchasePage.clickFirstOrder();
});
Then('I navigate order details of that order', () => {
    myPurchasePage.firstOrderDetail();
});
Then('I am able see product recommendation in detail page', () => {
    myPurchasePage.recommendationInorderDetail();
});
Then('I see search bar to search my order', () => {
    myPurchasePage.verifySearchBar();
});
Then('I click on search bar', () => {
    myPurchasePage.clickSearch();
});
Then('I enter an order id and click on submit icon', () => {
    myPurchasePage.enterOrderId(data.UserWithPurchase.order3);
});
Then('I can find order id in purchase list', () => {
    myPurchasePage.findInSearchList(data.UserWithPurchase.order3);
});
Then('I am able to see product recommendation in purchase list', () => {
    myPurchasePage.recommendationInsearchResult();
});
Then('I am able to see Tracking details link for order', () => {
    myPurchasePage.verifyTrackLink();
});
Then('I click on Tracking details link', () => {
    myPurchasePage.clickTrackLink();
});
Then('I navigate to Tracking page', () => {
    myPurchasePage.navigationTrackingPage();
});
Then('I enter the CVV in payment page', () => {
    myPurchasePage.enterCVV();
});
Then('I can see new Order created', async () => {
    await myPurchasePage.verifyNewlyCreatedOrder();
});
Then('I should see the new order in purchase list', () => {
    myPurchasePage.verifyOrderOnList();
});
Then('I click on my Account icon', () => {
    myPurchasePage.clickMyAccIcon();
});
Then('I verify total price matches the price on confirmation page', () => {
    myPurchasePage.verifyTotalPriceOfOrder();
})

