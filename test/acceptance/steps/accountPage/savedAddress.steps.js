const { util } = require("chai");

const { I, data, accountPage, utilities } = inject();

Then('I click add new address', () => {
    accountPage.clickAddAddress();
});
Then('I click save new address', () => {
    I.click(accountPage.locators.addAddressBtn);
});

Then('I click add new Payment', () => {
    accountPage.clickAddPayment();
});

Then('I click cancel', () => {
    accountPage.clickCancel();
});

Then('I fill in new address details', () => {
    accountPage.addNewAddress(data.newAddress.fName, data.newAddress.lName, data.newAddress.address1,
        data.newAddress.state, data.newAddress.city, data.newAddress.zip, data.newAddress.phone);
});

Then('I fill in first address details', () => {
    accountPage.addNewAddress(data.default.fName, data.default.lName, data.default.address1,
        data.default.state, data.default.city, data.default.zip, data.default.phone);
});

Then('Verify added new Address', () => {
    accountPage.verifyNewAddress(data.newAddress.fName, data.newAddress.lName, data.newAddress.address1,
        data.newAddress.stateAbr, data.newAddress.city, data.newAddress.zip, data.newAddress.phone);

});
Then('Verify added address appears as default', () => {
    accountPage.verifyDefaultAddress(data.default.fName, data.default.lName, data.default.address1,
        data.default.stateAbr, data.default.city, data.default.zip, data.default.phone);

});
Then('Verify the address form error messages', () => {
    accountPage.addressFormErrorMessages();
});

Then('I delete all saved cards', async () => {
    await accountPage.deleteAllSavedCards();
});
Then('I delete all the addresses', async () => {
    await accountPage.deleteAllAddresses();
});

Then('make saved address as default', async () => {
    await accountPage.editMakeDefault();
});

Then('Verify editing the default saved address', async () => {
    I.click(accountPage.locators.savedDefaultAddressEdit);
    //accountPage.editAddress(data.newAddress.fName, data.newAddress.address1);
    accountPage.editAddress("firstname " + await utilities.randomText(), "address1 " + await utilities.randomText());
});

Then('Verify editing the normal saved address', async () => {
    I.click(accountPage.locators.savedAddressEdit);
    //accountPage.editAddress(data.newAddress.fName, data.newAddress.address1);
    accountPage.editAddress("firstname " + await utilities.randomText(), "address1 " + await utilities.randomText());
});