const productDetailsPage = require("../../pages/productDetails.page");

const { I, accountPage, homepage, utilities } = inject();

Then('I navigate to my settings page', () => {
    accountPage.navigateToMySettings();
});

Then('I navigate to my saved item page', () => {
    accountPage.navigateToMySavedItem();
});

Then('I verify the redirection of {string} menu', (menu) => {

    accountPage.viewMyAccountMenuFlyout();
    const titleLocator = ".account-title";
    switch (menu) {
        case "My Account":
            I.click(accountPage.locators.profileList_MyAccount);
            I.waitForElement(titleLocator, 10);
            I.see('My Account');
            I.seeInCurrentUrl('/account');
            break;
        case "My Settings":
            I.click(accountPage.locators.profileList_MySettings);
            I.waitForElement(titleLocator, 10);
            I.see('My Settings');
            I.seeInCurrentUrl('/Account-MySettings');
            break;
        case "Manage Purchases":
            I.click(accountPage.locators.profileList_MyPurchases);
            I.waitForElement(titleLocator, 10);
            I.see('Manage Purchases');
            I.seeInCurrentUrl('/order-history');
            break;
        case "My Saved Items":
            I.click(accountPage.locators.profileList_MySavedItem);
            I.wait(3);
            I.seeElement('.wishlist-title');
            I.seeInCurrentUrl('/saved-items');
            break;
        case "Register My TUMI":
            I.click(accountPage.locators.profileList_RegisterMyTumi);
            I.wait(3);
            I.see('TUMI Tracer');
            I.seeInCurrentUrl('/Tracer-List');
            break;
        case "Repair Services":
            I.click(accountPage.locators.profileList_Repairs);
            I.wait(3);
            I.seeElement("//h1[contains(text(),'Service and Repairs')]");
            I.seeInCurrentUrl('/repair-services-guest');
            break;
        default:
            break;
    }
});

When('I select the {string} of shopping preferences', (option) => {
    accountPage.selectSavePreference(option);
});

Then('Verify {string} is the selected preference', (option) => {
    accountPage.verifyselectedPreference(option);
});

Then('Verify the shopping preferences options', () => {
    accountPage.savedPreferences();
});

Then('I select {string}', (tab) => {
    accountPage.mySettingsSelectPage(tab);
});
Then('I opt in communication preference', () => {
    accountPage.checkCommPref();
});
Then('I opt out communication preference', () => {
    accountPage.uncheckCommPref();
});
Then('Verify opt in status', () => {
    accountPage.verifyCommOptInUpdated();
});
Then('Verify opt out status', () => {
    accountPage.verifyCommOptOutUpdated();
});

//--------------------------------------------------------------------------------------------------//

Then('I can see create mongram button', () => {
    accountPage.createMonogramBtn();
});
Then('I click on create monogram and save as my monogram', () => {
    accountPage.clickCreateMonogram();
});
Then('my monogram is created and i can see update monogram button', () => {
    accountPage.verifyMymonogramCreation();
});
Then('I click on update monogram', () => {
    accountPage.updateMonogramBtn();
});
Then('I see monogram modal open and i am able to update my monogram', () => {
    accountPage.updateMyMonogram();
});
Then('I see updated monogram in my monogram', () => {
    accountPage.monogramUpdated();
});
Then('I see my monogram while creating monogram from pdp', () => {
    productDetailsPage.seeMymonogramPDP();
});
Then('I see delete monogram button', () => {
    accountPage.seedeleteMonogramBtn();
});
Then('I click on delete monogram button', () => {
    accountPage.deleteMymonogram();
});
Then('I see my monogram is removed', () => {
    accountPage.verifyMymonogramRemoved();
});
Then('I dont see my monogram while creating monogram from pdp', () => {
    productDetailsPage.dontSeeMymonogramPDP();
});