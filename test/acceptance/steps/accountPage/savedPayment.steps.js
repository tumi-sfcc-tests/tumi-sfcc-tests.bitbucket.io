const { I, data, accountPage, utilities } = inject();

Then('I click to add new payment', () => {
    accountPage.clickAddPayment();
});

Then('I fill in the card details', async () => {

    // await accountPage.deleteAllAddresses();
    // for (let i = 0; i < 50; i++) {
    //     accountPage.addPayment(data.newAddress.fName,
    //         // data.newAddress.ccNum,
    //         await utilities.randomCARDGenerator(),
    //         data.newAddress.expMonth,
    //         data.newAddress.expYear,
    //         data.newAddress.ccSecCode,
    //         data.newAddress.address1,
    //         data.newAddress.city,
    //         data.newAddress.state,
    //         data.newAddress.zip,
    //         data.newAddress.phone);
    //     accountPage.mySettingsSelectPage("SAVED PAYMENT");
    //     accountPage.clickAddPayment();
    // }

    accountPage.addPayment(data.newAddress.fName,
        data.newAddress.ccNum,
        data.newAddress.expMonth,
        data.newAddress.expYear,
        data.newAddress.ccSecCode,
        data.newAddress.address1,
        data.newAddress.city,
        data.newAddress.state,
        data.newAddress.zip,
        data.newAddress.phone);
});

Then('Verify card is added', () => {
    accountPage.verifyAddedCard(data.newAddress.fName, data.newAddress.zip);
});

Then('I delete all the payments', async () => {
    await accountPage.deleteAllAddresses();
});

Then('Verify error messages', () => {
    accountPage.errorMessagesPayment();
});

Then('I fill in credit card details by making it as default payment', () => {
    accountPage.addDefaultPayment(data.default.fName,
        data.default.ccNum,
        data.default.expMonth,
        data.default.expYear,
        data.default.ccSecCode,
        data.default.address1,
        data.default.city,
        data.default.state,
        data.default.zip,
        data.default.phone);
});

Then('I click on Make Default Payment', () => {
    accountPage.makeDefaultPayment();
});
