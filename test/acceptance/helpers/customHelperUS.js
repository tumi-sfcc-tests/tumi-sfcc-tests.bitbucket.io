const Helper = require('@codeceptjs/helper');
const wdioConf = require('../../../wdio.conf');
const codeceptConf = require('../../../codecept.conf');
const commonConf = require('../../../commonConf.js');
const utilities = require('../../utils/utilities');
const I = actor();

class Custom extends Helper {

    _before() {
        I.retry(2).amOnPage(commonConf.yottaaWhitelistURL);

        // To handle targeted modal
        // utilities.closeCountrySelectionModal();
        // I.amOnPage(commonConf.yottaaWhitelistURL);

        // For US(Running from pipeline) - To handle free shipping modal
        utilities.closeFreeShippingPopup();
        I.amOnPage(commonConf.yottaaWhitelistURL);
    }

    _failed(test) {
        // console.log(test);
        test.retries(2);
    };

    // _finishTest() {
    //     wdioConf.onComplete(codeceptConf);
    // }

    /**
     * @protected
     */
    // _after() {
    //     console.log('inside helper -- after test');
    // }

    // add custom methods here
    // If you need to access other helpers
    // use: this.helpers['helperName']

    // _init - before all tests
    // _finishTest - after all tests
    // _before - before a test
    // _after - after a test
    // _beforeStep - before each step
    // _afterStep - after each step
    // _beforeSuite - before each suite
    // _afterSuite - after each suite
    // _passed - after a test passed
    // _failed - after a test failed
}

module.exports = Custom;
