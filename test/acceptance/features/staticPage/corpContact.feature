Feature: Static Page: Corporate Contacts
    As a shopper, I want to browse the corporate contacts page

    @staticPage @corporateContacts @sanity @NotInCA
    Scenario: Verify corporate contacts page link redirection in footer
        Given I am on home page
        When I click link "CORPORATE GIFTS & INCENTIVES" in the footer
        Then Verify corporate contacts redirection