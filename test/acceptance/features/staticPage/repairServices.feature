Feature: Static Page: Repair Centers
    As a shopper, I want to browse the on repair centers page

    # Background: Navigate to the repair services page from footer
    #     Given I am on home page
    #     When I click link "SERVICE & REPAIRS" in the footer

    @staticPage @repairCenter @sanity
    Scenario: Verify Repair centers page link redirection in footer
        Given I am on home page
        When I click link "SERVICE & REPAIRS" in the footer
        Then I should see the title in header "Service and Repairs"

    @staticPage @repairCenter1 @sanity
    Scenario: As a guest shopper, I see sign in and create account options
        Given I am on home page
        When I click link "SERVICE & REPAIRS" in the footer
        Then I should see signin button and create account buttons

    @staticPage @repairCenter @sanity
    Scenario: As a registered shopper, I see request a repair and repair status buttons
        Given I am logged into the website
        When I click link "SERVICE & REPAIRS" in the footer
        Then I should see request a new repair button

    @staticPage @repairCenter @sanity
    Scenario: As a shopper, I am able to sign in on repair services page
        Given I am on home page
        When I click link "SERVICE & REPAIRS" in the footer
        Then I am able to sign-in

    @staticPage @repairCenter @createAccount @sanity
    Scenario: As a shopper, I am able to create account on repair services page
        Given I am on home page
        When I click link "SERVICE & REPAIRS" in the footer
        Then I click create account
        When I fill in registration form with new email
        And clicks on create account button
        Then I see email verification sent message




