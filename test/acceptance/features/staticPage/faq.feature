Feature: Static Page: FAQ
    As a shopper, I navigates on all the keywords and their questions on FAQs page

    @staticPage @FAQ @sanity
    Scenario:  Verify FAQ page link redirection in footer
        Given I am on home page
        When I click link "FAQs" in the footer

    @staticPage @FAQ @sanity
    Scenario:  Verify FAQ page functionality
        Given I am on home page
        When I click link "FAQs" in the footer
        And I click each keywords in the left side panel
        Then I should see relevant questions in right side panel
        Then I should able to expand every questions

