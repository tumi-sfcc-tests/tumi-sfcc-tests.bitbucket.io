Feature: Static Page: Tumi Lock
    As a shopper, I want to browse on Tumi lock page

    @staticPage @tumiLock @sanity
    Scenario: Verify Tumi Lock page link redirection in footer
        Given I am on home page
        When I click link "SETTING YOUR TUMI LOCK" in the footer
        Then I verify the the contents on tumi lock page