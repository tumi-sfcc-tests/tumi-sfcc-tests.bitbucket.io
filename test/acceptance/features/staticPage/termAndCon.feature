Feature: Static Page: Terms and Conditions
    As a shopper, I want to browse on terms and conditions page

    @staticPage @termsAndCondition @sanity
    Scenario: Verify Terms and Conditions page link redirection in footer
        Given I am on home page
        When I click link "TERMS & CONDITIONS" in the footer
        Then Verify "TERMS AND CONDITIONS" page title "Terms and Conditions"