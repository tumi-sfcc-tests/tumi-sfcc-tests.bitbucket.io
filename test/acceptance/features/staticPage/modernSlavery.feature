Feature: Static Page: Modern Slavery Statement
    As a shopper, I want to browse the Modern Slavery Statement page

    @staticPage @modernSlavery @sanity
    Scenario: Verify Modern Slavery Statement page link redirection in footer
        Given I am on home page
        When I click link "MODERN SLAVERY STATEMENT" in the footer
        Then Verify Slavery Statement PDF page should open
