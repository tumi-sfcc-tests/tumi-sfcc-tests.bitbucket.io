Feature: Static Page: Transparency in Supply Chain Act
    As a shopper, I want to browse on Transparency in Supply Chain Act page

    @staticPage @transparencySupply @sanity @NotInCA
    Scenario: Verify Transparency in Supply Chain Act page link redirection in footer
        Given I am on home page
        When I click link "TRANSPARENCY IN SUPPLY CHAIN ACT" in the footer
        Then Verify "TRANSPARENCY IN SUPPLY CHAIN ACT" page title "California Transparency In Supply Chains Act"