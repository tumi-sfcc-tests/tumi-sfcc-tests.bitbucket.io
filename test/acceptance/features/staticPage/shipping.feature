Feature: Static Page: Shipping
    As a shopper, I want to browse on shipping page

    @staticPage @shippingPage @sanity
    Scenario: Verify Shipping page link redirection in footer
        Given I am on home page
        When I click link "SHIPPING" in the footer
        Then Verify "SHIPPING" page title "Shipping"
# And shopper should see breadcrumb "Home / Customer Service / Shipping"

