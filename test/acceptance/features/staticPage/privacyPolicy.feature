Feature: Static Page: Privacy Policy
    As a shopper, I want to browse on privacy policy page

    @staticPage @privacyPolicy @sanity
    Scenario: Verify Privacy policy page link redirection in footer
        Given I am on home page
        When I click link "PRIVACY POLICY" in the footer
        Then Verify "PRIVACY POLICY" page title "Privacy Policy"