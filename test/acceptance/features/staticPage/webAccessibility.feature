Feature: Static Page: Web Accessibility Statement
    As a shopper, I want to browse on Web Accessibility Statement page

    @staticPage @webAccessibility @sanity @NotInCA
    Scenario: Verify Web Accessibility Statement page link redirection in footer
        Given I am on home page
        When I click link "WEB ACCESSIBILITY STATEMENT" in the footer
        Then I should see page title "Tumi Digital Accessibility Statement"