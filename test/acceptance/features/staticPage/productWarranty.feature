Feature:Static Page: Product Info and Warranty
    As a shopper, I want to browse the Product Info and Warranty

    @staticPage @productWarranty @sanity
    Scenario: Verify Product info and Warranty page link redirection in footer
        Given I am on home page
        When I click link "PRODUCT INFO AND WARRANTY" in the footer
        Then verify the title "Product Information and Warranty" on the page


