Feature: Static Page: Returns
    As a shopper, I want to browse on returns page

    @staticPage @returnPage @sanity
    Scenario: Verify Return page link redirection in footer
        Given I am on home page
        When I click link "RETURNS" in the footer
        Then Verify "RETURNS" page title "Returns"