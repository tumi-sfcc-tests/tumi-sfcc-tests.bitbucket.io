Feature: Create an Account
    As a shopper, I want to be able to create an account with the site

    @createAccount @sanity
    Scenario: Verify creating an account
        Given I navigate to the login Page
        When I fill in registration form with new email
        And clicks on create account button
        Then I see email verification sent message

    @RegisterFormErrorMsg @sanity
    Scenario: Verify register form error messages
        Given I navigate to the login Page
        Then I add a new email to navigate on register page
        And clicks on create account button
        Then I should see create account form error messages