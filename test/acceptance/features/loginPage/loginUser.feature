Feature: User Login Functionality
    As a shopper, I want to be able to log into the TUMI website

    @temp
    Scenario: temp
        Then test something

    @userLogin @sanity
    Scenario: Successful login with valid credentials
        Given I am on the login page
        When I log in with valid username and password
        Then I should see my user firstname
        Then I logout the website
        Then Verify user successfully logout


    @invalidUserLogin @sanity
    Scenario: Verify the Login functionality using Invalid credentials
        Given I am on the login page
        When I attempt to log in with invalid username "tumitest7@gmail.com" and password "password"
        Then I should see an invalid user error message


# @invalidUserLogin @sanity
# Scenario: Verification of error messages for blank fields on the login flyout
#     Given I am on the login page
#     When I attempt to log in with blank username
#     Then Verify the email field error message ""
#     When I attempt to login with blank password
#     Then Verify the password field error message ""

#Social Registration | Janrain
# @socialLogin @sanity
# Scenario: Verify successful google social login
#     Given I am on home page
#     When I log into the site using google email "taushif.sapient@gmail.com" and password "Tumi@2022"
#     Then I should see my user firstname