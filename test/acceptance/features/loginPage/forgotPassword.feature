Feature: Forgot Password
    As a shopper, I am able to reset my password through forgot password

    @forgotPassword @sanity
    Scenario: Verify forget password email confirmation message and back to sign in button
        Given I navigate to the login Page
        Then I enter email and continue
        When I click forgot password
        Then I should see cancel and return to login page CTA
        When I fill out their recovery email address
        Then I should see confirmation of password reset mail sent
        When I click back to sign in button
        Then I should navigate back to sign in page

