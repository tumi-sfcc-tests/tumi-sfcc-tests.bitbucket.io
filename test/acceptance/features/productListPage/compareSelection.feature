@ComparePage
Feature: Compare Page Functionality
    As a shopper, verify Compare page functionality is working fine

    Background:
        Given I am on home page
        When I navigate to luggage plp page
        When I hover over "1st Product" I can see compare checkbox
        And I can click on compare for "1st Product"
        When I hover over "2nd Product" I can see compare checkbox
        And I can click on compare for "2nd Product"
        Then compare button is enable and user can click
        Then I navigate to compare page

    @CompareSelection @sanity
    Scenario: Verify same products are seen on compare selection page
        Then I am able to same "product 1" on compare selection page
        Then I am able to same "product 2" on compare selection page
        And I am able to see "Basic Specifications" content on compare selection page
        And I am able to see "Additional services" content on compare selection page
        And I am able to see "Alternate Views" content on compare selection page
        And I am able to see "Feature" content on compare selection page

    @AddFromSuggestion
    Scenario: Verify user can select a product from compared suggestion to compare and then remove
        And I am able to see compared suggestion
        And I am able to see notes for compared suggestion
        And I am able to add product from compared suggestion
        Then I am able to see newly added product in compare selection
        And I click on Remove item and newly added product is removed

    @Backbutton
    Scenario: Verify user returns to plp upon clicking continue shopping
        And I am able to see continue shopping button on compare selection page
        When I click on continue shopping
        Then I navigate to luggage plp page

    @AddToCartFromComparePage @sanity
    Scenario: Verify Add to cart from compare page
        When I click on add to cart
        Then Cart flyout opens
        And respective product is added to cart