Feature: Search List Page
    As a shopper, verify search and search list page functionality is working fine

    @slp @sanity
    Scenario: Search a term in global search. Verify if you see auto-suggestion and redirected correctly to SLP when user clicks on one of the suggestion.
        And I click on the search button on homepage
        And I enter search term
        Then Verify search flyout product suggestions
        When I click on the suggested item
        And I navigate to suggested slp page
        Then Verify collection filter is working




