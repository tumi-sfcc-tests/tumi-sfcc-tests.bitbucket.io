Feature: My Account: Menus navigation's
    As a registered shopper, I want to be able to click on all navigational icons under my account

    @redirection @sanity
    Scenario: Verify My Account menus link redirection
        Given I am logged into the website
        Then I verify the redirection of "My Settings" menu
        And I verify the redirection of "Manage Purchases" menu
        And I verify the redirection of "My Saved Items" menu
        And I verify the redirection of "Register My TUMI" menu
        And I verify the redirection of "Repair Services" menu
        And I verify the redirection of "My Account" menu
