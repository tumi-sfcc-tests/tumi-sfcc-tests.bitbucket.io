Feature: My purchase: Verify Order list page
    As a registered user, I am able to see list of orders on my purchase page

    Background: Order List Page
        Given I login with a user having multiple orders
        When I click on my Account icon
        And I click on my purchase
        Then I navigate to my purchase page

    @managePurchase @sanity
    Scenario:Verify the list of order available on page
        Then I see list of orders on my purchase page
        And I am able to see load more button
        Then I click on Load more button
        Then I see more orders are shown in purchase list
        And I am able to see product recommendation in purchase list page

    Scenario: Order Detail redirection
        And I click on view details of first order in list
        Then I navigate order details of that order
        And I am able see product recommendation in detail page

    Scenario: Search functionality on order list page
        Then I see search bar to search my order
        When I click on search bar
        And I enter an order id and click on submit icon
        Then I can find order id in purchase list
        And I am able to see product recommendation in purchase list

    Scenario: Verify the tracking details CTA populates tracking page
        When I click on search bar
        And I enter an order id and click on submit icon
        And I am able to see Tracking details link for order
        When I click on Tracking details link
        Then I navigate to Tracking page

