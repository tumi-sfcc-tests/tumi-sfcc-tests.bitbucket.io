Feature: My Account: Saved Payments
    As a shopper with an account, I want to be able to manage  my payment Methods

    # Background:
    #     Given I am logged into the website
    #     Then I navigate to my settings page
    #     When I select "SAVED PAYMENT"

    @savedPayment @sanity
    Scenario: Verify deleting of a saved payment
        Given I am logged into the website
        Then I navigate to my settings page
        When I select "SAVED PAYMENT"
        Then I delete all saved cards

    @savedPayment @sanity @recaptchaPayment
    Scenario: Verify adding a new payment method
        Given I am logged into the website
        Then I navigate to my settings page
        When I select "SAVED PAYMENT"
        Then I click to add new payment
        And I fill in the card details
        Then Verify card is added

    @savedPayment @sanity
    Scenario: Verify error messages of add new payment method
        Given I am logged into the website
        Then I navigate to my settings page
        When I select "SAVED PAYMENT"
        Then I click to add new payment
        Then Verify error messages

    @newPaymentMethodAsDefault
    Scenario: Verify by adding new payment method as default
        Given I am logged into the website
        Then I navigate to my settings page
        When I select "SAVED PAYMENT"
        Then I click to add new payment
        Then I fill in credit card details by making it as default payment

    @VerifyExistingPaymentAsDefault
    Scenario: Verify setting existing payment as a default payment
        Given I am logged into the website
        Then I navigate to my settings page
        When I select "SAVED PAYMENT"
        Then I click on Make Default Payment




