Feature: My Account: Saved Addresses
    As a I with an account, I want to be able to manage  my address

    # Background:
    #     Given I am logged into the website to check address functionality
    #     Then I navigate to my settings page

    @savedAddress @sanity
    Scenario: Verify the delete of a saved address
        Given I am logged into the website to check address functionality
        Then I navigate to my settings page
        When I select "SAVED ADDRESSES"
        Then I delete all the addresses

    @savedAddress @sanity
    Scenario: Save first new address, that appears as default
        Given I am logged into the website to check address functionality
        Then I navigate to my settings page
        When I select "SAVED ADDRESSES"
        Then I click add new address
        And I fill in first address details
        Then Verify added address appears as default

    @savedAddress @sanity
    Scenario: Verify save new address
        Given I am logged into the website to check address functionality
        Then I navigate to my settings page
        When I select "SAVED ADDRESSES"
        Then I click add new address
        And I fill in new address details
        And Verify added new Address

    @savedAddress
    Scenario: Cancel adding new address form
        Given I am logged into the website to check address functionality
        Then I navigate to my settings page
        When I select "SAVED ADDRESSES"
        Then I click add new address
        And I click cancel

    @savedAddress
    Scenario: Verify add new address error messages for blank input fields
        Given I am logged into the website to check address functionality
        Then I navigate to my settings page
        When I select "SAVED ADDRESSES"
        Then I click add new address
        Then I click save new address
        And Verify the address form error messages

    @savedAddress @sanity
    Scenario: Verify edit functionality for saved and default addresses
        Given I am logged into the website to check address functionality
        Then I navigate to my settings page
        When I select "SAVED ADDRESSES"
        Then Verify editing the normal saved address
        Then Verify editing the default saved address

    @savedAddress @sanity
    Scenario: Mark saved address as default
        Given I am logged into the website to check address functionality
        Then I navigate to my settings page
        Then I navigate to my settings page
        When I select "SAVED ADDRESSES"
        Then make saved address as default




