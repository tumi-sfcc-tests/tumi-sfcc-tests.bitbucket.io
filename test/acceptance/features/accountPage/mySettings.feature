Feature: My Account: Overview - Edit Personal Details
    As a shopper with an account, I want to be able to edit my profile

    # Background:
    #     Given I am logged into the website to update profile
    #     Then I navigate to my settings page

    @editProfile @sanity
    Scenario: Verify user able to update his personal details
        Given I am logged into the website to update profile
        Then I navigate to my settings page
        And I update personal details fields
        Then I should see the updated personal details
        Then I reset the personal details fields

    @changePassword @sanity
    Scenario: change the password
        Given I am logged into the website to update profile
        Then I navigate to my settings page
        When I change the password
        Then I reset the old password

    @shoppingPreferences
    Scenario: Verify all the Shopping Preferences options
        Given I am logged into the website to update profile
        Then I navigate to my settings page
        When I select "SHOPPING PREFERENCES"
        Then Verify the shopping preferences options

    @shoppingPreferences
    Scenario Outline: Verify the shopping preferences selection
        Given I am logged into the website to update profile
        Then I navigate to my settings page
        When I select "SHOPPING PREFERENCES"
        When I select the <Preference> of shopping preferences
        Then Verify <Preference> is the selected preference
        Examples:
            | Preference    |
            | "LUGGAGE"     |
            | "WOMEN BAGS"  |
            | "MEN BAGS"    |
            | "ACCESSORIES" |

    @communicationPreferences
    Scenario: Verify the opt in for email in communication preferences
        Given I am logged into the website to update profile
        Then I navigate to my settings page
        When I select "COMMUNICATION PREFERENCES"
        And I opt in communication preference
        Then Verify opt in status
        Then I opt out communication preference
        Then Verify opt out status

    @myMonogram
    Scenario: Verify user is able to add my monograms
        Given I am logged into the website to update profile
        Then I navigate to my settings page
        When I select "MY MONOGRAM"
        Then I can see create mongram button
        Then I click on create monogram and save as my monogram
        Then my monogram is created and i can see update monogram button
        When I click on update monogram
        And I see monogram modal open and i am able to update my monogram
        Then I see updated monogram in my monogram
        When I navigate to monogrammable product page
        And I see my monogram while creating monogram from pdp

    @deletemyMonogram
    Scenario: Verify user is able to delete my monogram
        Given I am logged into the website to update profile
        Then I navigate to my settings page
        When I select "MY MONOGRAM"
        And I see delete monogram button
        And I click on delete monogram button
        Then I see my monogram is removed
        When I navigate to monogrammable product page
        And I dont see my monogram while creating monogram from pdp