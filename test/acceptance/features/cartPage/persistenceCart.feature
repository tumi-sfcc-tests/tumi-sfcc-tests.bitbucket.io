Feature: Cart Persistence
    As a shopper, verify guest user persist items in cart after user login in the same session

    @clearCart @cartPersistence
    Scenario: Verify removing cart items
        Given I am on home page
        When I am logged into the website
        Then I clear the existing cart

    @cartPersistence @sanity
    Scenario: Verify guest cart items persist if shopper login
        Given I am logged into the website
        Then I clear the existing cart
        Then I logout the website
        When I navigate to 'LUGGAGE' product page and add it in the cart
        Then I should see the 'LUGGAGE' product on cart flyout
        When I sign in
        Then I should see the 'LUGGAGE' product in the cart

    @cartPersistence @sanity
    Scenario: Verify existing user cart items is merging with guest cart items
        Given I am logged into the website
        Then I clear the existing cart
        When I navigate to 'LUGGAGE2' product page and add it in the cart
        Then I should see the 'LUGGAGE2' product on cart flyout
        Then I logout the website
        When I navigate to 'LUGGAGE' product page and add it in the cart
        Then I should see the 'LUGGAGE' product on cart flyout
        When I sign in
        Then I should see the 'LUGGAGE2' product in the cart
        Then I should see the 'LUGGAGE' product in the cart