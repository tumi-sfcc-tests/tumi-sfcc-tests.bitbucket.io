Feature: Cart Flyout
    As a shopper, verify cart flyout functionality

    @cartFlyout @sanity
    Scenario: Verify product displayed on flyout on add to cart
        Given I am on home page
        When I navigate to 'LUGGAGE' product page and add it in the cart
        Then I should see the 'LUGGAGE' product on cart flyout