Feature:  Cart Page
    As a shopper, verify guest user able to edit the items in the cart

    @cartPage @sanity
    Scenario: Verify header cart icon navigates to the cart page
        Given I am on home page
        When I click cart icon in the header
        Then I see the text "There are no items in your bag."
        When I navigate to 'LUGGAGE2' product page and add it in the cart
        And  I close the cartFlyout
        When I navigate to the cart page
        Then I should see the 'LUGGAGE2' product in the cart

    @cartPage @editCart @sanity
    Scenario: Verify edit cart swatch selection and update quantity
        Given I am on home page
        When I navigate to 'LUGGAGE2' product page and add it in the cart
        Then I navigate to the cart page
        Then I should see the 'LUGGAGE2' product in the cart
        Then Verify product count "1" on the cart page
        Then I click edit cart
        Then I select "Navy" color variant
        Then I update product quantity by 2
        Then Verify product count "3" on the cart page
        Then Verify color appears "Navy" for added products

    @cartPage @classicMonogram @sanity
    Scenario: Classic Monogram: User is able to add and remove classic monogram on the cart page
        When I navigate to monogrammable product page
        Then I add product to the cart
        Then I navigate to the cart page
        Then I see monogram customization option available
        When I add classic monogram for the product
        Then I see added monogram
        When I remove added monogram
        Then Added monogram should be removed

    @cartPage @premiumMonogram @sanity
    Scenario: Premium Monogram: User is able to add and remove premium monogram on the cart page
        When I navigate to monogrammable product page
        Then I add product to the cart
        Then I navigate to the cart page
        Then I see monogram customization option available
        When I add premium monogram for the product
        Then I see added monogram
        When I remove added monogram
        Then Added monogram should be removed

    # @cartPage @accent @sanity @temptest
    # Scenario: Accent: User able to add and remove accents on the cart page
    #     Given I am on home page
    #     When I navigate to accent product page
    #     Then I add product to the cart
    #     Then I navigate to the cart page
    #     When I add an accent for the product
    #     Then I should see added accents
    #     Then I should see updated product price
    #     Then I remove added accent

    @cartPage @giftOptions @sanity
    Scenario: Gift Box Options: User able to add gift options on a product
        When I navigate to PDP page
        Then I add product to the cart
        And I navigate to the cart page
        When I add a gift message on a product as given: to "Einstine", from "Newton" and gift message "You were wrong!!"
        Then Gift message should be added to product
        When I remove the added gift Options
        Then Gift message should be removed

@cartPage @createAccountCart
Scenario: Verify Account Registration from cart page
    When I navigate to PDP page
    Then I add product to the cart
    And I navigate to the cart page
    And I click on create a account link on cart page
    When I fill in registration form with new email
    And clicks on create account button
    Then I see email verification sent message

@cartPage @signInAccountCart
Scenario: Verify user login from the cart page
    When I navigate to PDP page
    Then I add product to the cart
    And I navigate to the cart page
    And I click on sign in link on cart page
    Then I sign in
    Then I clear the cart


