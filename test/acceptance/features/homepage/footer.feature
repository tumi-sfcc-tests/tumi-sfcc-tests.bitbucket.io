Feature: Footer
    As a User, I should be able to see the footer placed at the end of all pages with an entry point to all relevant information that I would require to purchase, get brand awareness, or reach out to a specialist for support

    #Social icons redirection
    @footer  @followTUMI
    Scenario Outline: Verify TUMI social pages
        Given I am on home page
        When I click <SocialPage> icon in footer
        Then TUMI <SocialPage> social page should launch in new tab
        Examples:
            | SocialPage  |
            | "INSTAGRAM" |
            | "TIKTOK"    |
            | "FACEBOOK"  |
            | "TWITTER"   |
            | "YOUTUBE"   |
            | "PINTEREST" |

    #Newsletter subscription, add new mail everytime
    @footer  @newsletter @sanity
    Scenario: Newsletter Subscription: Add new email address
        Given I am on home page
        When I enter the already existing email
        Then I should get a message "Thank you! We will be in touch."

    #Newsletter subscription
    @footer  @newsletter
    Scenario: Newsletter Subscription: Add existing email address
        Given I am on home page
        When I enter the email "tumitest@yopmail.com"
        Then I should get a message "You are already registered"

    #Newsletter subscription
    @footer  @newsletter
    Scenario: Newsletter Subscription: Verify Invalid formate error message for invalid emails
        Given I am on home page
        When I enter the email "tumitest@.com"
        Then I should get a message "Invalid format"

    #Newsletter subscription
    @footer  @newsletter
    Scenario: Newsletter Subscription: Verify Invalid formate error message for blank email field
        Given I am on home page
        When I enter the email " "
        Then I should get a message "Invalid format"

    @footer @sanity
    Scenario: Verify All footer links are available in the footer
        Given I am on home page
        Then I should see "Footer area"
        Then I should see "TUMI logo"
        Then I should see "Customer Service"
        Then I should see "My Account"
        Then I should see "About TUMI"
        Then I should see "Get the latest news"
        Then I should see "Register yout TUMI"
        Then I should see "selected country"
        Then I should see "Footer copyright links"

#footer links redirection
# @footerLinks @incomplete
# Scenario Outline: Verify Footer link redirection
#     Given I am on home page
#     When I click link <Link> in the footer
#     Examples:
#         | Link               |
#         | "CUSTOMER SERVICE" |




