Feature: Header functionality
    As a user, I should be able to see TUMI logo on all the pages across the catalog, with an exception of the Payment Gateway Page, and this TUMI logo will be a CTA to the homepage

    #TUMI logo verification on each page
    @globalNav @sanity
    Scenario Outline: Verify global nav menus drawers and there redirection
        Given I am on home page
        When I click on <SuperCategory>
        And I should see <SuperCategory> drawer
        Then I should see <URL Contains> in page current URL
        Examples:
            | SuperCategory | URL Contains       |
            | "Luggage"     | "luggage"          |
            | "Backpacks"   | "backpacks"        |
            | "Bags"        | "bags"             |
            | "Accessories" | "accessories"      |
            | "Collections" | "collections"      |
            | "Gifts"       | "gift-ideas"       |
            | "Service"     | "customer-service" |
            | "Sale"        | "sale"             |

    # @headerMenus @drawers @sanity
    # Scenario: Verify global nav menus drawer is getting displayed on mouse over
    #     Given I am on home page
    #     Then I should see "Luggage" drawer
    #     Then I should see "Backpacks" drawer
    #     Then I should see "Bags" drawer
    #     Then I should see "Accessories" drawer
    #     Then I should see "Collections" drawer
    #     Then I should see "Gifting" drawer
    #     Then I should see "Service" drawer
    #     Then I should see "Sale" drawer


    #CH-317
    @headerIcons @sanity
    Scenario: As a guest user, Verify My Account, Search and Cart icons available in the header section
        Given I am on home page
        Then I should see TUMI logo
        Then I should see My Account, Search and Cart icons
        When I click cart icon
        Then I should navigate to cart page

    # @headerIcons @sanity
    # Scenario: For register user, Verify Saved Item icon available and functional in the header section
    #     Given I am logged into the website
    #     Then I should see saved item icon
    #     When I click saved item icon

    @header
    Scenario: Verify store in Service drawer
        Given I am on home page
        Then I should see store under service drawer



