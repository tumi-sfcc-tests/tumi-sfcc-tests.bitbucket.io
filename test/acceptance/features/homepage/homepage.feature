Feature: Homepage
    As a User, I should be able to browse all the section/components of home page

    @homepage @sanity
    Scenario: Verify homepage components
        Given I am on home page
        Then I see "HERO COMPONENT" component
        Then I see "COLLECTIONS AND CATEGORIES" component
        Then I see "TUMI AT YOUR SERVICE" component
        Then I see "STORE LOCATOR" component
        Then I see "TUMI DIFFERENCE" component
        Then I see "PIXLEE" component


    @storeLocator
    Scenario: Store Locator: Verify if user is able to change the store location
        Given I am on home page
        Then I am logged into the website with a user to verify store location
        Then I am able to see store name in footer store details
        When I click on change cta
        Then I navigate to store finder page
        Then I enter a store title that i want to search
        And I click on find store
        Then I verify the store title shown in list
        And I am able to see and click on save as my store radio button
        Then I click on tumi logo to go to home page
        And I verify save as my store saved this new store
        And I verify the store on row content
        When I hover over service in navigation bar
        Then I can see selected store title on service nav container


