Feature: Place an order and verify details on order confirmation screen
    As a shopper, I am able to place order and see details on order confirmation page

    @ocpagedetails
    Scenario: order confirmation page
        Given I am on home page
        When I navigate to accent product page
        When I add an accent for the product
        Then I add product to the cart
        Then I clicks checkout on minicart flyout
        Then I should redirect to the checkout page
        Then I fill in the guest user shipping details
        And I click Continue shipping
        Then I fill in credit card details
        When I click Continue billing
        Then I should be able to see all details on checkout page
        Then I click Place order
        Then I should see order confirmation screen
        Then I should see order number on order confirmation screen
        And I should see correct shipping address displayed on order confirmation screen
        Then I should see enable alert section on order confirmation screen
        And I should see correct product and details on order confirmation screen
        And I should see correct order total on order confirmation screen


