Feature: Checkout Shipping Page
    As a shopper, I verify checkout page shipping components

    # Background:
    #     When I navigate to 'LUGGAGE' product page and add it in the cart
    #     Then I clicks checkout on minicart flyout
    #     Then I should redirect to the checkout page

    Scenario: Verify shipping details section
        When I navigate to 'LUGGAGE' product page and add it in the cart
        Then I clicks checkout on minicart flyout
        Then I should redirect to the checkout page
        Then I fill in the guest user shipping details
        Then I am able to see "first name" filled on shipping page
        Then I am able to see "last name" filled on shipping page
        Then I am able to see "address 1" filled on shipping page
        Then I am able to see "city" filled on shipping page
        Then I am able to see "state" filled on shipping page
        Then I am able to see "zip code" filled on shipping page
        Then I am able to see "phone" filled on shipping page
        Then I am able to see "email" filled on shipping page
        Then I am able to check the email new arrival checkbox
        Then I am able to see standard shipping method selected
        Then I am able to see continue button

    @verifyShippingFields @sanity
    Scenario Outline:Verify shipping methods selection
        When I navigate to 'LUGGAGE' product page and add it in the cart
        Then I clicks checkout on minicart flyout
        Then I should redirect to the checkout page
        Then I fill in the guest user shipping details
        #Then I am able to see standard shipping method selected
        When I click on Choose expedited shipping link
        Then I am able to see "<Shipping Method>" with "<Expected Days>" and "<Shipping Charges>"
        Then I click to select "<Shipping method>" by clicking on radio button
        Then I see "<Shipping Method>" gets selected with "<Expected Days>" and "<Shipping Charges>"

        Examples:
            | Shipping Method | Expected Days     | Shipping Charges |
            | Standard Ground | 5-7 Business Days | $0.00            |
            | Second Day      | 2-4 Business Days | $75.00           |
            | Priority        | 1-2 Business Day  | $90.00           |
    # New
    # | Standard Ground | 8-10 Business Days | $0.00            |
    # | Second Day      | 5-7 Business Days  | $75.00           |
    # | Priority        | 3-4 Business Day   | $90.00           |

    @EDQ @sanity
    Scenario:Verify QAS in shipping details
        When I navigate to 'LUGGAGE' product page and add it in the cart
        Then I clicks checkout on minicart flyout
        Then I should redirect to the checkout page
        Then I click on address line 1
        Then I enter the search term to get auto-suggested address
        Then I am able to see expected address in auto suggestion list