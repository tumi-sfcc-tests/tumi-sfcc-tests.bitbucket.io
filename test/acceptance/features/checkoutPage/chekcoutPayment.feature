Feature: Checkout Payment Page
    As a shopper, I place the orders using different payment methods

    @cybersource @sanity
    Scenario: Cybersource CC: As a guest user, Verify successful order placement
        Given I am on home page
        When I navigate to 'LUGGAGE' product page and add it in the cart
        Then I clicks checkout on minicart flyout
        Then I should redirect to the checkout page
        Then I fill in the guest user shipping details
        And I click Continue shipping
        Then I fill in credit card details
        When I click Continue billing
        Then I click Place order
        Then I should see order confirmation screen
    # Then As a "guest credit card user", I should verifies all the details after placing order

    @cybersource @sanity
    #User having address and card details saved
    Scenario: Cybersource CC: As a registered user having address and card details saved, Verify successful order placement
        Given I am logged into the website for order placement
        Then I clear the existing cart
        When I navigate to 'LUGGAGE' product page and add it in the cart
        Then I clicks checkout on minicart flyout
        Then I should redirect to the checkout page
        Then I fills CVV for saved credit card
        When I click Continue billing
        Then I click Place order
        Then I should see order confirmation screen
    # Then As a "registered credit card user", I should verifies all the details after placing order

    @cybersource @sanity
    #User having address and card details saved
    Scenario: Cybersource CC: As a registered user having address and card details saved, Verify successful order placement using BuyItNow button
        Given I am logged into the website for order placement
        Then I clear the existing cart
        Then I navigate to 'LUGGAGE' product page
        When I clicks Buy It Now
        Then I should redirect to the checkout page
        Then I click Place order
        Then I should see order confirmation screen

    @paypal
    Scenario: PayPal: As a guest shopper, Verify successful PayPal order placement
        Given I am on home page
        When I navigate to 'LUGGAGE' product page and add it in the cart
        Then I clicks checkout on minicart flyout
        Then I should redirect to the checkout page
        Then I fill in the guest user shipping details
        And I click Continue shipping
        Then I select PayPal tab
        And complete the PayPal account payment
        Then I click Place order
        Then I should see order confirmation screen

    @valuetec @giftCard @NotInCA
    Scenario: Gift Card: As a guest shopper, Verify successful GC order placement
        Given I am on home page
        When I navigate to 'LUGGAGE' product page and add it in the cart
        Then I clicks checkout on minicart flyout
        Then I should redirect to the checkout page
        Then I fill in the guest user shipping details
        And I click Continue shipping
        Then I fill in gift card details
        When I click Continue billing
        When I click Place order
        Then I should see order confirmation screen

    @valuetec @giftCard
    Scenario: As a registered shopper, Verify successful GC  order placement
        Given I am logged into the website for order placement
        Then I clear the existing cart
        When I navigate to 'LUGGAGE' product page and add it in the cart
        Then I clicks checkout on minicart flyout
        Then I should redirect to the checkout page
        Then I fill in gift card details
        When I click Continue billing
        When I click Place order
        Then I should see order confirmation screen

    @valuetec @giftCard @sanity
    Scenario: As a guest shopper, Verify I am able to apply and remove gift card on the checkout page
        Given I am on home page
        When I navigate to 'LUGGAGE' product page and add it in the cart
        Then I clicks checkout on minicart flyout
        Then I should redirect to the checkout page
        Then I fill in the guest user shipping details
        And I click Continue shipping
        Then I fill in gift card details
        When I remove the applied gift card
        Then Gift card should be removed