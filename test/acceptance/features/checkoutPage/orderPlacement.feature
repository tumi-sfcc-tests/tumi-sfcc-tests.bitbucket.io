Feature: Order Placement with accents and monograms
    As a shopper, I am able to place different type of orders adding product customizations

    @creditCardOrder @sanity
    Scenario: Credit Card Order: Add a accent and place order using credit card
        Given I am on home page
        When I navigate to accent product page
        When I add an accent for the product
        Then I add product to the cart
        Then I clicks checkout on minicart flyout
        Then I should redirect to the checkout page
        Then I fill in the guest user shipping details
        And I click Continue shipping
        Then I fill in credit card details
        When I click Continue billing
        Then I click Place order
        Then I should see order confirmation screen

    @giftCardOrder @sanity @NotInCA
    Scenario: Gift Card Order: Add a premium monogram and place order using gift card
        Given I am on home page
        When I navigate to the product page
        When I add premium monogram for the product
        Then I add product to the cart
        Then I clicks checkout on minicart flyout
        Then I fill in the guest user shipping details
        And I click Continue shipping
        Then I fill in gift card details
        When I click Continue billing
        When I click Place order
        Then I should see order confirmation screen

    @payPalOrder @sanity
    Scenario: PayPal Order: Add classic monogram on the cart page and Place order using PayPal
        Given I am on home page
        When I navigate to the product page
        Then I add product to the cart
        Then I navigate to the cart page
        When I add classic monogram for the product
        Then I see added monogram
        When I click checkout now on the cart page
        Then I should redirect to the checkout page
        Then I fill in the guest user shipping details
        And I click Continue shipping
        Then I select PayPal tab
        And complete the PayPal account payment
        Then I click Place order
        Then I should see order confirmation screen


    @klarna @sanity
    Scenario: Klarna Order: Verify successful Klarna order placement
        Given I am on home page
        When I navigate to the product page
        Then I add product to the cart
        Then I clicks checkout on minicart flyout
        Then I fill in the guest user shipping details
        And I click Continue shipping
        Then I select klarna payment option
        And complete the Klarna payment
        Then I place klarna order
        Then I should see order confirmation screen

    @myPurchaseOrderDetails
    Scenario: Place an order an verify that in purchase list and order detail
        Given I login with a user to verify purchase list
        When I navigate to accent product page
        Then I add product to the cart
        Then I clicks checkout on minicart flyout
        Then I should redirect to the checkout page
        Then I enter the CVV in payment page
        When I click Continue billing
        Then I click Place order
        Then I can see new Order created
        Then I should see order confirmation screen
        Then I click on my Account icon
        Then I click on my purchase
        Then I should see the new order in purchase list
        And I click on view details of first order in list
        Then I verify total price matches the price on confirmation page

# @Sprint2 @PayPal @PayIn4 @regression
# Scenario: PayPal: As a Guest shopper, I should able to buy item using paypal pay in 4
#     Given I am on home page
#     When I navigate to product page and add it in the cart
#     Then I clicks checkout on minicart flyout
#     Then I should redirect to the checkout page
#     Then I fill in the guest user shipping details
#     And I click Continue shipping
#     Then I select PayPal tab
#     And complete the PayPal Pay in 4 payment
#     Then I click Place order
#     Then As a "guest paypal user", I should verifies all the details after placing order
