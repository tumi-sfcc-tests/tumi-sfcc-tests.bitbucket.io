Feature: Avalara Integration
  Verify the tax line items updates on checkout page

  @avalara @sanity
  Scenario: Verify zero tax for invalid address on checkout
    Given I am on home page
    When I navigate to 'LUGGAGE' product page and add it in the cart
    Then I clicks checkout on minicart flyout
    Then I should redirect to the checkout page
    Then As a guest, I fills in invalid shipping details
    And I click Continue shipping
    Then I verify that zero tax on checkout page

  @avalara @sanity @NotInCA
  Scenario: Verify zero tax for tax free state addresses on checkout
    Given I am on home page
    When I navigate to 'LUGGAGE' product page and add it in the cart
    Then I clicks checkout on minicart flyout
    Then I should redirect to the checkout page
    Then I fills in Alaska shipping details
    And I click Continue shipping
    Then I verify that zero tax on checkout page

  @avalara @sanity @NotInCA
  Scenario: Verify applicable tax reflecting for valid addresses
    Given I am on home page
    When I navigate to 'LUGGAGE' product page and add it in the cart
    Then I clicks checkout on minicart flyout
    Then I should redirect to the checkout page
    Then I fill in the guest user shipping details
    And I click Continue shipping
    Then I verify that non zero tax on checkout page
