Feature: Accents
    Shopper checks accent funtionality on PDP, Cart, Checkout, Order confirmation and order details page

    @cartPage @accent @sanity
    Scenario: User able to add and remove accents on the cart page
        Given I am on home page
        When I navigate to accent product page
        When I add an accent for the product
        Then I should see added accents
        Then I should see updated product price
        Then I remove added accent