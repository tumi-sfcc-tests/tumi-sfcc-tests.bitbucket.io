Feature: Save for later
    As a shopper, I am able to add items in save for later section

    @saveForLater @flaky
    Scenario: Guest User - Verify save for later on the cart page
        Given I am on home page
        When I navigate to the product page
        And I add product to the cart
        Then I navigate to the cart page
        When I click save for later
        And I complete the login to wishlist items
        And I click cart icon in the header
        Then I see saved items icon in header and click
        And I should see the added product on wishlist page
        Then I am able to remove the product from wishlist

    @saveForLater @cartPage @flaky
    Scenario: Registered user - Verify save for later on the cart page
        Given I am logged into the website
        And I clear the existing cart
        When I navigate to the product page
        And I add product to the cart
        Then I navigate to the cart page
        When I click save for later
        Then I see saved items icon in header and click
        And I should see the added product on wishlist page
        Then I am able to remove the product from wishlist

    # @saveForLater @plp @incomplete
    # Scenario: Registered user - Verify save for later on PLP page
    #     Given I am logged into the website
    #     When I navigate to luggage plp page
    #     And I hover over the product and bookmarked the product
    #     Then I should see the added product on wishlist page
    #     Then I am able to remove the product from wishlist

    @saveForLater @pdp @@flaky
    Scenario: Guest user - Verify save for later on PDP page
        Given I am on home page
        When I navigate to the product page
        And I bookmarked the product
        Then I complete the login to wishlist items
        And I should see the added product on wishlist page
        Then I am able to remove the product from wishlist

    @saveForLater @clearWishlist
    Scenario: Registered user - Verify I am able to clear the wishlist items
        Given I am logged into the website
        Then I navigate to my saved item page
        And I clear the wishlist