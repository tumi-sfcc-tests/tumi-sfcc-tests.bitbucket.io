Feature: Gift Card Functionality
    As a shopper, I should be able to purchase gift cards
    Background:
        Given I am on home page
        And I hover over Gifting
        Then I can see gift card in sub-category
        And I click on gift card

    @giftCardPageUI
    Scenario: navigation and component on Gift card page
        Then I move to gift card page
        Then I am able to see "gift card banner" on gift card page
        And I am able to see "Tumi Gift Card header" on gift card page
        And I am able to see "bread-crumbs" on gift card page
        And I am able to see "all card values" on gift card page
        And I am able to see "recipient name field" on gift card page
        And I am able to see "sender name field" on gift card page
        And I am able to see "add a message field" on gift card page
        And I am able to see " add to cart cta" on gift card page
        And I am able to see "Gift card balance header" on gift card page
        And I am able to see "gift card num field" on gift card page
        And I am able to see "pin field" on gift card page
        And I am able to see "check balance cta" on gift card page
        And I am able to see "gift card sample image" on gift card page

    # Scenario Outline: Select a card value and add to cart
    #     And I select a card "<value>" to add to cart
    #     Then I enter "Recipient Name" for gift card
    #     Then I enter "Sender Name" for gift card
    #     Then I enter "Message" for gift card
    #     And I click on add To Cart to add gift card
    #     Then I see add to cart flyout should open
    #     Then I see same card details on cart flyout that is added
    #     Examples:
    #         | value |
    #         | $25   |
    #         | $500  |
    
    @giftCardBalance
    Scenario: Check Gift Card Balance
        And I enter Gift Card Number
        And I enter Pin
        Then I click on check balance button
        Then I can see gift card balance