Feature: PDP Components

    @pdp @360View @sanity
    Scenario: Verify 360 degree view components on Product Page
        Given I am on home page
        When I navigate to 360 degree view product page
        Then Verify product 360 degree view is available

    @pdp @video @sanity
    Scenario: Verify product video is available for the product
        Given I am on home page
        When I navigate to the product page
        Then Verify product video is available

    #-----------------------------------------------------#

    @pdp @IncludeWithPurchase @sanity
    Scenario: Verify include with purchase components on Product Page
        When I navigate to the product page
        Then Verify the include with purchase

    #-----------------------------------------------------#

    @pdp @specification @sanity
    Scenario: Verify Specifications Details on Product Page
        When I navigate to the product page
        And I click on specifications of product page
        Then Verify the specification Details

    #-----------------------------------------------------#

    @pdp @Maintenance @sanity
    Scenario: Verify Maintenance and care details on PDP
        When I navigate to the product page
        And I click on MaintenanceAndCare of product page
        Then Verify the MaintenanceAndCare Details

    #-----------------------------------------------------#

    @pdp @airlineGuide @sanity
    Scenario: Verify Airline Guide details on PDP
        Given I am on home page
        When I navigate to the product page
        And I click on AirLineGuide of product page
        Then Verify the AirLineGuide Details

    @pdp @airlineGuide @sanity
    Scenario: Verify Airline Guide unit switching is working
        When I navigate to the product page
        And I click on AirLineGuide of product page
        Then I am able to change size units

    # @pdp @airlineGuide @sanity
    # Scenario: Verify browse all Airline Guide list
    #     Given I am on home page
    #     When I navigate to the product page
    #     When I click on AirLineGuide of product page
    #     Then I am able to browse all the AirlineGuide list

    # #-----------------------------------------------------#

    @pdp @stickyComponent @sanity
    Scenario: Verify ATC button on sticky component
        When I navigate to the product page
        Then Verify the product price on sticky ATC
        Then Verify add to cart is functional

    @pdp @stickyComponent @sanity
    Scenario: Verify View options on sticky component
        When I navigate to the product page
        Then Verify the View option button is functional

    @pdp @recommendation @sanity
    Scenario: Verify recommendation section is available down below the page
        When I navigate to the product page
        Then Verify the recommendation component is available







