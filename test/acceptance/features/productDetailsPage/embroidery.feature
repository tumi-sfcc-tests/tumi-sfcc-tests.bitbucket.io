Feature: Embroidery
    Shopper checks embroidery functionality on PDP
    #NOTE: Font, Initial Symbols, Color are passed in number from feature file. All these number depicts the postion in which you view respective Font, Initial Symbols, Color.

    @embroidery
    Scenario: Verify user can add embroidery to product
        Given I am on home page
        Then I go to an Embroidery product
        And I click on add to add Embroidery
        Then I see embroidery modal open
        And I verify note for instore pickup on Embroidery modal
        Then I choose style "1" font
        Then I click on next button on "1 of 3"
        Then I navigate to page "2 of 3" of embroidery modal
        And I enter symbol "1" initials
        Then I click on next button on "2 of 3"
        Then I navigate to page "3 of 3" of embroidery modal
        And I select font color "1"
        Then I click on Apply button to apply embroidery
        Then I see embroidery added to product with initial "1"

    @embroidery
    Scenario: Embroidery modal Cancel and Previous button functionality
        Given I am on home page
        Then I go to an Embroidery product
        And I click on add to add Embroidery
        Then I choose style "1" font
        Then I click on next button on "1 of 3"
        And I enter symbol "1" initials
        Then I click on next button on "2 of 3"
        And I select font color "1"
        Then I click on previous on embroidery modal on page "3 of 3"
        Then I navigate to page "2 of 3" of embroidery modal
        Then I click on previous on embroidery modal on page "2 of 3"
        Then I navigate to page "1 of 3" of embroidery modal
        Then I click on cancel on embroidery modal
        Then I see embroidery is not added and modal is closed

    @embroidery @sanity
    Scenario: Edit and Remove Embroidery on the PDP
        Given I am on home page
        Then I go to an Embroidery product
        And I click on add to add Embroidery
        Then I choose style "1" font
        Then I click on next button on "1 of 3"
        And I enter symbol "1" initials
        Then I click on next button on "2 of 3"
        And I select font color "1"
        Then I click on Apply button to apply embroidery
        Then I see embroidery added to product with initial "1"
        #-------------------Editing------------------------------------------#
        Then I can see Edit button for embroidery
        Then I click on Edit button
        Then I choose style "2" font
        Then I click on next button on "1 of 3"
        And I enter symbol "2" initials
        Then I click on next button on "2 of 3"
        And I select font color "2"
        Then I click on Apply button to apply embroidery
        Then I see embroidery added to product with initial "2"
        #-------------------Removing----------------------------------------#
        Then I see remove button for embroidery
        Then I click on remove button
        Then I see embroidery is removed

    @embroidery
    Scenario: Close button functionality on embroidery modal
        Given I am on home page
        Then I go to an Embroidery product
        And I click on add to add Embroidery
        Then I am able to see cross button
        Then I click on cross button
        Then I see embroidery is not added and modal is closed

    @embroidery @sanity
    Scenario: Add embroidered(Symbol) product to cart and verify embroidery initials on cart and then on checkout
        Given I am on home page
        Then I go to an Embroidery product
        And I click on add to add Embroidery
        Then I choose style "1" font
        Then I click on next button on "1 of 3"
        And I enter symbol "3" initials
        Then I click on next button on "2 of 3"
        And I select font color "1"
        Then I click on Apply button to apply embroidery
        Then I click on add to the cart
        Then I can see Edit button for embroidery
        Then I see remove button for embroidery
        Then I verify initial "3" on cart flyout and click on view my cart
        Then I navigate to the cart page
        Then I see embroidery added to product with initial "3"
        Then I click on checkout from cart
        Then I can see on checkout page that embroidery added to product with initial "3"

    @embroidery
    Scenario: Verify embroidery style name and Enter alphanumeric initial
        Given I am on home page
        Then I go to an Embroidery product
        And I click on add to add Embroidery
        Then I choose style "2" font
        Then I see correct name for font "2"
        Then I click on next button on "1 of 3"
        Then I enter alphanumeric value "9B" for initial
        Then I click on next button on "2 of 3"
        And I select font color "1"
        Then I click on Apply button to apply embroidery
        Then I see embroidery with initial "9 B" on PDP

    @embroidery @sanity
    Scenario: Verify added embroidery initials for the product on the order confirmation page
        Given I am on home page
        Then I go to an Embroidery product
        And I click on add to add Embroidery
        Then I choose style "2" font
        Then I click on next button on "1 of 3"
        And I enter symbol "3" initials
        Then I click on next button on "2 of 3"
        And I select font color "1"
        Then I click on Apply button to apply embroidery
        Then I click on add to the cart
        Then I verify initial "3" on cart flyout and click on view my cart
        Then I click on checkout from cart
        Then I fill in the guest user shipping details
        And I click Continue shipping
        Then I fill in credit card details
        When I click Continue billing
        Then I click Place order
        Then I should see order confirmation screen
        Then I can see on Order Confirmation page that embroidery added to product with initial "3"









