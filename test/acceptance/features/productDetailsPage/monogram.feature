Feature: Classic & Premium monograms
    Shopper checks classic monogram functionality on PDP, Cart, Checkout, Order confirmation and order details page

    @pdp @classicMonogram @sanity
    Scenario: User is able to add classic monogrammable product to the cart
        Given I am on home page
        When I navigate to monogrammable product page
        Then I see monogram customization option available
        When I add classic monogram for the product
        Then I see added monogram
        Then I am able to add monogrammable product to the cart

    @pdp @classicMonogram @sanity
    Scenario: User is able to remove classic monogram for the product
        Given I am on home page
        When I navigate to monogrammable product page
        Then I see monogram customization option available
        When I add classic monogram for the product
        Then I see added monogram
        When I remove added monogram
        Then Added monogram should be removed

    @pdp @premiumMonogram @sanity
    Scenario: User is able to add premium monogrammable product to the cart
        Given I am on home page
        When I navigate to monogrammable product page
        Then I see monogram customization option available
        When I add premium monogram for the product
        Then I see added monogram
        Then I am able to add monogrammable product to the cart

    @pdp @premiumMonogram @sanity
    Scenario: User is able to remove premium monogram for the product
        Given I am on home page
        When I navigate to monogrammable product page
        Then I see monogram customization option available
        When I add premium monogram for the product
        Then I see added monogram
        When I remove added monogram
        Then Added monogram should be removed

    @priceChangeAddPremiumMonogram @sanity
    Scenario: Verify that price doesn't change on pdp upon adding premium monogram
        Given I am on home page
        When I navigate to monogrammable product page
        Then I see monogram customization option available
        When I add premium monogram for the product
        Then I see added monogram
        Then I should not see updated product price
        When I remove added monogram
        Then Added monogram should be removed