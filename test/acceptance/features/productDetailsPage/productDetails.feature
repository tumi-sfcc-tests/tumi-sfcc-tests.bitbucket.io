Feature: Product Details Page
    Verify the product details on PDP

    @pdp @sanity
    Scenario: Verify product details and add to cart displayed on PDP
        Given I am on home page
        When I navigate to the product page
        Then I should see all product details
        Then I should see Add To Cart button

    #Buy it now product variation
    @pdp @BuyItNowVariation @sanity
    Scenario: As a registered user having address and card details saved, I am able to see Add to Cart and Buy it Now button on PDP page
        Given I am logged into the website for order placement
        When I navigate to the product page
        Then I should see Add To Cart button
        Then I should see Buy It Now button

    @pdp @turnTo @reviewSection @sanity
    Scenario: Review and Ratings: Verify review and rating section available on PDP
        Given I am on home page
        When I navigate to the product page
        Then I should see review and rating section

    @pdp @scene7 @sanity
    Scenario: Scene7: Browse PDP page and verify images coming through Scene7
        Given I am on home page
        When I navigate to the product page
        Then I should see images displaying from Scene7
