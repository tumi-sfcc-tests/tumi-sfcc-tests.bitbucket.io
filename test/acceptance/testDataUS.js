module.exports = {

    pageURLs: {
        homepage: "https://stage-us.tumi.com/?yottaa-internal-passthru",
        cart: "/cart",
        plp: "/c/sale/",
        pdp: "/p/garment-bag-tri-fold-carry-on-01171481041"
    },
    socialURLs: {
        facebook: "facebook.com/TumiTravel",
        twitter: "twitter.com/Tumitravel",
        pinterest: "pinterest.com/tumitravel/",
        instagram: "instagram.com/tumitravel/",
        youtube: "youtube.com/user/TumiTravel",
        tiktok: "tiktok.com/@tumitravel"
    },
    guestUser: {
        fName: "Guest",
        lName: "User",
        address1: "981 President St",
        country: "United States",
        state: "New York",
        stateAbr: "NY",
        city: "Brooklyn",
        zip: "11225-1102",
        phone: "908-765-4321",
        email: "testapproved@signifyd.com"
    },
    registerUser: {
        fName: "Tumi",
        lName: "User",
        address1: "1998 Perrin St",
        country: "United States",
        state: "Louisiana",
        stateAbr: "LA",
        city: "Shreveport",
        zip: "71101-2225",
        phone: "999-000-1225",
        email: "tumitest@yopmail.com",
        password: "Tumi@2022"
    },
    registerUserForOrder: {
        fName: "John",
        lName: "Smith",
        address1: "7777 Adier Rd",
        country: "United States",
        state: "Los Angeles",
        stateAbr: "LA",
        city: "Abbeville",
        zip: "70510-2421",
        phone: "911-222-3333",
        email: "testapproved@signifyd.com",
        password: "Tumi@2024"
    },
    userForSavedAddress: {
        fName: "Xyz",
        lName: "abc",
        email: "tumixyz@yopmail.com",
        password: "Tumi@2022"
    },
    taxFreeAddress: {
        fName: "Test",
        lName: "User",
        address1: "1960 Jerry Toth Drive",
        country: "United States",
        state: "Alaska",
        stateAbr: "AK",
        city: "Rest",
        zip: "99501",
        phone: "907-599-7871"
    },
    newRegisterAccount: {
        email: "tumitest11@xyz.com",
        password: "Tumi@123",
        fName: "John",
        lName: "Smith"
    },
    validLoginCred: {
        email: "stgtmus01@yopmail.com",
        password: "Test@123"
    },
    CC: {
        name: "John Smith",
        ccNum: "4111111111111111",
        ccSecCode: "555",
        expMonth: "08",
        expYear: "2028",
        ccExpDate: "8/2028",
        address1: "XYZ Presedential",
        city: "Woburn",
        state: "Massachusetts",
        zip: "01801",
        phone: "7815551211"
    },
    paypalCred: {
        email: "jorge.hernandez@publicissapient.com",
        fName: "Jorge",
        lName: "Hernandez",
        password: "Tumi8864"
    },

    klarna: {
        phone: "(908) 765-4321",
        otp: "123456"
    },

    giftCard: {
        cardNum: "7375580000049528687",
        pin: "6011"
    },
    defaultProduct: {
        productURL: "/p/small-compact-4-wheeled-brief-01396821041/",
        name: "Small Compact 4 Wheeled Brief",
        collection: "19 DEGREE",
        color: "Black",
        itemPrice: "$650.00"
    },
    productBelt: {
        productURL: "/p/monaco-leather-belt-0104842T60836/",
        name: "Monaco Leather Belt",
        collection: "BELTS",
        color: "Gun Metal/Black",
        size: "36",
        itemPrice: "$195.00"
    },
    productAccented: {
        productURL: "/p/expandable-organizer-laptop-brief-01173051041/",
        name: "Expandable Organizer Laptop Brief",
        collection: "ALPHA",
        color: "BLACK",
        itemPrice: "$625.00"
    },
    productMonogrammed: {
        productURL: "/p/search-backpack-01424801041/",
        itemPrice: "$595.00",
        collection: "ALPHA BRAVO",
        name: "Search Backpack",
        color: "Black"
    },
    product360view: {
        productURL: "/p/search-backpack-01424801596/",
    },
    productembroidered: {
        productURL: "/p/just-in-case-tote-01465892693/",
        itemPrice: "$150.00",
        note: "Please note: Embroidery is not available for items picked up in store."
    },
    productOnOffer: {
        productURL: "/p/tumipax-charlotte-packable-travel-puffer-jacket-142070.html",
        name: "TUMIPAX Charlotte Packable Travel Puffer Jacket",
        color: "BLACK",
        size: "S",
        itemPrice: "$30.00",
        offerPrice: "$27.00",
        offerDesc: "10% OFF on selected products"
    },
    updateProfile: {
        fName: "John",
        lName: "Snow",
        email: "tumitest7@yopmail.com",
        password: "Tumi@2024",
        phone: "923-577-6655",
        DOB: "07/21",
        gender: "Male",
        newfName: "Ned",
        newlName: "Stark",
        newPhone: "923-555-4433",
        newDOB: "10/21",
        newGender: "Female",
        newPassword: "Tumi@2023"
    },
    newAddress: {
        fName: "NewUserFname",
        lName: "NewUserLname",
        address1: "104 Presidential Way",
        country: "United States",
        state: "Massachusetts",
        stateAbr: "MA",
        city: "Woburn",
        zip: "01801",
        phone: "871-555-2121",
        email: "testnewaddress@yopmail.com",
        ccNum: "4111111111111111",
        ccSecCode: "555",
        expMonth: "08",
        expYear: "2028",
        ccExpDate: "8/2028"
    },
    default: {
        fName: "TstDefault",
        lName: "UsrDefault",
        address1: "1998 Perry Street",
        country: "United States",
        state: "Michigan",
        stateAbr: "MI",
        city: "Flint",
        zip: "48506",
        phone: "781-555-1212",
        email: "testdefaultaddress@yopmail.com",
        ccNum: "4111111111111111",
        ccSecCode: "555",
        expMonth: "08",
        expYear: "2028",
        ccExpDate: "8/2028"
    },
    searchTerm: {
        name: "Backpack"
    },
    giftCardDetails: {
        giftCardNum: "7018525193950000038",
        giftCardPin: "56579075",
        balance: "$9,281.75"
    },
    address: {
        term: "2019",
        suggestedLine: "2019 Amberjack Ct, Reston VA 20191"
    },
    UserWithPurchase: {
        email: "stgtmus2@yopmail.com",
        password: "Test@123",
        order3: "TUS00018868"
    },
    checkoutData: {
        productURL: "/p/organizer-brief-01173041041/",
        fName: "user",
        lName: "confirms",
        address1: "981 President St",
        country: "United States",
        state: "New York",
        stateAbr: "NY",
        city: "Brooklyn",
        zip: "11225-1102",
        phone: "546-765-4321",
        email: "checkoutTumi@yopmail.com"
    }
};