const I = actor();

module.exports = {
    locators: {
        productDetails: "//div[contains(@class,'product-detail')]",
        addToCartButton: "//div[@class='product-btn-wrapper']//button[text()='Add to Cart']",
        stickyATCButton: "//div[contains(@class,'sticky-container')]//div[@class='add-to-cart']",
        stickyViewOption: "//div[contains(@class,'sticky-container')]//a[normalize-space()='View Options']",
        buyItNowButton: "//div[@id='sub-container']//button[contains(@class,'buy-it-now')]",
        miniCartIcon: "//div[@class='minicart']",
        miniCartCheckoutButton: "//div[@id='flyout-minicart']//a[contains(text(), 'Checkout')]",
        originalStrikePrice: "//span[@class='strike-through list']",
        itemPrice: ".prices-add-to-cart-actions .price .sales .value",
        promoOfferDesc: "//div[@class='promo-section']",
        rating: "//div[@class='ratings']",
        sizeChart: "//div[@class='size-container']",
        colorChart: "//button[contains(@class,'color-attribute')]",
        productName: "//h1[@class='product-name']",

        cartFlyoutViewMyCartBtn: "//a[contains(text(),'Checkout Now')]",

        featuresTab: "//a[@id='featuresTab']",
        specificationsTab: "//a[@id='specificationsTab']",
        maintenanceAndCareTab: "//a[@id='maintenanceandcareTab']",
        compatibleAirlineGuideTab: "//a[@id='compatibleairlineguideTab']",

        AirlineGuideMetric: "label[for='metric']",
        AirlineGuideImperial: "label[for='imperial']",

        exterior: "//h4[normalize-space()='Exterior']",
        browseAirLineData: "(//button[@id='airlinesModalData'])[1]",

        priceTag: "//div[@class='prices']//span[@class='value']",
        includeWithPurchase: "#services-included-with-purchase>h3",
        description: "//p[@class='description']",
        reviewRatingSection: "//div[@id='tt-reviews-summary']",
        writeReview: "//button[contains(text(),'Write a Review')]",
        primary: "(//a[contains(@class,'button button--primary w-100')][normalize-space()='primary'])[2]",
        shipToMe: "(//span[normalize-space()='Ship to me'])[1]",

        pdp360ViewSection: "//div[@class='product_360_enable']",
        // pdpVideoSection: "//div[@id='product-video']",
        pdpVideoSection: "//div[@id='product-video']/../../video",
        pdpPixlee: "//div[@id='widget_header']",
        pdpAccentSection: "//div[contains(@class,'accent-item-wrapper')]",
        pdpMonogramSection: "//div[contains(@class,'add-monogram')]",
        pdpRecommendation: ".rfk-carosuel-container",
        accents_addBtn: ".add-accent-link",
        accents_editBtn: "//a[@class='edit-accent-link']",
        accents_removeBtn: "//a[@class='remove-accent-link']",
        accents_flyoutModal: "//div[contains(@id,'accentModal')]",
        accents_select: "//div[contains(@id,'accentModal')]//button[contains(@class,'accents-button')]",
        accents_apply: "//div[contains(@id,'accentModal')]//div[@class='accent-btn-cntr']",
        accents_cancel: "//div[contains(@id,'accentModal')]//a[text()='Cancel']",
        accents_addedDesc: ".accent-item-wrapper .title",

        classicMonogramFormLocator: "//form[contains(@class,'monogram-form')]",

        monogram_addBtn: "//a[@class='add-monogram-modal']",
        monogram_editBtn: "//a[contains(@class,'edit-monogram')]",
        monogram_removeBtn: "//a[contains(@class,'remove-monogram')]",
        monogram_flyoutModal: "//div[contains(@id,'monogramModal')]",
        monogram_addClassicLink: "//a[@class='classic-modal-btn']",
        //monogram_applyClassic: "//form[contains(@class,'monogram-form')]//a[contains(@class,'monogram-step-button-appl')]",
        monogram_applyClassic: "//div[contains(text(),'Apply')]",

        monogram_classicPreviousBtn: "//form[contains(@class,'monogram-form')]//a[contains(@class,'previousBtn')]",
        monogram_classicNextBtn: "//form[contains(@class,'monogram-form')]//a[contains(@class,'monogram-step-button-next')]",
        monogram_classicInitialsChar: "//form[contains(@class,'monogram-form')]//input[@name='monogramText']",

        monogram_classicSelectFont: "//form[contains(@class,'monogram-form')]//div[contains(@class,'monogram-fonts')]/label",
        monogram_classicSelectFontColor: "//form[contains(@class,'monogram-form')]//div[contains(@class,'monogram-fontColor')]/label",
        monogram_classicTagSelect: "//form[contains(@class,'monogram-form')]//div[@class='placement']/label[1]",
        monogram_classicPatchSelect: "//form[contains(@class,'monogram-form')]//div[@class='placement']/label[2]",
        monogram_classicBothSelect: "//form[contains(@class,'monogram-form')]//div[@class='placement']/label[3]",

        monogram_addPremiumLink: "//a[@class='premium-modal-btn']",
        monogram_premiumPreviousBtn: "//form[@class='premium-monogram-form']//a[contains(@class,'previousBtn')]",
        monogram_premiumNextBtn: "//form[@class='premium-monogram-form']//a[contains(@class,'monogram-step-button-next')]",
        monogram_premiumNextBtnStep2: "//form[@class='premium-monogram-form']//div[contains(@class,'monogram_2')]//a[contains(@class,'monogram-step-button-next')]",
        monogram_premiumInitialsChar: "//form[@class='premium-monogram-form']//input[@name='monogramText']",
        monogram_premiumSelectFontColor: "//form[@class='premium-monogram-form']//div[contains(@class,'monogram-fontColor')]/button",
        monogram_premiumSelectPatch: "//form[@class='premium-monogram-form']//label[@class='option option-2']",
        monogram_applyPremium: "//form[@class='premium-monogram-form']//a[contains(@class,'monogram-step-button-appl')]",
        monogram_page4hearder: "(//div[@class='monogram-wrapper'])[4]/div[1]",

        savedItemIcon: "//div[@class='product-bookmark']//a[@class='bookmark']",
        removeSavedItem: "//div[@class='product-bookmark']//a[@class='remove-from-wishlist']",
    },

    async getProductPrice() {
        let price = await I.grabTextFrom(this.locators.itemPrice);
    },

    verifyProductPrice(productPrice) {
        I.see(productPrice, this.locators.itemPrice);
    },

    userOnProductDetailsPage() {
        I.seeElement(this.locators.productDetails);
    },

    selectSize(size) {
        I.waitForElement("//button[@data-attr-value='" + size + "']", 10);
        I.click("//button[@data-attr-value='" + size + "']")
    },

    clickBuyItNow() {
        I.waitForClickable(this.locators.buyItNowButton, 5);
        I.click(this.locators.buyItNowButton);
    },
    selectColor(color) {
        I.click("//button[contains(@aria-label,'" + color + "')]");
    },
    selectQuantity(quantity) {
        I.waitForElement(this.locators.selectQuantity);
        I.selectOption(this.locators.selectQuantity, quantity);
    },

    addToCart() {
        I.waitForElement(this.locators.addToCartButton, 10);
        // I.waitForClickable(this.locators.addToCartButton, 20)
        I.click(this.locators.addToCartButton);
        I.wait(5);
    },
    addProductToMiniCart(productSize, originalQuantity) {
        this.selectSize(productSize);
        this.selectQuantity(originalQuantity);
        I.wait(2);
        this.addToCart();
    },

    proceedToCheckout() {
        I.waitForVisible(this.locators.miniCartIcon);
        I.moveCursorTo(this.locators.miniCartIcon);
        I.waitForVisible(this.locators.miniCartCheckoutButton);
        I.click(this.locators.miniCartCheckoutButton);
    },

    proceedToCart() {
        I.waitForElement(this.locators.miniCartIcon);
        I.click(this.locators.miniCartIcon);
        I.wait(2);
    },

    async verifyScene7Images() {
        let productImages = "//div[@class='product-carousel']//img";
        this.scrollToSpecification();
        I.wait(1);
        let imgCounts = await I.grabNumberOfVisibleElements(productImages);
        I.say("Image count : " + imgCounts);
        for (let i = 1; i < imgCounts; i++) {
            let imgSrc = await I.grabAttributeFrom("(//div[@class='product-carousel']//img)[" + i + "]", "src");
            I.assertContain(imgSrc, "tumi.scene7.com");
        }
    },

    verifyDiscountedProductPrice(originalPrice, offerPrice, offerDesc) {
        I.wait(2);
        I.see(originalPrice, this.locators.originalStrikePrice);
        I.see(offerPrice, this.locators.itemPrice);
        I.see(offerDesc, this.locators.promoOfferDesc);
    },

    verifyProductDetails() {
        I.seeElement(this.locators.itemPrice);
        I.seeElement(this.locators.colorChart);
        I.seeElement(this.locators.rating);
    },

    clickOnMaintenance() {
        this.scrollToSpecification();
        I.click(this.locators.maintenanceAndCareTab);
    },

    clickOnSpecifications() {
        this.scrollToSpecification();
        I.click(this.locators.specificationsTab);
    },

    verifyMaintenanceAndCare() {
        I.see('Product Warranty', '#pdpTabContent');
    },

    verifySpecifications() {
        I.see('Weight', "#pdpTabContent");
    },

    scrollToSpecification() {
        I.scrollIntoView(this.locators.specificationsTab, { behavior: "smooth", block: "center", inline: "center" });
    },

    clickOnairLineGuide() {
        this.scrollToSpecification();
        I.click(this.locators.compatibleAirlineGuideTab);
    },

    verifyairLineGuide() {
        I.see('Popular Airlines', '#pdpTabContent');
        I.see('Size Allowance', '#pdpTabContent');
        I.see('Weight', '#pdpTabContent');
    },

    selectSizeUnits() {
        I.click(this.locators.AirlineGuideMetric);
        this.verifyairLineGuide();
    },

    verifyBrowseAllAirlineGuidePopUp() {
        I.click(this.locators.browseAirLineData);
        I.wait(2);
        I.seeElement("//div[@id='airlineGuideModal']");
    },

    verifyStickyAddToCart() {
        I.scrollPageToBottom();
        I.seeElement(this.locators.stickyATCButton);
        I.click(this.locators.stickyATCButton);
        I.waitForVisible(this.locators.cartFlyoutViewMyCartBtn);
    },

    verifyStickyViewOption() {
        I.scrollPageToBottom();
        I.seeElement(this.locators.stickyViewOption);
        I.click(this.locators.stickyViewOption);
        I.waitForVisible(this.locators.colorChart);
    },

    verifyProductPriceOnSticky(price) {
        I.scrollPageToBottom();
        I.see(price, "//div[contains(@class,'sticky-container')]//div[@class='add-to-cart']//div[@class='price']");
    },

    verify360View() {
        I.scrollTo(this.locators.pdp360ViewSection);
        I.wait(5);
        I.seeElement(this.locators.pdp360ViewSection);
    },
    verifyVideo() {
        // I.scrollPageToBottom();
        // this.scrollToSpecification();
        I.wait(5);
        // I.scrollTo(this.locators.pdpVideoSection);
        I.seeElement(this.locators.pdpVideoSection);
    },

    async verifyIncludeWithPurchase() {
        this.scrollToSpecification();
        I.wait(2);
        I.scrollIntoView(this.locators.includeWithPurchase);
        let getText = await I.grabTextFrom(this.locators.includeWithPurchase);
        I.assertEqualIgnoreCase(getText, "Included With This Purchase");
    },

    verifyPixlee() {
        I.scrollTo("//iframe[contains(@id,'pixlee_widget')]");
        I.switchTo("//iframe[contains(@id,'pixlee_widget')]");
        I.wait(1);
        I.seeElement(this.locators.pdpPixlee);
    },

    verifyRecommendation() {
        I.scrollPageToBottom();
        I.scrollIntoView(this.locators.pdpRecommendation);
        I.seeElement(this.locators.pdpRecommendation);
    },


    //Accent customization

    addAccent() {
        I.click(this.locators.accents_addBtn);
        I.wait(2);
        I.see("Add Color Accents", this.locators.accents_flyoutModal);
        I.click(this.locators.accents_select);
        I.scrollIntoView(this.locators.accents_apply);
        I.click(this.locators.accents_apply);
        I.wait(5);
    },

    verifyAddedAccent() {
        I.see("Accent Added", this.locators.accents_addedDesc);
    },

    verifyRemoveAccent() {
        I.waitForVisible(this.locators.accents_removeBtn, 20);
        I.click(this.locators.accents_removeBtn);
        I.wait(10);
        I.scrollIntoView(this.locators.accents_addedDesc);
        I.see("Accent", this.locators.accents_addedDesc);
    },

    verifyAccentPriceChange(productPrice) {
        I.dontSee(productPrice, this.locators.priceTag);
    },
    verifyPriceisNotChange(productPrice) {
        I.see(productPrice, this.locators.priceTag);
    },

    //Monogram customization

    addClassicMonogram() {
        I.waitForVisible(this.locators.monogram_addBtn, 10);
        I.click(this.locators.monogram_addBtn);
        I.wait(2);
        I.see("Make it Yours", this.locators.monogram_flyoutModal);
        I.click(this.locators.monogram_addClassicLink);

        I.fillField(this.locators.monogram_classicInitialsChar + "[1]", "A");
        I.fillField(this.locators.monogram_classicInitialsChar + "[2]", "B");
        I.fillField(this.locators.monogram_classicInitialsChar + "[3]", "C");
        I.click(this.locators.monogram_classicNextBtn);

        I.click(this.locators.monogram_classicSelectFont + "[1]");
        I.click("(" + this.locators.monogram_classicNextBtn + ")[2]");

        I.click(this.locators.monogram_classicSelectFontColor + "[1]");
        I.click("(" + this.locators.monogram_classicNextBtn + ")[3]");

        I.click(this.locators.monogram_classicBothSelect);
        I.click(this.locators.monogram_applyClassic);
        I.wait(2);
    },

    seeMymonogramPDP() {
        I.click(this.locators.monogram_addBtn);
        I.wait(2);
        I.click(this.locators.monogram_addClassicLink);
        I.see('4 OF 4', this.locators.monogram_page4hearder);
    },
    dontSeeMymonogramPDP() {
        I.click(this.locators.monogram_addBtn);
        I.wait(2);
        I.click(this.locators.monogram_addClassicLink);
        I.dontSee('4 OF 4', this.locators.monogram_page4hearder);
    },

    verifyAddedClassincMonogram() {
        I.scrollIntoView(".edit-monogram", { behavior: "smooth", block: "center", inline: "center" });
        I.seeElement(".edit-monogram");
        I.wait(1);
    },

    verifyRemoveMonogram() {
        I.waitForVisible(this.locators.monogram_removeBtn, 20);
        I.click(this.locators.monogram_removeBtn);
    },

    verifyMonogramRemoved() {
        I.wait(2);
        I.dontSeeElement(".edit-monogram");
    },

    addPremiumMonogram() {
        I.click(this.locators.monogram_addBtn);
        I.wait(2);
        I.see("Make it Yours", this.locators.monogram_flyoutModal);
        I.click(this.locators.monogram_addPremiumLink);

        I.fillField(this.locators.monogram_premiumInitialsChar + "[1]", "A");
        I.fillField(this.locators.monogram_premiumInitialsChar + "[2]", "B");

        I.click(this.locators.monogram_premiumNextBtn);
        I.wait(2);
        I.click(this.locators.monogram_premiumSelectFontColor + "[1]");
        I.click(this.locators.monogram_premiumNextBtnStep2);
        I.wait(2);
        I.click(this.locators.monogram_premiumSelectPatch)
        I.click(this.locators.monogram_applyPremium);
        I.wait(5);
    },

    PdpSavedItem() {
        I.wait(5);
        I.click(this.locators.savedItemIcon);
    },




};
