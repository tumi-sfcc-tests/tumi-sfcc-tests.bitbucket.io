const I = actor();
module.exports = {
    locators: {
        embroideryAdded: "//div[@class='embroidery-confirmmsg']",
        addLink: "//a[@aria-label='Add link of embroidery']",
        instoreNote: "//p[@class='instore-pickup-note']",
        fontStyle: "//span[@class='embroidery_font_style_",
        embroiderStepNext: "(//a[contains(@class,'embroidery-step-button-next')])",
        pageOfModal: "(//div[@class='embroidery-count'])",
        initials: "//fieldset[@class='personalization-embroidery-symbols']/button",
        fontColor: "(//div[@class='embroidery-fontColor']/label)",
        applyEmbroidery: "//a[contains(@class,'embroidery-step-button-apply')]",
        previousEmbroidery: "(//a[contains(@class,'embroidery-step-button-prev')])",
        cancelEmbroidery: "(//div[@class='embroidery-btn-cntr'])[1]/a[2]",
        modalTitle: "//form[@id='embroideryForm']//div[@class='modal-title']",
        embroideryInitialInput1: "(//div[@class='embroidery-initials'])[3]//input[1]",
        editEmbroidery: "//a[@class='edit-embroidery']",
        removeEmbroidery: "//a[@class='remove-embroidery']",
        closeOnEmbroideryPage1: ".embroidery-modal .close",
        addToCart: ".product-btn-wrapper .add-to-cart",
        viewMyCart: "//div[contains(@class,'global-modal show')]//a[@href='/cart/']",
        checkoutCart: "//a[contains(@class,'checkout-btn')]",
        iconEmbroidered: "(//div[@class='icon'])[1]/span",
        iconEmbroideredCartFlyout: "(//span[@class='icon embroidery'])[1]/span",
        iconEmbroideredCheckout: "//div[@class='contact-info']/following-sibling::div//span[@class='icon']/span",
        iconEmbroideredOrderConfirmation: "//div[contains(@class,'order-summmary-details')]//span[@class='icon']",
        alphaNumericInitialonPDP: "(//div[@class='icon'])[1]",
        fontNameEmbroidery: "(//div[@class='embroidery-wrapper'])[1]/div[3]",
        orderConfModalClose: "//div[@class='tt-o-modal__close']/button"

    },
    verifyEmbAdded(initial) {
        I.see('Embroidery Added', this.locators.embroideryAdded);
        I.see(initial, this.locators.iconEmbroidered + "[1]");
        I.see(initial, this.locators.iconEmbroidered + "[2]");
        I.see(initial, this.locators.iconEmbroidered + "[3]");
    },
    checkAddedNote(note) {
        I.see(note, this.locators.instoreNote);
    },
    addEmbroidery() {
        I.click(this.locators.addLink);
    },
    verifyModalEmbroidery() {
        I.see('Embroidery', this.locators.modalTitle);
    },
    chooseStyleEmb(font) {
        I.click(this.locators.fontStyle + font + "']");
    },
    clickNextEmb(modalPageNum) {
        switch (modalPageNum) {
            case "1 of 3":
                I.click(this.locators.embroiderStepNext + "[1]");
                break;
            case "2 of 3":
                I.click(this.locators.embroiderStepNext + "[2]");
                break;
        }
    },
    verifyPageNavigationEmb(modalPageNum) {
        switch (modalPageNum) {
            case "1 of 3":
                I.see('1 OF 3', this.locators.pageOfModal + "[1]");
                break;
            case "2 of 3":
                I.see('2 OF 3', this.locators.pageOfModal + "[2]");
                break;
            case "3 of 3":
                I.see('3 OF 3', this.locators.pageOfModal + "[3]");
                break;
        }

    },
    enterInitialEmb(initial) {
        I.click(this.locators.embroideryInitialInput1);
        I.click(this.locators.initials + "[" + initial + "]");
        I.click(this.locators.initials + "[" + initial + "]");
        I.click(this.locators.initials + "[" + initial + "]");
    },
    selectColorEmb(color) {
        I.click(this.locators.fontColor + "[" + color + "]");
    },
    applyEmbroidery() {
        I.click(this.locators.applyEmbroidery);
    },
    clickCancelEmb() {
        I.click(this.locators.cancelEmbroidery);
    },
    clickPreviousEmb(modalPageNum) {
        switch (modalPageNum) {
            case "2 of 3":
                I.click(this.locators.previousEmbroidery + "[1]");
                break;
            case "3 of 3":
                I.click(this.locators.previousEmbroidery + "[2]");
                break;
        }
    },
    verifyEmbroideryNotAdded() {
        I.dontSeeElement(this.locators.embroideryAdded)
        // I.dontSee('Embroidery Added', this.locators.embroideryAdded);
        I.dontSeeElement(this.locators.modalTitle);
    },
    seeEditEmbroidery() {
        I.see('Edit', this.locators.editEmbroidery);
    },
    clickEditEmbroidery() {
        I.click(this.locators.editEmbroidery);
    },
    seeRemoveEmbroidery() {
        I.see('Remove', this.locators.removeEmbroidery);
    },
    clickRemoveEmbroidery() {
        I.click(this.locators.removeEmbroidery);
    },
    embroideryRemoved() {
        I.dontSeeElement(this.locators.iconEmbroidered);
    },
    crossOnEmbModal() {
        I.waitForVisible(this.locators.closeOnEmbroideryPage1, 10);
        I.seeElement(this.locators.closeOnEmbroideryPage1);
    },
    clickcrossOnEmbModal() {
        I.click(this.locators.closeOnEmbroideryPage1);
    },
    clickCheckoutOnCart() {
        I.waitForVisible(this.locators.checkoutCart, 10);
        I.click(this.locators.checkoutCart);
    },
    verifyEmbroideryOnCheckout(initial) {
        I.see(initial, this.locators.iconEmbroideredCheckout + "[1]");
        I.see(initial, this.locators.iconEmbroideredCheckout + "[2]");
        I.see(initial, this.locators.iconEmbroideredCheckout + "[3]");
    },
    verifyEmbroideryOnOrderConfirmation(initial) {
        // I.click(this.locators.orderConfModalClose);
        I.see(initial, this.locators.iconEmbroideredOrderConfirmation);
    },
    clickAddtoCart() {
        I.click(this.locators.addToCart);
    },
    verifyEmbroideryOnCartFlyout(initial) {
        I.waitForVisible(this.locators.viewMyCart, 10);
        I.see(initial, this.locators.iconEmbroideredCartFlyout + "[1]");
        I.see(initial, this.locators.iconEmbroideredCartFlyout + "[2]");
        I.see(initial, this.locators.iconEmbroideredCartFlyout + "[3]");
        I.click(this.locators.viewMyCart);
    },
    verifyNameOfFont(style) {
        switch (style) {
            case "1":
                I.see('Times: Classic and sophisticated.', this.locators.fontNameEmbroidery);
                break;
            case "2":
                I.see('Futura: Clean and modern.', this.locators.fontNameEmbroidery);
                break;
            case "3":
                I.see('Bodoni: Timeless and elegant.', this.locators.fontNameEmbroidery);
                break;
            case "4":
                I.see('Script: Stylish and decorative.', this.locators.fontNameEmbroidery);
                break;
        }
    },
    enterAlpNumInitial(initial) {
        I.fillField(this.locators.embroideryInitialInput1, initial);
    },
    verifyAlpNumInitial(initial) {
        I.see(initial, this.locators.alphaNumericInitialonPDP);
    }


}