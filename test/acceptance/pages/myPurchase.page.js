const { I } = inject();
module.exports = {
    locators: {
        myAccount: "//a[@id='myaccount']",
        myPurchases: "//ul[@class='account-list']//a[text()='Manage Purchases']",
        managePurchasesHeading: "//h1[@class='account-title']",
        orderList_mP: "(//div[@class='row order-list'])",
        loadMore_mP: "//button[contains(@class,'load-more')]",
        contentSlot_Recomendation: "//div[@class='rfk-carosuel-container']/div/h2",
        position1: "//div[@class='html-slot-container']/h2",
        recommendedProduct: "(//div[@class='product'])",
        collectionNext: "//div[@class='collections-next']",
        collectionPrev: "//div[@class='collections-prev']",
        viewDetails: "(//a[contains(@href,'Order-Details')])",
        orderDetails: "//div[contains(@class,'purchase-order-details')]",
        totalOnOrderDetail: "//span[@class='grand-total-sum']",
        searchBar: "#searchInputField",
        clearSearch: "//button[@class='clear-search-button']",
        submitSearch: "(//button[@name='search-button'])[2]",
        orderInSearchList: "(//h4[@class='order-no'])",
        trackingDetailsLink: "(//a[@class='tracking-details'])",
        trackingIndicator: "(//div[@class='tracking_status_steps__indicator'])",
        cvvInput: "(//input[@placeholder='CVV'])[1]",
        orderNumberConf: "//div[@class='order-number']/span",
        myAccountIcon: "#myaccount",
        closeModalConf: "(//button[@aria-label='Close'])[5]",
        totalOnConfirmation: "//div[@class='row total']/div[2]/p",
        orderListCount: "//span[@class='order-list-count']"
    },

    grasp: {
        numberBeforeLoadMore: "",
        orderOnConf: "",
        totalOnConf: ""
    },
    clickOnAccountIcon() {
        I.click(this.locators.myAccount);
    },
    clickOnMyPurchase() {
        I.click(this.locators.myPurchases);
    },
    navigationPurachsePage() {
        I.see('Manage Purchases', this.locators.managePurchasesHeading);
    },
    async verifyOrderList() {
        I.seeElement(this.locators.orderList_mP);
        let numberOfOrder = await I.grabNumberOfVisibleElements(this.locators.orderList_mP);
        this.grasp.numberBeforeLoadMore = numberOfOrder;
    },
    viewLoadMore() {
        I.see('Load More', this.locators.loadMore_mP);
    },
    async clickLoadMore() {
        await I.click(this.locators.loadMore_mP);
        I.wait(5);
    },
    async verifyLoadMore() {
        let numberOfOrder = await I.grabNumberOfVisibleElements(this.locators.orderList_mP);
        console.log(numberOfOrder + this.grasp.numberBeforeLoadMore.toString());
        I.assertContain((numberOfOrder > this.grasp.numberBeforeLoadMore).toString(), "true");
    },
    recommendationInPurchaseList() {
        I.scrollIntoView(this.locators.orderListCount);
        I.wait(2);
        I.see('Recommended', this.locators.contentSlot_Recomendation);
        I.seeElement(this.locators.recommendedProduct);
    },
    recommendationInsearchResult() {
        I.scrollIntoView(this.locators.viewDetails);
        I.wait(2);
        I.see('Recommended', this.locators.contentSlot_Recomendation);
        I.seeElement(this.locators.recommendedProduct);
    },
    clickFirstOrder() {
        I.click(this.locators.viewDetails + "[1]");
    },
    firstOrderDetail() {
        I.seeElement(this.locators.orderDetails);
    },
    recommendationInorderDetail() {
        I.scrollIntoView(this.locators.totalOnOrderDetail);
        I.wait(2);
        I.see('Recommended', this.locators.contentSlot_Recomendation);
        I.seeElement(this.locators.recommendedProduct);
    },
    verifySearchBar() {
        I.seeElement(this.locators.searchBar);
    },
    clickSearch() {
        I.click(this.locators.searchBar);
    },
    enterOrderId(orderNum) {
        I.fillField(this.locators.searchBar, orderNum);
        I.click(this.locators.submitSearch);
    },
    findInSearchList(orderNum) {
        I.see(orderNum, this.locators.orderInSearchList + "[1]");
    },
    verifyTrackLink() {
        I.seeElement(this.locators.trackingDetailsLink);
    },
    clickTrackLink() {
        I.click(this.locators.trackingDetailsLink + "[1]");
    },
    navigationTrackingPage() {
        I.seeElement(this.locators.trackingIndicator);
    },
    enterCVV() {
        I.fillField(this.locators.cvvInput, '123');
    },
    async verifyNewlyCreatedOrder() {
        let orderNumber = await I.grabTextFrom(this.locators.orderNumberConf);
        let priceOnConfirmation = await I.grabTextFrom(this.locators.totalOnConfirmation);
        this.grasp.totalOnConf = priceOnConfirmation;
        this.grasp.orderOnConf = orderNumber;
        I.click(this.locators.closeModalConf);

    },
    clickMyAccIcon() {
        I.click(this.locators.myAccountIcon);
    },
    verifyOrderOnList() {
        I.see("Order Number " + this.grasp.orderOnConf, this.locators.orderList_mP + "[1]");
    },
    verifyTotalPriceOfOrder() {
        I.see(this.grasp.totalOnConf, this.locators.totalOnOrderDetail);
    }
}