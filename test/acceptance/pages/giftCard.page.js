const { data, giftCardPage, utilities } = inject();
const I = actor();

module.exports = {
    locators: {
        giftingCategory: "//a[@id='gifting']",
        giftCardMenu: "//a[@id='gift-cards']",
        giftCardBanner: "//div[@class='giftcard-creation-banner']",
        tumiGiftCardTxt: "//h2[@class='giftcard-creation__title']",
        breadCurmb: "//ol[@class='breadcrumb']",
        cardValue: "(//label[@class='form-check-label'])",
        recipientName: "//input[@id='recipientName']",
        senderName: "//input[@id='senderName']",
        gcMessage: "//input[@id='gcMessage']",
        addToCart: "//div[@class='gc-add-to-cart']/button[1]",
        checkGCbalanceTxt: "//h2[@class='giftcard-balance__title']",
        gcNumField: "//input[@id='giftcard-balance-form-accountnumber']",
        gcPinField: "//input[@id='giftcard-balance-form-accountnumber']",
        checkBalanceGC: "//button[@class='button w-100 check-balance-cta']",
        gcSampleImg: "//div[@class='giftcard-balance-sample']",
        balanceGC: "//div[contains(@class,'check-balance-details')]/p"

    },
    hoverGifiting() {
        I.moveCursorTo(this.locators.giftingCategory);
    },
    verifySubCategoryGiftCard() {
        I.seeElement(this.locators.giftCardMenu);
    },
    clickSubCategoryGiftCard() {
        I.click(this.locators.giftCardMenu);
    },
    verifyOnGiftCardPage(component) {
        switch (component) {
            case 'gift card banner':
                I.seeElement(this.locators.giftCardBanner);
                break;
            case 'Tumi Gift Card header':
                I.see("The TUMI Gift Card", this.locators.tumiGiftCardTxt);
                break;
            case 'bread-crumbs':
                I.seeElement(this.locators.breadCurmb);
                break;
            case 'all card values':
                for (let i = 1; i < 6; i++) {
                    I.seeElement((this.locators.cardValue) + "[" + i + "]");
                }
                break;
            case 'recipient name field':
                I.seeElement(this.locators.recipientName);
                break;
            case 'sender name field':
                I.seeElement(this.locators.senderName);
                break;
            case 'add a message field':
                I.seeElement(this.locators.gcMessage);
                break;
            case 'add to cart cta':
                I.seeElement(this.locators.addToCart);
                break;
            case 'Gift card balance header':
                I.seeElement(this.locators.checkBalanceGC);
                break;
            case 'gift card num field':
                I.seeElement(this.locators.gcNumField);
                break;
            case 'pin field':
                I.seeElement(this.locators.gcPinField);
                break;
            case 'check balance cta':
                I.seeElement(this.locators.checkBalanceGC);
                break;
            case 'add to cart cta':
                I.see(this.locators.addToCart);
                break;
            case 'gift card sample image':
                I.seeElement(this.locators.gcSampleImg);
                break;
        }
    },
    enterCardNum(cardNumber) {
        I.fillField(this.locators.gcNumField, cardNumber);
    },
    enterCardPin(cardPin) {
        I.fillField(this.locators.gcPinField, cardPin);
    },
    clickCheckBalance() {
        I.click(this.locators.checkBalanceGC);
    },
    verifyBalanace(balanceInCard) {
        I.see(balanceInCard, this.locators.balanceGC)
    }
}    