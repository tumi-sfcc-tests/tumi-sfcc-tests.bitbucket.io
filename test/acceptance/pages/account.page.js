const { I, data } = inject();

module.exports = {
    locators: {
        viewAll: "//a[@class='pull-right']",
        firstname: "//input[@id='firstName']",
        EditAddressfirstname: "//form[@id='addressForm']//input[@id='firstName']",
        lastname: "//input[@id='lastName']",
        phone: "#phone",
        email: "//input[@id='email']",
        confirmEmail: "//input[@id='confirmEmail']",
        confirmPassword: "//input[@id='password']",
        saveBtn: "//div[@class='edit-profile']//button[@name='save']",
        cancel: '//a[normalize-space()="Cancel"]',
        makeDefault: '//label[normalize-space()="Make this my default address"]',
        DOB: '#birthday',
        gender: '//select[@id="gender"]',

        myAccountIcon: "//a[@id='myaccount']",
        myAccountIconNotLoggedIn: "//a[@data-target='#requestLoginModal']",
        loggedInUsername: "//div[@class='signed-in']/h3",

        profileList_MyAccount: "//div[@class='signed-in']//a[contains(text(),'My Account')]",
        profileList_MySettings: "//ul[@class='account-list']//a[text()='My Settings']",
        profileList_MyPurchases: "//ul[@class='account-list']//a[text()='Manage Purchases']",
        profileList_MySavedItem: "//ul[@class='account-list']//a[text()='My Saved Items']",
        profileList_RegisterMyTumi: "//ul[@class='account-list']//a[text()='Register My TUMI']",
        profileList_Repairs: "//ul[@class='account-list']//a[text()='Repair Services']",

        myAccount_MySettings: "//div[@id='signedInModal']//a[text()='My Account']",

        displayedFname: "//dt[contains(text(),'Name')]/../dd",
        displayedLname: "//dt[contains(text(),'Name')]/../dd",
        displayedPhone: "//dt[contains(text(),'Mobile')]/../dd",

        personalDetailsEdit: "//button[@id='profile-edit']",

        mySettings_Overview: "//li[@id='setting-overview-left']",

        //--------------------------------------------------------------------------//
        overview: "setting-overview-left",
        savedAddress: '#setting-address-left',
        savedPayment: '#setting-payment-left',
        shoppingPreferences: '#setting-shoppingPreferences-left',
        communicationPreferences: "#setting-communicationPreference-left",
        myMonogram: "#setting-myMonogram-left",
        luggagecheckbox: "#luggagecheckbox",
        womenbagcheckbox: "#womensbackpackscheckbox",
        menbagcheckbox: "#mensbackpackscheckbox",
        accessoriescheckbox: "#accessoriescheckbox",
        luggage: "//label[normalize-space()='Luggage']",
        womenBags: "//label[contains(text(),'Women’s Bags and Backpacks')]",
        menBags: "//label[contains(text(),'Men’s Bags and Backpacks')]",
        accessories: "//label[normalize-space()='Accessories']",
        savePreference: "//div[@id='setting-shoppingPreferences']//button[normalize-space()='Save']",

        addnewaddress: '//a[normalize-space()="Add New Address"]',
        addAddressBtn: "//div[@class='edit-address']//button[@name='save']",
        saveAddressChangesBtn: "//div[@class='saved-address']//button[@name='save']",
        savedDefaultAddressEdit: ".new-address .divider .edit-adr-btn",
        savedAddressEdit: "//div[@class='card-make-default-link']/../form[@class='edit-my-address-form']/button",
        addnewPayment: '//div[@id="setting-payment"]//a',
        addressTitle: '#addressId.form-control',
        address1: '//input[@id="address1"]',
        country: '#country.form-control',
        state: '//select[@id="state"]',
        city: '//input[@id="city"]',
        zip: '//input[@id="zipCode"]',
        deleteAddress: '//button[normalize-space()="Delete"]',

        cardOwner: "#cardOwner",
        cardNumber: "#cardNumber",
        cardNumberNewCYB: "//input[@name='number']",
        cardExpMonth: "#month",
        cardExpYear: "#year",
        cardCVV: "#securityCode",
        cardCVVNewCYB: "//input[@name='securityCode']",

        billingAddress1: "#address1",
        billingCity: "#billingAddressCity",
        billingState: "#billingState",
        billingZIP: "#postalCode",
        billingMobile: "#phone",
        cardSaveBtn: "//button[@name='save']",

        commPrefOptCheckBox: "#emailcheckbox",
        commSaveBtn: ".communication-pref-form .button-communication-pref",
        passwordEditButton: "//button[@class='password-edit']",
        changePasswordSaveButton: "//div[@class='password-form']//button[@name='save']",
        currentPasswordInput: "//div[@class='password-form']//input[@placeholder='Current Password*']",
        newPasswordInput: "//div[@class='password-form']//input[@placeholder='New Password*']",
        defaultCheckbox: "//div[@class='payment-section']//input[contains(@id,'checkbox')]/..",
        checkingdefaultpayment: "//h2[contains(text(),'Default Payment')]/../..",
        makeDefaultPaymentCTA: "//button[@class='set-default-paymentInstrument']",
        //---------------------------------------------------------------------------------------------------------------------------------------
        createMonogram: "//a[contains(@class,'add-monogram-modal')]",
        monogramForm: ".monogram-form",
        firstInitial: "//div[@class='monogram-initials']/input[1]",
        secondInitial: "//div[@class='monogram-initials']/input[2]",
        thirdInitial: "//div[@class='monogram-initials']/input[3]",
        monogramSymbol: "//fieldset[contains(@class,'personalization-monogram-symbols')]/label",
        page1Next: "(//a[contains(@class,'monogram-step-button-next')])[1]",
        monogramFonts: "//div[@class='monogram-fonts']/label",
        page2Next: "(//a[contains(@class,'monogram-step-button-next')])[2]",
        saveAsMymonogram: "(//a[contains(@class,'monogram-step-button-apply')])[1]",
        myMonogramIcon: "//img[@class='icon']",
        updateMymonogram: "//a[contains(@class,'edit-monogram')]",
        removeMymonogram: "//a[contains(@class,'remove-monogram')]",

        monogram_addBtn: "//a[@class='add-monogram-modal']",
        monogram_addClassicLink: "//a[@class='classic-modal-btn']",
        monogramPdpPage4of4: "(//div[@class='monogram-wrapper'])[4]"


    },

    viewMyAccountMenuFlyout() {
        I.pressKey("Escape");
        I.waitForElement(this.locators.myAccountIcon, 10);
        I.click(this.locators.myAccountIcon);
        I.wait(2);
        I.waitForElement(this.locators.profileList_MyAccount, 10);
    },

    navigateToMySettings() {
        I.waitForVisible(this.locators.myAccountIcon, 15);
        I.click(this.locators.myAccountIcon);
        I.waitForVisible(this.locators.profileList_MySettings, 15);
        I.click(this.locators.profileList_MySettings);
        I.wait(5);
    },

    navigateToMySavedItem() {
        I.waitForVisible(this.locators.myAccountIcon, 15);
        I.click(this.locators.myAccountIcon);
        I.waitForVisible(this.locators.profileList_MySavedItem, 15);
        I.click(this.locators.profileList_MySavedItem);
    },

    clickEditProfile() {
        I.waitForClickable(this.locators.personalDetailsEdit, 20);
        I.click(this.locators.personalDetailsEdit);
    },

    clickMySettings() {
        I.click(this.locators.myAccount_MySettings);
    },

    clickOverview() {
        I.click(this.locators.mySettings_Overview);
    },

    clickAddAddress() {
        I.click(this.locators.addnewaddress);
    },
    clickCancel() {
        I.waitForClickable(this.locators.cancel, 5);
        I.click(this.locators.cancel);
        I.seeElement(this.locators.savedAddress);
    },
    clickAddPayment() {
        I.waitForClickable(this.locators.addnewPayment, 10);
        I.click(this.locators.addnewPayment);
    },

    clickEditPassword() {
        let locator = locate(this.locators.viewAll)
            .withAttr({ 'aria-label': 'Change Password' });
        I.click(locator);
    },



    verifyUpdatedDetails() {
        I.waitForClickable(this.locators.personalDetailsEdit, 20);
        I.see(data.updateProfile.newfName, ".pname");
        I.see(data.updateProfile.newlName, ".pname");
        // I.seeElement('//dd[contains(normalize-space(),"' + data.updateProfile.newfName + '")]');
        // I.seeElement('//dd[contains(normalize-space(),"' + data.updateProfile.newlName + '")]');
        I.see(data.updateProfile.newPhone, ".pmobile");
        I.see(data.updateProfile.newGender, ".pgender");
        I.see(data.updateProfile.newDOB, ".pbirthday");
        // I.seeElement('//dd[contains(normalize-space(),"' + data.updateProfile.newDOB + '")]');
        // I.seeElement('//dd[contains(normalize-space(),"' + data.updateProfile.newGender + '")]');
    },

    editPersonalDetails() {
        this.clickEditProfile();
        I.fillField(this.locators.firstname, data.updateProfile.newfName);
        I.fillField(this.locators.lastname, data.updateProfile.newlName);
        // I.fillField(this.locators.phone, data.updateProfile.newPhone);
        I.executeScript("document.querySelector('" + this.locators.phone + "').value = '" + data.updateProfile.newPhone + "';");
        // I.fillField(this.locators.DOB, data.updateProfile.newDOB);
        I.executeScript("document.querySelector('" + this.locators.DOB + "').value = '" + data.updateProfile.newDOB + "';");
        I.selectOption(this.locators.gender, data.updateProfile.newGender);
        I.click(this.locators.saveBtn);
    },

    resetPersonalDetails() {
        this.clickEditProfile();
        I.fillField(this.locators.firstname, data.updateProfile.fName);
        I.fillField(this.locators.lastname, data.updateProfile.lName);
        I.fillField(this.locators.phone, data.updateProfile.phone);
        I.fillField(this.locators.DOB, data.updateProfile.DOB);
        I.selectOption(this.locators.gender, data.updateProfile.gender);
        I.click(this.locators.saveBtn);
    },


    // verifyUpdatedDetails(field) {
    //     I.waitForClickable(this.locators.personalDetailsEdit, 20);
    //     switch (field.toUpperCase()) {
    //         case 'FIRSTNAME':
    //             I.seeElement('//dd[contains(normalize-space(),"' + data.updateProfile.newfName + '")]');
    //             break;
    //         case 'LASTNAME':
    //             I.seeElement('//dd[contains(normalize-space(),"' + data.updateProfile.newlName + '")]');
    //             break;
    //         case "PHONE NUMBER":
    //             phone = data.updateProfile.newPhone;
    //             const phno = phone.slice(0, 3) + "-" + phone.slice(3, 6) + "-" + phone.slice(6, 11);
    //             I.see(phno, this.locators.displayedPhone);
    //             break;
    //         case "DOB":
    //             I.seeElement('//dd[contains(normalize-space(),"' + data.updateProfile.newDOB + '")]');
    //             break;
    //         case "GENDER":
    //             I.seeElement('//dd[contains(normalize-space(),"' + data.updateProfile.newGender + '")]');
    //             break;
    //     }
    // },

    // editPersonalDetails(field) {
    //     this.clickEditProfile();
    //     switch (field.toUpperCase()) {
    //         case 'FIRSTNAME':
    //             I.fillField(this.locators.firstname, data.updateProfile.newfName);
    //             break;
    //         case 'LASTNAME':
    //             I.fillField(this.locators.lastname, data.updateProfile.newlName);
    //             break;
    //         case "PHONE NUMBER":
    //             I.fillField(this.locators.phone, data.updateProfile.newPhone);
    //             break;
    //         case "DOB":
    //             I.fillField(this.locators.DOB, data.updateProfile.newDOB);
    //             break;
    //         case "GENDER":
    //             I.selectOption(this.locators.gender, data.updateProfile.newGender);
    //             break;
    //     }
    //     I.fillField(this.locators.DOB, data.updateProfile.newDOB);
    //     I.click(this.locators.saveBtn);
    // },

    // resetPersonalDetails(field) {
    //     this.clickEditProfile();
    //     switch (field.toUpperCase()) {
    //         case 'FIRSTNAME':
    //             I.fillField(this.locators.firstname, data.updateProfile.fName);
    //             break;
    //         case 'LASTNAME':
    //             I.fillField(this.locators.lastname, data.updateProfile.lName);
    //             break;
    //         case "PHONE NUMBER":
    //             I.fillField(this.locators.phone, data.updateProfile.phone);
    //             break;
    //         case "DOB":
    //             I.fillField(this.locators.DOB, data.updateProfile.DOB);
    //             break;
    //         case "GENDER":
    //             I.selectOption(this.locators.gender, data.updateProfile.gender);
    //             break;
    //     }
    //     I.fillField(this.locators.DOB, data.updateProfile.DOB);
    //     I.click(this.locators.saveBtn);
    // },


    //--------------------------------------------------------------------------//


    addNewAddress(fName, lName, address1, state, city, zipcode, phone) {
        I.waitForClickable(this.locators.firstname, 5);
        I.fillField(this.locators.firstname, fName);
        I.fillField(this.locators.lastname, lName);
        I.fillField(this.locators.address1, address1);
        I.wait(2);
        // I.pressKey("Escape");
        I.click(this.locators.lastname);
        I.selectOption(this.locators.state, state);
        I.fillField(this.locators.city, city);
        I.fillField(this.locators.zip, zipcode);
        I.wait(3);
        // I.fillField(this.locators.phone, phone);
        I.executeScript("document.querySelector('" + this.locators.phone + "').value = '" + phone + "';");
        I.click(this.locators.addAddressBtn);
    },

    verifyDefaultAddress(fName, lName, address1, state, city, zipcode, phone) {
        I.wait(10);
        let defaultAddressLocator = "//div[@class='new-address']//div[@class='divider']";
        I.see(fName, defaultAddressLocator);
        I.see(lName, defaultAddressLocator);
        I.see(address1, defaultAddressLocator);
        I.see(state, defaultAddressLocator);
        I.see(city, defaultAddressLocator);
        I.see(zipcode, defaultAddressLocator);
        I.see(phone, defaultAddressLocator);
    },

    verifyNewAddress(fName, lName, address1, state, city, zipcode, phone) {
        I.wait(10);
        let addressLocator = ".new-address";
        I.see(fName, addressLocator);
        I.see(lName, addressLocator);
        I.see(address1, addressLocator);
        I.see(state, addressLocator);
        I.see(city, addressLocator);
        I.see(zipcode, addressLocator);
        I.see(phone, addressLocator);
    },

    mySettingsSelectPage(page) {
        switch (page.toUpperCase()) {
            case 'SAVED ADDRESSES':
                I.forceClick(this.locators.savedAddress);
                break;
            case 'SAVED PAYMENT':
                I.forceClick(this.locators.savedPayment);
                break;
            case 'SHOPPING PREFERENCES':
                I.forceClick(this.locators.shoppingPreferences);
                break;
            case 'COMMUNICATION PREFERENCES':
                I.forceClick(this.locators.communicationPreferences);
                break;
            case 'MY MONOGRAM':
                I.forceClick(this.locators.myMonogram);
                break;
        }
        I.wait(2);
    },

    addressFormErrorMessages() {
        I.wait(2);
        I.see("Please enter a first name");
        I.see("Please enter a surname");
        I.see("Please enter Address Line 1");
        I.see("Please enter a Town/City");
        I.see("Please enter a State/Province");
        I.see("Please enter a Zip or Postal Code*");

    },

    savedPreferences() {
        I.see("Luggage");
        I.see("Women’s Bags and Backpacks");
        I.see("Men’s Bags and Backpacks");
        I.see("Accessories");
    },

    selectSavePreference(preferenceOption) {

        switch (preferenceOption.toUpperCase()) {
            case 'LUGGAGE':
                I.uncheckOption(this.locators.luggagecheckbox);
                I.waitForClickable(this.locators.luggage, 15);
                I.click(this.locators.luggage);
                break;
            case 'WOMEN BAGS':
                I.uncheckOption(this.locators.womenbagcheckbox);
                I.waitForClickable(this.locators.womenBags, 15);
                I.click(this.locators.womenBags);
                break;
            case 'MEN BAGS':
                I.uncheckOption(this.locators.menbagcheckbox);
                I.waitForClickable(this.locators.menBags, 15);
                I.click(this.locators.menBags);
                break;
            case 'ACCESSORIES':
                I.uncheckOption(this.locators.accessoriescheckbox);
                I.waitForClickable(this.locators.accessories, 15);
                I.click(this.locators.accessories);
                break;
        }
        I.click(this.locators.savePreference);
    },

    verifyselectedPreference(preferenceOption) {

        switch (preferenceOption.toUpperCase()) {
            case 'LUGGAGE':
                I.seeCheckboxIsChecked(this.locators.luggagecheckbox);
                break;
            case 'WOMEN BAGS':
                I.seeCheckboxIsChecked(this.locators.womenbagcheckbox);
                break;
            case 'MEN BAGS':
                I.seeCheckboxIsChecked(this.locators.menbagcheckbox);
                break;
            case 'ACCESSORIES':
                I.seeCheckboxIsChecked(this.locators.accessoriescheckbox);
                break;
        }

    },

    errorMessagesPayment() {
        let formLocator = ".payment-form";
        I.waitForClickable(this.locators.cardSaveBtn, 5);
        I.click(this.locators.cardSaveBtn);
        I.see("Please enter Address Line 1", formLocator);
        I.see("Please enter a Town/City", formLocator);
        I.see("Please enter a State/Province", formLocator);
        I.see("Please enter a Zip or Postal Code", formLocator);
        I.see("Please enter a valid phone number", formLocator);
        I.see("Please enter name on the card", formLocator);
        I.see("Please enter the card number", formLocator);
        I.see("Please select expiry month of the card", formLocator);
        I.see("Please select expiry year of the card", formLocator);
        I.see("Please enter security code", formLocator);
    },

    async deleteAllAddresses() {
        I.wait(3);
        let deleteElement = await I.grabNumberOfVisibleElements(".remove-address");
        I.say("Count: " + deleteElement);
        for (let index = 0; index < deleteElement; index++) {
            I.waitForClickable(".remove-address", 5);
            I.click(".remove-address");
            I.waitForClickable("//div[@id='deleteAddressModal']//button[contains(text(),'Delete')]", 10);
            I.click("//div[@id='deleteAddressModal']//button[contains(text(),'Delete')]");
            I.wait(2);
        }
    },

    async deleteAllSavedCards() {
        let deleteElement = await I.grabNumberOfVisibleElements(".remove-payment");
        for (let index = 0; index < deleteElement; index++) {
            I.waitForClickable(".remove-payment", 5);
            I.click(".remove-payment");
            I.waitForClickable("//div[@id='deletePaymentModal']//button[contains(text(),'Delete')]", 10);
            I.click("//div[@id='deletePaymentModal']//button[contains(text(),'Delete')]");
            I.wait(2);
        }
    },

    async editMakeDefault() {
        var savedNewAddress = await I.grabTextFrom("//h2[contains(text(),'Saved Addresses')]/../..//div[@class='address-blog']");
        I.click("//div[@class='card-make-default-link']");
        I.wait(10);
        let defaultAddressLocator = "//div[@class='new-address']//div[@class='divider']";
        I.see(savedNewAddress, defaultAddressLocator);
    },

    clickEditAddress(addName) {
        let locator = locate(this.locators.viewAll)
            .withAttr({ 'aria-label': `Edit Address : ${addName} (Default Address)` });
        I.click(locator);
    },

    editAddress(addNewName, addNewAddress1) {
        I.waitForVisible(this.locators.EditAddressfirstname, 20);
        I.clearField(this.locators.EditAddressfirstname);
        I.fillField(this.locators.EditAddressfirstname, addNewName);
        I.clearField(this.locators.address1);
        I.fillField(this.locators.address1, addNewAddress1);
        I.wait(3);
        I.click(this.locators.saveAddressChangesBtn);
        I.waitForVisible('.new-address', 20);
        I.see(addNewName, '.new-address');
        I.see(addNewAddress1, '.new-address');
    },
    //New Cybersource
    addPayment(nameOnCard, ccNum, expMonth, expYear, cvv, address1, city, state, zip, phone) {
        I.fillField(this.locators.cardOwner, nameOnCard);

        var iframe_locator = "//div[@id='cardNumber-container']//iframe";
        I.switchTo(iframe_locator);
        I.fillField(this.locators.cardNumberNewCYB, "");
        I.type(ccNum, 100);
        I.wait(2);
        I.switchTo();


        I.scrollTo(this.locators.cardExpMonth);
        I.selectOption(this.locators.cardExpMonth, expMonth);
        I.selectOption(this.locators.cardExpYear, expYear);

        var iframe_cvv = "//div[@id='securityCode-container']/iframe";
        I.waitForElement(iframe_cvv, 10);
        I.switchTo(iframe_cvv);
        I.waitForElement(this.locators.cardCVVNewCYB);
        I.fillField(this.locators.cardCVVNewCYB, cvv);
        I.switchTo();


        I.fillField(this.locators.billingAddress1, address1);
        I.fillField(this.locators.billingCity, city);
        I.selectOption(this.locators.billingState, state);
        I.fillField(this.locators.billingZIP, zip);
        // I.fillField(this.locators.billingMobile, phone);
        I.executeScript("document.querySelector('" + this.locators.billingMobile + "').value = '" + phone + "';");
        I.wait(2);
        I.click(this.locators.cardSaveBtn);
        I.wait(10);
        I.scrollPageToTop();
        I.wait(2);
    },

    //Old Cybersource
    // addPayment(nameOnCard, ccNum, expMonth, expYear, cvv, address1, city, state, zip, phone) {
    //     I.fillField(this.locators.cardOwner, nameOnCard);
    //     I.fillField(this.locators.cardNumber, ccNum);
    //     I.scrollTo(this.locators.cardExpMonth);
    //     I.selectOption(this.locators.cardExpMonth, expMonth);
    //     I.selectOption(this.locators.cardExpYear, expYear);
    //     I.fillField(this.locators.cardCVV, cvv);

    //     I.fillField(this.locators.billingAddress1, address1);
    //     I.fillField(this.locators.billingCity, city);
    //     I.selectOption(this.locators.billingState, state);
    //     I.fillField(this.locators.billingZIP, zip);
    //     // I.fillField(this.locators.billingMobile, phone);
    //     I.executeScript("document.querySelector('" + this.locators.billingMobile + "').value = '" + phone + "';");
    //     // I.wait(2);
    //     I.click(this.locators.cardSaveBtn);
    //     I.scrollPageToTop();
    //     I.wait(2);
    // },

    addDefaultPayment(nameOnCard, ccNum, expMonth, expYear, cvv, address1, city, state, zip, phone) {
        I.fillField(this.locators.cardOwner, nameOnCard);
        I.fillField(this.locators.cardNumber, ccNum);
        I.scrollTo(this.locators.cardExpMonth);
        I.selectOption(this.locators.cardExpMonth, expMonth);
        I.selectOption(this.locators.cardExpYear, expYear);
        I.fillField(this.locators.cardCVV, cvv);
        I.fillField(this.locators.billingAddress1, address1);
        I.fillField(this.locators.billingCity, city);
        I.selectOption(this.locators.billingState, state);
        I.fillField(this.locators.billingZIP, zip);
        I.fillField(this.locators.billingMobile, phone);
        I.wait(2);
        I.click(this.locators.defaultCheckbox);
        I.wait(5);
        I.click(this.locators.cardSaveBtn);
        I.wait(10);
        I.waitForVisible(this.locators.checkingdefaultpayment);
        I.see(nameOnCard, this.locators.checkingdefaultpayment);
    },

    verifyAddedCard(cardName, zip) {
        I.see(cardName, '.card-info-group');
        I.see(zip, '.card-info-group');
    },

    checkCommPref() {
        I.checkOption(this.locators.commPrefOptCheckBox);
        I.click(this.locators.commSaveBtn);
    },
    uncheckCommPref() {
        I.uncheckOption(this.locators.commPrefOptCheckBox);
        I.click(this.locators.commSaveBtn);
    },

    verifyCommOptInUpdated() {
        I.seeCheckboxIsChecked(this.locators.commPrefOptCheckBox);
    },
    verifyCommOptOutUpdated() {
        I.dontSeeCheckboxIsChecked(this.locators.commPrefOptCheckBox);
    },

    changePassword(currentPassword, newPassword) {
        I.waitForClickable(this.locators.passwordEditButton, 20);
        I.click(this.locators.passwordEditButton);
        I.fillField(this.locators.currentPasswordInput, currentPassword);
        I.fillField(this.locators.newPasswordInput, newPassword);
        I.click(this.locators.changePasswordSaveButton);
        I.wait(10);
        I.see('********', '.password-section');
    },

    editProfile(phone, email, password) {
        I.fillField(this.locators.phone, phone);
        I.fillField(this.locators.confirmEmail, email);
        I.fillField(this.locators.confirmPassword, password);
        I.click(this.locators.saveBtn);
    },

    makeDefaultPayment() {
        I.click(this.locators.makeDefaultPaymentCTA);
    },

    //--------------------------------My Monogram----------------------------------------------------//
    createMonogramBtn() {
        I.seeElement(this.locators.createMonogram);
    },
    clickCreateMonogram() {
        I.click(this.locators.createMonogram);

        I.fillField(this.locators.firstInitial, 'A');
        I.fillField(this.locators.secondInitial, '1');
        I.click(this.locators.monogramSymbol + "[1]/span");
        I.click(this.locators.page1Next);
        I.click(this.locators.monogramFonts + "[2]/span");
        I.click(this.locators.page2Next);
        I.click(this.locators.saveAsMymonogram);
    },
    verifyMymonogramCreation() {
        I.seeElement(this.locators.myMonogramIcon);
    },
    updateMonogramBtn() {
        I.wait(2);
        I.click(this.locators.updateMymonogram);
    },
    updateMyMonogram() {
        I.seeElement(this.locators.monogramForm);
    },
    monogramUpdated() {
        I.seeElement(this.locators.myMonogramIcon);
    },
    seedeleteMonogramBtn() {
        I.wait(2);
        I.seeElement(this.locators.removeMymonogram);
    },
    deleteMymonogram() {
        I.click(this.locators.removeMymonogram);
    },
    verifyMymonogramRemoved() {
        I.dontSeeElement(this.locators.myMonogramIcon);
    }
};
