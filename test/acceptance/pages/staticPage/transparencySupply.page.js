const I = actor();

module.exports = {

    locators: {
        pageTitle: "//h1",
        transparencySupplyInFooter: "//a[text()='Transparency in Supply Chain Act']",
    },

    clickTransparencySupply() {
        I.scrollTo(this.locators.transparencySupplyInFooter);
        I.click(this.locators.transparencySupplyInFooter);
    },

    verifyPageTitle(pageTitle) {
        I.see(pageTitle, this.locators.pageTitle)
    }
}