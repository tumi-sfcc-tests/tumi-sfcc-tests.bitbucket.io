const I = actor();

module.exports = {
    locators: {
        modernSlaveryInFooter: "//a[text()='Modern Slavery Statement']",
        pageTitle: "//div[@class='container static-page']/h2",
    },

    clickModernSlavery() {
        I.click(this.locators.modernSlaveryInFooter);
    },

    verifyPageTitle() {
        I.see('MODERN SLAVERY AND HUMAN TRAFFICKING STATEMENT 2020', this.locators.pageTitle);
    }
}   