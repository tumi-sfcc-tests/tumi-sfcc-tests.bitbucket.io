const { I, loginPage } = inject();

module.exports = {

    locators: {
        footer_ServiceRepairs: "//a[text()='Service & Repairs']",
        pageTitle: "//h1",
        requestRepairBtn: "//span[@class='request-a-repair']",
        repairStatusBtn: "//span[@class='check-status']",
        signInBtn: "//a[@data-login='sign-in']",
        createAccount: "//a[@data-login='create-account']",

    },

    async verifyPageTitle(pageTitle) {
        let title = await I.grabTextFrom(this.locators.pageTitle);
        I.assertEqualIgnoreCase(title, pageTitle);
    },

    userSignIn(email, password) {
        I.click(this.locators.signInBtn);
        loginPage.login(email, password);
        I.refreshPage();
    }
}