const I = actor();

module.exports = {

    locators: {
        pageTitle: ".category-title",
        breadcrumb: "//ol[@class='breadcrumb']",
        bookmark_ShippingMethodsCosts: "//a[@href='#shipping-methods-and-costs']",
        bookmark_ItemAvailability: "//a[@href='#item-availability']",
        bookmark_TrackingYourOrder: "//a[@href='#tracking-your-order']",
        learnMoreLink: "//a[contains(text(),'Learn more')]",

        section_ShippingMethodsCosts: "//div[@id='shipping-methods-and-costs']",
        section_ItemAvailability: "//div[@id='item-availability']",
        section_TrackingYourOrder: "//div[@id='tracking-your-order']",

        trackYourOrder_SignIn: "//a[@href='/login']",
        trackYourOrder_OrderHistory: "//a[@href='/my-account']",
        emailAddress: "//a[@href='mailto:service@tumi.com']",


    },

    verifyPageTitle(pageTitle) {
        // I.see(pageTitle, this.locators.pageTitle)
        I.see(pageTitle);
    },

    verifyBreadcrumb(breadcrumb_text) {
        I.see(breadcrumb_text, this.locators.breadcrumb);
    },

    verifyBookmarks() {
        I.seeElement(this.locators.bookmark_ShippingMethodsCosts);
        I.seeElement(this.locators.bookmark_ItemAvailability);
        I.seeElement(this.locators.bookmark_TrackingYourOrder);
    },

    verifyShippingMethodsCostsRedirection() {
        I.click(this.locators.bookmark_ShippingMethodsCosts);
        I.seeElement(this.locators.section_ShippingMethodsCosts);
    },
    verifyItemAvailabilityRedirection() {
        I.click(this.locators.bookmark_ItemAvailability);
        I.seeElement(this.locators.section_ItemAvailability);
    },
    verifyTrackingYourOrderRedirection() {
        I.click(this.locators.bookmark_TrackingYourOrder);
        I.seeElement(this.locators.section_TrackingYourOrder);
    },

    verifyLinkNavigation_SignIn() {
        I.click(this.locators.trackYourOrder_SignIn);
        //var pageTitle = I.getPageTitle();
        I.seeInCurrentUrl('/login');
    },

    verifyLinkNavigation_OrderHistory() {
        I.click(this.locators.trackYourOrder_SignIn);
        I.seeInCurrentUrl('/my-account');
    },

    verifyEmail() {

    }



}