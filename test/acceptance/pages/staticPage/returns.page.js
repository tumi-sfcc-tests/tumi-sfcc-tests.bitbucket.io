const I = actor();

module.exports = {

    locators: {
        pageTitle: "//h1",

    },
    verifyPageTitle(pageTitle) {
        I.see(pageTitle, this.locators.pageTitle)
    }
}