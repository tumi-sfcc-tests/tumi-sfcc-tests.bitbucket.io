const I = actor();

module.exports = {

    locators: {
        pageTitle: ".col-md-12>h1",
        regionUnitedStates: "//h3[text()='United States']",
        regionCanada: "//h3[text()='CANADA']",
        regionEurope: "//h3[text()='Europe']",
        regionAsia: "//h3[text()='Asia']",
    },

    verifyPageTitle(pageTitle) {
        I.see(pageTitle, this.locators.pageTitle);
    }
}