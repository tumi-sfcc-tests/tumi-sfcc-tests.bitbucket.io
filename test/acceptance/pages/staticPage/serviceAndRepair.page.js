const I = actor();

module.exports = {

    locators: {
        pageTitle: "//div[@class='service-header']/h1",
        serviceInFooter: "//a[contains(text(),'Service & Repairs')]",

    },
    verifyPageTitle(pageTitle) {
        I.waitForElement(this.locators.pageTitle);
        I.see(pageTitle, this.locators.pageTitle);
    },

    clickServiceAndRepairInFooter() {
        I.click(this.locators.serviceInFooter);

    }
}