const I = actor();

module.exports = {

    locators: {
        keywords: "//ul[@class='categories']/li",
        questions: "//div[@class='faq-content-container']/div",
        //breadcrumbs: "//ul[@class='navbreadcrumb']",

        faqFooter: "//a[text()='FAQs']",

        keyStorePickup: "//li[@rel='store-pickup-and-curbside']",
        keyShipping: "//li[@rel='shipping']",
        keyOrdersStatus: "//li[@rel='orders']",
        keyRepairs: "//li[@rel='repairs']",
        keyTracerReg: "//li[@rel='tracer-registration']",
        keyReturns: "//li[@rel='returns']",
        keyProductInquiry: "//li[@rel='product-inquiry']",
        keyViewAll: "//li[@rel='view-all']",

        quesStorePickup: "//div[contains(@class,'store-pickup-and-curbside')]",
        quesShipping: "//div[contains(@class,'shipping')]",
        quesOrders: "//div[contains(@class,'orders')]",
        quesRepairs: "//div[contains(@class,'repairs')]",
        quesqaTracerReg: "//div[contains(@class,'tracer-registration')]",
        quesReturns: "//div[contains(@class,'returns')]",
        quesProductInquiry: "//div[contains(@class,'product-inquiry')]",
        quesViewAll: "//div[contains(@class,'repairs')]"
    },

    clickFAQinFooter() {
        I.click(this.locators.faqFooter);
    },

    async clickFAQsKeywords() {
        let keywordsCount = await I.grabNumberOfVisibleElements(this.locators.keywords);
        for (let i = 1; i <= keywordsCount; i++) {
            I.click("//ul[@class='categories']/li[" + i + "]");
        }
    },

    async clickFAQsQuestions() {
        let elCount = await I.grabNumberOfVisibleElements(this.locators.questions);
        for (let i = 1; i <= elCount; i++) {
            I.click("//div[@class='faq-content-container']/div[" + i + "]");
            I.seeElement("//div[@class='faq-content-container']/div[" + i + "]/div[@class='answer']")
        }
    }
}