const I = actor();

module.exports = {

    locators: {
        pageTitle: "//h1"
    },

    verifyPageTitle(headerTitle) {
        I.see(headerTitle, this.locators.pageTitle)
    }
}