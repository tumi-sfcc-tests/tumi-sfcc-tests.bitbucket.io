const I = actor();

module.exports = {

    locators: {
        pageTitle: "//h1",
    },

    async verifyPageTitle(pageTitle) {
        let title = await I.grabTextFrom(this.locators.pageTitle);
        I.assertEqualIgnoreCase(title, pageTitle);
    }
}