const I = actor();

module.exports = {

    locators: {
        pageTitle: ".static-title",
    },

    verifyPageTitle(pageTitle) {
        // I.see(pageTitle, this.locators.pageTitle);
        I.see(pageTitle);
    }
}