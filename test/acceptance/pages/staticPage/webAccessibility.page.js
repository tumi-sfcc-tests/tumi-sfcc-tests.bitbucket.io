const I = actor();

module.exports = {
    locators: {
        webAccessibilityInFooter: "//a[text()='Web Accessibility Statement']",
        pageTitle: "//div[contains(@class,'web-accessibility')]/h1",
        webContentAccessibility: "//a[text()='Web Content Accessibility Guidelines (WCAG)']",
        americanDisability: "//a[text()='Americans with Disabilities Act (ADA)']",
        assistiveCX: "//a[text()='assistive CX technology application']",
    },

    clickWebAccessibility() {
        I.click(this.locators.webAccessibilityInFooter);
    },

    verifyPageTitle(pageTitle) {
        I.see(pageTitle, this.locators.pageTitle);
    }

}