const I = actor();

module.exports = {

    locators: {
        pageTitle: "//h1",
        tumiLinkInFooter: "//a[text()='Setting Your TUMI Lock']"
    },

    clickTumiLockInFooter() {
        I.scrollTo(this.locators.tumiLinkInFooter);
        I.click(this.locators.tumiLinkInFooter);
    },

    verifyTumiLockPage() {
        I.see('Padlock');
        I.see('Dual Integrated Lock');
        I.see('Integrated Lock');
        I.see('Low Profile Integrated Lock');
        I.see('Snap Latch Lock');
        I.see('Briefcase Lock');
    }
}