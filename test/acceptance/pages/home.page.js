const { I, data, utilities } = inject();

module.exports = {
    locators: {

        loggedInUsername: ".welcome-user",
        closeSignedInModal: "#signedInModal .close",
        categories: "#sub-hero-category-tab",
        categoryNext: "//div[@class='categories-next']",
        categoryPrevious: "div[class='categories-prev']",
        collections: "#sub-hero-collection-tab",
        // loginButton: "//div[contains(@class,'user')]",
        // loginButton: ".accountheader",
        loginButton: ".header-profile",
        loggedUsername: "//a[@id='myaccount']",
        tumiLogo: "//div[@class='logo']/a",
        superCatgry_Luggege: "#luggage",
        superCatgry_Backpacks: "#backpacks",
        superCatgry_Bags: "#bags",
        superCatgry_Accessories: "#accessories",
        superCatgry_Collections: "#collections",
        superCatgry_Gifting: "#gifting",
        superCatgry_Personalization: "#personalization",
        superCatgry_Service: "#services",
        superCatgry_Sale: "#sale",

        headerSearchIcon: "//button[@aria-label='Search']",
        headerAccountIcon: "//li[@class='accountheader']/div/a",
        headerCartIcon: "//div[@class='minicart']",
        headerSavedItemIcon: "//li[@class='saved-item-icon']",

        storeInService: "//div[@id='header-store-loc']",

        scrollForComponent: "#homePageOne",
        heroComponent: ".hero-banner",
        bestSellerComponent: "//h2[contains(text(),'Bestseller')]",
        tumiAtYourServiceComponent: "//h3[normalize-space()='At Your Service']",
        tumiStoreComponent: "//div[@id='home-your-store']",
        tumiDifferenceComponent: "//div[contains(@data-creative-name,'TumiDifference')]",
        recommendationComponent: ".rfk-carosuel-container",
        pixleeComponent: ".pixlee-container",

        footer_TUMILogo: ".footer .logo",
        footerSection: "//footer[contains(@class,'footer')]",
        footer_storeImg: "//div[@class='store-image']/img",
        footer_CustomerService: "//a[@id='footer-customer-service']",
        footer_Shipping: "//a[normalize-space()='Shipping']",
        footer_Returns: "//a[normalize-space()='Returns']",
        footer_PaymentMethods: "//a[normalize-space()='Payment Methods']",
        footer_ServiceRepairs: "//a[normalize-space()='Service & Repairs']",
        footer_ProductInfo: "//a[normalize-space()='Product Info and Warranty']",
        footer_AirlineGuide: "//a[normalize-space()='Airline Carry-On Guide']",
        footer_FAQs: "//a[normalize-space()='FAQs']",
        footer_GiftCards: "//a[normalize-space()='Gift Cards']",
        footer_ReplacementParts: "//a[normalize-space()='Replacement Parts']",
        footer_TumiLock: "//a[normalize-space()='Setting Your TUMI Lock']",

        footer_MyAccount: "//a[@id='footer-my-account']",
        footer_SignIn: "//ul[@class='footer-menu-links']//a[normalize-space()='Sign In']",
        footer_TrackOreder: "//ul[@class='footer-menu-links']//a[normalize-space()='Track Orders']",
        footer_RegisterYourTUMI: "//div[@class='footer-register']",

        footer_AboutTUMI: "//a[@id='footer-about-tumi']",
        footer_TumiDifference: "//ul[@class='footer-menu-sub-links']//a[normalize-space()='TUMI Difference']",
        footer_CorporateGift: "//ul[@class='footer-menu-sub-links']//a[normalize-space()='Corporate Gifts & Incentives']",
        footer_Career: "//ul[@class='footer-menu-sub-links']//a[normalize-space()='Careers']",

        footer_ShipToCountry: "//div[contains(@class,'ship-to')]",
        footer_CopyrightLinks: "//ul[@class='footer-copyright-links']//a[contains(text(),'TUMI, Inc')]",

        footer_CanadaULC: "//ul[@class='footer-copyright-links']//a[contains(text(),'Tumi Canada, ULC')]",
        footer_TermsCondition: "//ul[@class='footer-copyright-links']//a[contains(text(),'Terms & Conditions')]",
        footer_Transparency: "//ul[@class='footer-copyright-links']/li[4]",
        footer_WebAccessibility: "//ul[@class='footer-copyright-links']/li[5]",
        footer_ModernSlavery: "//ul[@class='footer-copyright-links']//a[contains(text(),'Modern Slavery Statement')]",
        footer_PrivacyPolicy: "//ul[@class='footer-copyright-links']//a[contains(text(),'Privacy Policy')]",
        footer_Sitemap: "//ul[@class='footer-copyright-links']//a[contains(text(),'Sitemap')]",
        footer_GetLatestNews: "//div[@class='footer-latest-news']",

        footerSocialIcon_Instagram: "//ul[@class='footer-social-list']/li[1]",
        footerSocialIcon_TikTok: "//ul[@class='footer-social-list']/li[2]",
        footerSocialIcon_Facebook: "//ul[@class='footer-social-list']/li[3]",
        footerSocialIcon_Twitter: "//ul[@class='footer-social-list']/li[4]",
        footerSocialIcon_Youtube: "//ul[@class='footer-social-list']/li[5]",
        footerSocialIcon_Pinterest: "//ul[@class='footer-social-list']/li[6]",

        footerNewsletterEmail: "//input[@id='email-alert-address']",
        footerNewsletterBtn: "//form[@id='emarsys-newsletter-subscription']//button",
        footerNewsletterMsg: "//div[@class='email-subscription-response']",

        productHighlightSection: "//div[@data-creative-name='hero-storytelling-component']",
        productHighlightImage: "//div[@data-creative-name='hero-storytelling-component']//img",
        productHighlightContent: "//div[contains(@class,'product-highlight-content')]",

        storeNameFooter: "//div[@class='footer-store-info-details']/div/p",
        tumiLogo: "//a[@title='Tumi Home']",
        change: "//div[@class='footer-store-info-details']/div/p/a",
        map: "//div[@aria-label='Map']",
        postalCode: "#store-postal-code",
        findStore: "//button[contains(@class,'btn-storelocator-search')]",
        storeName1: "(//h5[@class='locator-storename'])[1]",
        saveStoreRadio: "(//div[contains(@class,'radio')])[1]",
        storeName: "#store-location",
        storeHoursAndServiceBtn: "//a[contains(@class,'primary appointment')]",
        callUSCta: "#services",
        storeNameOnService: "//div[@class='address pb-3']/p",

    },
    grasp: {
        storeSelectedNow: ""
    },

    search(product) {
        I.fillField(this.locators.searchField, product);
        I.waitForElement(this.locators.searchedImage);
        I.click(this.locators.searchedImage);
    },

    clickCart() {
        I.click(this.locators.headerCartIcon);
    },

    clickTumiLogo() {
        I.click(this.locators.tumiLogo);
    },

    navigateToLoginPage() {
        I.waitForClickable(this.locators.loginButton, 20);
        I.click(this.locators.loginButton);
    },

    clickSavedItemsIcon() {
        I.click(this.locators.headerSavedItemIcon);
    },

    varifySavedItemIcon() {
        I.waitForVisible(productDetailsPage.locators.globalSavedItemIcon);
        I.click(productDetailsPage.locators.globalSavedItemIcon);
        I.waitForVisible(productDetailsPage.locators.wishlistWrappper);
    },

    async verifyUsername(username) {
        I.see(username, this.locators.loggedUsername);
    },

    async launchSocialPages(socialPage) {
        switch (socialPage.toUpperCase()) {
            case 'FACEBOOK':
                I.click(this.locators.footerSocialIcon_Facebook);
                break;
            case 'TWITTER':
                I.click(this.locators.footerSocialIcon_Twitter);
                break;
            case 'PINTEREST':
                I.click(this.locators.footerSocialIcon_Pinterest);
                break;
            case 'INSTAGRAM':
                I.click(this.locators.footerSocialIcon_Instagram);
                break;
            case 'YOUTUBE':
                I.click(this.locators.footerSocialIcon_Youtube);
                break;
            case 'TIKTOK':
                I.click(this.locators.footerSocialIcon_TikTok);
                break;
            default:
            //code block
        }
        I.wait(5);
    },

    async verifySocialPageInNewTab(socialPage) {
        var url = "";
        switch (socialPage.toUpperCase()) {
            case 'FACEBOOK':
                url = data.socialURLs.facebook;
                break;
            case 'TWITTER':
                url = data.socialURLs.twitter;
                break;
            case 'PINTEREST':
                url = data.socialURLs.pinterest;
                break;
            case 'INSTAGRAM':
                url = data.socialURLs.instagram;
                break;
            case 'YOUTUBE':
                url = data.socialURLs.youtube;
                break;
            case 'TIKTOK':
                url = data.socialURLs.tiktok;
                break;
            default:
            //code block
        }
        const windows = await I.grabAllWindowHandles();
        I.say('windows handle-- ' + windows.length);
        if (windows.length > 1) {
            I.switchToNextTab();
            var currentURL = await I.grabCurrentUrl();
            I.assertContain(currentURL, url)
        }
    },


    verifyHomePageComponents(component) {
        I.scrollTo(this.locators.scrollForComponent);
        I.wait(2);
        switch (component.toUpperCase()) {
            case 'HERO COMPONENT':
                I.waitForVisible(this.locators.heroComponent, 20);
                break;
            case 'COLLECTIONS AND CATEGORIES':
                I.scrollTo(this.locators.categories);
                I.seeElement(this.locators.categories);
                I.seeElement(this.locators.collections);
                break;
            case 'BESTSELLER':
                I.scrollTo("#myTabContent");
                I.wait(3);
                I.scrollIntoView(this.locators.bestSellerComponent);
                I.seeElement(this.locators.bestSellerComponent);
                break;
            case 'TUMI AT YOUR SERVICE"':
                I.scrollTo(this.locators.tumiAtYourServiceComponent);
                I.seeElement(this.locators.tumiAtYourServiceComponent);
                break;
            case 'STORE LOCATOR':
                I.seeElement(this.locators.tumiStoreComponent);
                break;
            case 'TUMI DIFFERENCE':
                I.seeElement(this.locators.tumiDifferenceComponent);
                break;
            case 'PIXLEE':
                I.scrollPageToBottom();
                I.wait(2);
                I.scrollIntoView(this.locators.pixleeComponent)
                I.seeElement(this.locators.pixleeComponent);
                break;
            case 'RECOMMENDATION':
                I.scrollPageToBottom();
                I.wait(2);
                I.scrollIntoView(this.locators.recommendationComponent)
                I.seeElement(this.locators.recommendationComponent);
                break;
            default:
                I.say("No case found");
                break;
        }
    },

    addEmailInNewsLetter(email) {
        I.scrollIntoView(this.locators.footerNewsletterEmail);
        I.waitForElement(this.locators.footerNewsletterEmail, 5);
        I.fillField(this.locators.footerNewsletterEmail, email);
        I.click(this.locators.footerNewsletterBtn);
        I.wait(3);
    },

    verifyNewsLetterMsg(statusMsg) {
        I.see(statusMsg, this.locators.footerNewsletterMsg);
    },

    async openSuperMenu(superMenu) {
        switch (superMenu.toUpperCase()) {
            case 'LUGGAGE':
                I.click(this.locators.superCatgry_Luggege);
                break;
            case 'BACKPACKS':
                I.click(this.locators.superCatgry_Backpacks);
                break;
            case 'BAGS':
                I.click(this.locators.superCatgry_Bags);
                break;
            case 'ACCESSORIES':
                I.click(this.locators.superCatgry_Accessories);
                break;
            case 'COLLECTIONS':
                I.click(this.locators.superCatgry_Collections);
                break;
            case 'GIFTS':
                I.click(this.locators.superCatgry_Gifting);
                break;
            case 'PERSONALIZATION':
                I.click(this.locators.superCatgry_Personalization);
                break;
            case 'SERVICE':
                I.click(this.locators.superCatgry_Service);
                break;
            case 'SALE':
                I.click(this.locators.superCatgry_Sale);
                break;
            default:
                I.say("No tab found");
                break;
        }
    },

    async verifySuperCatgryPage(superMenu) {
        switch (superMenu.toUpperCase()) {
            case 'LUGGEGE':
                I.click(this.locators.superCatgry_Luggege);
                break;
            case 'BACKPACKS':
                I.click(this.locators.superCatgry_Backpacks);
                break;
            case 'BAGS':
                I.click(this.locators.superCatgry_Bags);
                break;
            case 'ACCESSORIES':
                I.click(this.locators.superCatgry_Accessories);
                break;
            case 'COLLECTIONS':
                I.click(this.locators.superCatgry_Collections);
                break;
            case 'GIFTING':
                I.click(this.locators.superCatgry_Gifting);
                break;
            case 'PERSONALIZATION':
                I.click(this.locators.superCatgry_Personalization);
                break;
            case 'SERVICE':
                I.click(this.locators.superCatgry_Service);
                break;
            case 'SALE':
                I.click(this.locators.superCatgry_Sale);
                break;
            default:
        }
        I.wait(2);
    },

    async verifySuperCategoryDrawers(superMenu) {
        switch (superMenu.toUpperCase()) {
            case 'LUGGAGE':
                // I.moveCursorTo(this.locators.superCatgry_Luggege);
                await utilities.mouseHoverInSafari(this.locators.superCatgry_Luggege);
                I.seeElement("//a[normalize-space()='Shop all Luggage']");
                break;
            case 'BACKPACKS':
                // I.moveCursorTo(this.locators.superCatgry_Backpacks);
                await utilities.mouseHoverInSafari(this.locators.superCatgry_Backpacks);
                I.seeElement("//a[normalize-space()='Shop all Backpacks']");
                break;
            case 'BAGS':
                // I.moveCursorTo(this.locators.superCatgry_Bags);
                await utilities.mouseHoverInSafari(this.locators.superCatgry_Bags);
                I.seeElement("//a[normalize-space()='Shop all Bags']");
                break;
            case 'ACCESSORIES':
                // I.moveCursorTo(this.locators.superCatgry_Accessories);
                await utilities.mouseHoverInSafari(this.locators.superCatgry_Accessories);
                I.seeElement("(//a[normalize-space()='Shop all Accessories'])[1]");
                break;
            case 'COLLECTIONS':
                // I.moveCursorTo(this.locators.superCatgry_Collections);
                await utilities.mouseHoverInSafari(this.locators.superCatgry_Collections);
                I.seeElement("//a[@id='collections-travel-tab']");
                break;
            case 'GIFTS':
                // I.moveCursorTo(this.locators.superCatgry_Gifting);
                await utilities.mouseHoverInSafari(this.locators.superCatgry_Gifting);
                I.seeElement("#gift-cards");
                break;
            case 'PERSONALIZATION':
                // I.moveCursorTo(this.locators.superCatgry_Personalization);
                utilities.mouseHoverInSafari(this.locators.superCatgry_Personalization);
                break;
            case 'SERVICE':
                // I.moveCursorTo(this.locators.superCatgry_Service);
                utilities.mouseHoverInSafari(this.locators.superCatgry_Service);
                I.seeElement("//span[contains(text(),'Our Warranty')]");
                break;
            //Drawer hidden
            // case 'SALE':
            //     // I.moveCursorTo(this.locators.superCatgry_Sale);
            //     utilities.mouseHoverInSafari(this.locators.superCatgry_Sale);
            //     I.seeElement("//a[normalize-space()='Shop all Sale']");
            //     break;
            default:
        }
    },

    verifyUserIsHomepage() {
        I.seeElement("#sub-hero-category-tab");
    },

    verifyHeaderMenus() {
        I.seeElement(this.locators.superCatgry_Accessories);
        I.seeElement(this.locators.superCatgry_Backpacks);
        I.seeElement(this.locators.superCatgry_Bags);
        I.seeElement(this.locators.superCatgry_Collections);
        I.seeElement(this.locators.superCatgry_Gifting);
        I.seeElement(this.locators.superCatgry_Luggege);
        I.seeElement(this.locators.superCatgry_Personalization);
        I.seeElement(this.locators.superCatgry_Sale);
        I.seeElement(this.locators.superCatgry_Service);
    },

    verifyHeaderNavIcons() {
        I.seeElement(this.locators.headerSearchIcon);
        I.seeElement(this.locators.headerAccountIcon);
        I.seeElement(this.locators.headerCartIcon);
    },

    verifyTheCategoryAndCollectionSwitch() {
        I.seeElement(this.locators.categories);
        I.waitForClickable(this.locators.collections);
        I.wait(2);
    },

    verifyStoreinServiceDrawer() {
        I.moveCursorTo(this.locators.superCatgry_Service);
        I.wait(2);
        I.seeElement(this.locators.storeInService);
    },

    clickProductWarrantyInFooter() {
        I.click(this.locators.footer_ProductInfo);
    },
    async seeStoreName() {
        I.refreshPage();
        I.scrollIntoView(this.locators.storeNameFooter);
        I.seeElement(this.locators.storeNameFooter);
        let selectedStore = await I.grabTextFrom(this.locators.storeNameFooter);
        console.log(selectedStore);
        if (selectedStore === "TUMI Store - Pentagon City") {
            this.grasp.storeSelectedNow = "TUMI Store - Pentagon City";
        }
        else {
            this.grasp.storeSelectedNow = "TUMI Store - City Center";
        }
    },
    clickChange() {
        I.click(this.locators.change);
    },
    clickTumiLogo() {
        I.click(this.locators.tumiLogo)
    },
    NavStoreFinder() {
        I.seeElement(this.locators.map)
    },
    clickLocationField() {
        if (this.grasp.storeSelectedNow = "TUMI Store - Pentagon City") {
            I.fillField(this.locators.postalCode, "TUMI Store - City Center");
        }
        else {
            I.fillField(this.locators.postalCode, "TUMI Store - Pentagon City");
        }
    },
    clickFindStore() {
        I.click(this.locators.findStore);
    },
    verifyStoreTitle() {
        if ("TUMI Store - Pentagon City") {
            I.see("TUMI Store - City Center", this.locators.storeName1);
        }
        else {
            I.see("TUMI Store - Pentagon City", this.locators.storeName1);
        }
    },
    clickSaveStore() {
        I.click(this.locators.saveStoreRadio);
    },
    verifyStoreNamefooter() {
        if ("TUMI Store - Pentagon City") {
            I.see("TUMI Store - City Center", this.locators.storeNameFooter);
        }
        else {
            I.see("TUMI Store - Pentagon City", this.locators.storeNameFooter);
        }
    },
    verifyStoreNameBody() {
        if ("TUMI Store - Pentagon City") {
            I.see("TUMI Store - City Center", this.locators.storeNameFooter);
        }
        else {
            I.see("TUMI Store - Pentagon City", this.locators.storeNameFooter);
        }
    },
    hoverServiceNav() {
        I.moveCursorTo(this.locators.superCatgry_Service);
    },
    verifyStoreNameServiceNavCont() {
        if ("TUMI Store - Pentagon City") {
            I.see("TUMI Store - City Center", this.locators.storeNameFooter);
        }
        else {
            I.see("TUMI Store - Pentagon City", this.locators.storeNameFooter);
        }
    }
};
