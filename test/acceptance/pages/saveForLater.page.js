const { I, data, homepage } = inject();

module.exports = {
    locators: {
        globalSavedItemIcon: "//li[@class='saved-item-icon']//a[@class='darkMode-focus']",
        wishlistWrappper: ".wishlistItemWrapper",
        wishlistProduct: "//div[@class='row wishlistItemCards']//a[contains(text(),'International Expandable 4 Wheeled Carry-On')]//../../../..//div[@class='product-bookmark']",


    },

    clickSavedItemsHeaderIcon() {
        I.click(homepage.locators.headerSavedItemIcon);
    },

    verifyProductInSaveForLater(productName) {
        this.clickSavedItemsHeaderIcon();
        I.wait(2);
        I.see(productName, this.locators.wishlistWrappper);
    },

    removeSavedproduct(productName) {
        I.click("//div[@class='row wishlistItemCards']//a[contains(text(),'" + productName + "')]//../../../..//div[@class='product-bookmark']");
        I.wait(5);
        I.dontSee(productName, this.locators.wishlistWrappper);
    },

    async clearWishlist() {
        for (let k = 0; k < 10; k++) {
            let removeBtnCount = await I.grabNumberOfVisibleElements("//div[contains(@class,'wishlistItemWrapper')]//div[@class='product-bookmark']");
            if (removeBtnCount > 0) {
                I.click("(//div[contains(@class,'wishlistItemWrapper')]//div[@class='product-bookmark'])[" + 1 + "]");
                I.refreshPage();
            }
        }
    }

}