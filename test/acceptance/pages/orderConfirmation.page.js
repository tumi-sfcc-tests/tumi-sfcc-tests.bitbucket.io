const { I } = inject();

module.exports = {
    locators: {
        orderConf_thankYou: "//div[@class='hero-confirmation-note']",
        orderConf_orderNumber: "//span[@class='summary-details order-number']",
        orderConf_orderDate: "//span[@class='summary-details order-date']",
        orderConf_shippingSection: "//div[@class='summary-details shipping']",

        orderConfPage: "//div[@class='order-number']",

        orderConf_shippingMethod: "//p[@class='shipping-method']",
        orderConf_billingSection: "//div[@class='summary-details billing']",
        orderConf_paymentSection: "//div[@class='payment-details']",
        orderConf_quantity: "//span[@class='pricing qty-card-quantity-count']",
        orderConf_totalSection: "//div[@class='card-body order-total-summary']",
        continueShoppingAfterOrderConf: "//a[contains(@class,'order-confirmation-continue-shopping')]",

        //------------------------------------------Checkout Page ------------------------------------------------//
        orderTotal_checkout: "(//p[contains(@class,'grand-total')])[2]",
        subTotal_checkout: "(//p[contains(@class,'sub-total')])[2]",
        taxTotal_checkout: "(//p[contains(@class,'tax-total')])[2]",
        shippingCost_checkout: "(//p[contains(@class,'shipping-cost')])[2]",
        firstName_checkout: "//span[@class='firstName']",
        lastName_checkout: "//span[@class='lastName']",
        addressLine1_checkout: "//div[@class='address1']",
        city_checkout: "//span[@class='city']",
        state_checkout: ".stateCode",
        postalCode_checkout: ".postalCode",
        productLine1_checkout: "(//div[@class='line-item-name'])[2]",
        //----------------------------------------Confirmation Page-----------------------------------------------//
        closeModal_orderConfirmation: "//button[contains(@class,'close-btn')]",
        menu_close: "(//sbutton[@class='menu-close'])", //"(//button[@class='menu-close'])[8]",
        smsSection_orderConfirmation: "//div[@class='sms-section']",
        firstName_orderConfirmation: "//span[@class='firstName']",
        lastName_orderConfirmation: "//span[@class='lastName']",
        addressLine1_orderConfirmation: "//div[@class='address1']",
        city_orderConfirmation: "//span[@class='city']",
        state_orderConfirmation: ".stateCode",
        postalCode_orderConfirmation: ".postalCode",
        createAccountBtn_orderConfirmation: "#register-form",
        loginForm: ".login-form-nav",
        productLine1_orderConfirmation: "(//div[@class='line-item-name'])[1]",
        orderNumber_orderConfirmation: "//div[@class='order-number']/span",
        email_orderConfirmation: "//span[contains(@class,'email')]",
        subtotal_orderConfirmation: "(//div[@class='row subtotal'])[2]/div[2]/p",
        shipping_orderConfirmation: "(//div[@class='row shipping-text'])[2]/div[2]/p",
        tax_orderConfirmation: "(//div[@class='row subtotal-text'])[2]/div[2]/p",
        total_orderConfirmation: "(//div[@class='row total'])[2]/div[2]/p"
    },

    grasp: {
        fname: "",
        lname: "",
        addressLine1: "",
        city: "",
        state: "",
        postalCode: "",
        product1: "",
        shippingCost: "",
        taxTotal: "",
        subTotal: "",
        orderTotal: ""

    },

    async verifyOrderPlaced() {
        I.wait(5);
        I.waitForElement(this.locators.orderConfPage, 30);
        let orderNumber = await I.grabTextFrom(this.locators.orderConfPage);
        I.say(orderNumber);
    },

    verifyOrderConfirmation(fName, lName, add1, city, zip, phone, email, billingFname, billingLname, billingEmail, ccNum, ccExpDate, quantity,
        totalItemPrice, shipping, tax, estimatedTotal) {
        // verify order is place successfully by verifying the order confirmation page
        I.waitForElement(this.locators.orderConf_thankYou, 10);
        I.scrollTo(this.locators.orderConf_thankYou);
        I.see('Thank you for your order', this.locators.orderConf_thankYou);

        // verify shipping address is correct
        I.scrollTo(this.locators.orderConf_shippingSection);
        I.see(fName, this.locators.orderConf_shippingSection);
        I.see(lName, this.locators.orderConf_shippingSection);
        I.see(add1, this.locators.orderConf_shippingSection);
        I.see(city, this.locators.orderConf_shippingSection);
        I.see(zip, this.locators.orderConf_shippingSection);
        I.see(phone, this.locators.orderConf_shippingSection);

        // verify billing address is correct
        // I.scrollTo(this.locators.orderConf_billingSection);
        // I.see(billingFname, this.locators.orderConf_billingSection);
        // I.see(billingLname, this.locators.orderConf_billingSection);
        // I.see(add1, this.locators.orderConf_billingSection);
        // I.see(city, this.locators.orderConf_billingSection);
        // I.see(zip, this.locators.orderConf_billingSection);
        // I.see(email, this.locators.orderConf_billingSection);
        // I.see(phone, this.locators.orderConf_billingSection);


        // verify payment info is correct
        // if (ccNum != "NA") {
        //     // Leave the last 4 digits shown; replace everything else with '*'
        //     I.see(ccNum.replace(/\d(?=\d{4})/g, '*'), this.locators.orderConf_paymentSection);
        // }

        // if (ccExpDate != "NA") {
        //     //I.see(ccExpDate, this.locators.orderConf_paymentSection);
        // }

        // verify product info is correct
        // I.scrollTo(this.locators.orderConf_quantity);
        // I.see(quantity, this.locators.orderConf_quantity);

        // I.see(totalItemPrice, this.locators.orderConf_totalSection);
        // I.see(shipping, this.locators.orderConf_totalSection);
        // I.see(tax, this.locators.orderConf_totalSection);
        // I.see(estimatedTotal, this.locators.orderConf_totalSection);
    },
    async getAllDetailsCheckout() {
        this.grasp.fname = await I.grabTextFrom(this.locators.firstName_checkout);
        this.grasp.lname = await I.grabTextFrom(this.locators.lastName_checkout);
        this.grasp.shippingCost = await I.grabTextFrom(this.locators.shippingCost_checkout);
        this.grasp.taxTotal = await I.grabTextFrom(this.locators.taxTotal_checkout);
        this.grasp.subTotal = await I.grabTextFrom(this.locators.subTotal_checkout);
        this.grasp.orderTotal = await I.grabTextFrom(this.locators.orderTotal_checkout);
        this.grasp.addressLine1 = await I.grabTextFrom(this.locators.addressLine1_checkout);
        this.grasp.city = await I.grabTextFrom(this.locators.city_checkout);
        this.grasp.state = await I.grabTextFrom(this.locators.state_checkout);
        this.grasp.postalCode = await I.grabTextFrom(this.locators.postalCode_checkout);
        this.grasp.product1 = await I.grabTextFrom(this.locators.productLine1_checkout);
    },

    verifyOrderNumber() {
        I.waitForVisible(this.locators.closeModal_orderConfirmation);
        I.click(this.locators.closeModal_orderConfirmation);
        I.click(this.locators.menu_close);
        I.seeElement(this.locators.orderNumber_orderConfirmation);
    },
    async verifyShippingOrderConf() {
        let firstName_OC = await I.grabTextFrom(this.locators.firstName_orderConfirmation);
        let lastName_OC = await I.grabTextFrom(this.locators.lastName_orderConfirmation);
        let addressLine1_OC = await I.grabTextFrom(this.locators.addressLine1_orderConfirmation);
        let city_OC = await I.grabTextFrom(this.locators.city_orderConfirmation);
        let state_OC = await I.grabTextFrom(this.locators.state_orderConfirmation);
        let postalCode_OC = await I.grabTextFrom(this.locators.postalCode_orderConfirmation);
        I.assertEqual(firstName_OC, this.grasp.fname);
        I.assertEqual(lastName_OC, this.grasp.lname);
        I.assertEqual(addressLine1_OC, this.grasp.addressLine1);
        I.assertEqual(city_OC, this.grasp.city + ",");
        I.assertEqual(state_OC, this.grasp.state);
        I.assertEqual(postalCode_OC, this.grasp.postalCode);
    },
    async verifyProductDetailOrderConf() {
        let product1_OC = await I.grabTextFrom(this.locators.productLine1_checkout);
        I.assertEqual(product1_OC, this.grasp.product1);
    },
    async verifyOrderTotalOrderConf() {
        let shippingCost_OC = await I.grabTextFrom(this.locators.shipping_orderConfirmation);
        let taxTotal_OC = await I.grabTextFrom(this.locators.tax_orderConfirmation);
        let subTotal_OC = await I.grabTextFrom(this.locators.subtotal_orderConfirmation);
        let orderTotal_OC = await I.grabTextFrom(this.locators.total_orderConfirmation);
        I.assertEqual(shippingCost_OC, this.grasp.shippingCost);
        I.assertEqual(taxTotal_OC, this.grasp.taxTotal);
        I.assertEqual(subTotal_OC, this.grasp.subTotal);
        I.assertEqual(orderTotal_OC, this.grasp.orderTotal);
    },
    verifyAlertSectionOrderConf() {
        I.seeElement(this.locators.smsSection_orderConfirmation);
    }
}