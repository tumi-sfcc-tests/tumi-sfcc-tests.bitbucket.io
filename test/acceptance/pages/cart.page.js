const { pause } = require("codeceptjs");

const I = actor();
const { utilities, data } = inject();

module.exports = {
    locators: {

        headerCartIcon: "//div[@class='minicart']//a",
        cartPageHeading: "//h1[@class='your-cart-header']",
        checkoutBtn: "//form[@class='cart-action-checkout']",
        selectShippingMethods: "//select[@id='shippingMethods']",
        promocode: "//input[@id='couponCode']",
        promocodeSubmitBtn: "//button[contains(@class,'promo-code-btn')]",
        lineItemText: "//div[@class='product-card']",
        saveForLaterBtn: "//a[@title='Save for Later']",
        minicartFlyoutModal: "//div[@id='flyout-minicart']",
        // minicartFlyoutCheckout: "//div[@id='flyout-minicart']//a[contains(text(), 'Checkout')]",
        minicartFlyoutCheckout: "//div[contains(@class,'global-modal show') and @id='miniCartModal']//a[contains(text(), 'Checkout')]",
        // minicartFlyoutViewCart: "//div[@id='flyout-minicart']//a[contains(text(), 'View My Cart')]",
        minicartFlyoutViewCart: "//div[contains(@class,'global-modal show') and @id='miniCartModal']//a[contains(text(), 'View My Cart')]",
        minicartFlyoutClose: "//div[contains(@class,'minicart-flyout')]//button[@class='close']",
        editButton: "//a[normalize-space()='Edit Item']",
        updateCart: "//a[contains(@class,'update-cart-product-global')]",
        closeEditCart: "//div[@id='editProductModal']//button[contains(@class,'close pull-right')]",
        cartplus: "button[data-quantity='plus']",
        cartminus: "button[data-quantity='minus']",
        itemcount: ".number-of-items",
        cartProductsSection: "//div[@class='product-card']",
        minicartIcon: "//a[contains(@title,'Cart')]//img[@alt='Commerce Cloud Storefront Reference Architecture']",
        editFlyoutModal: "//div[@id='editProductModal']",

        addLinkGiftOptions: ".link-add",
        giftOptionsModal: "//h1[text()='Add Gift Details']",
        giftMsgTo: "#firstName",
        giftMsgFrom: "#senderName",
        giftMsgInput: "#giftMessage",
        giftSubmitBtn: "//form[@id='giftCardForm']//button",
        giftCancelBtn: "//form[@id='giftCardForm']//a[text()='Cancel']",
        giftOptionsModalClose: "//div[contains(@id,'giftCardModal')]//button[@class='close']",
        giftOptionsModalProductSection: "//div[contains(@id,'giftCardModal')]//div[@class='product-wrapper']",
        giftMsgAddedToProduct: ".gift-options",
        giftMsgEdit: "//div[@class='link-add']/a[1]",
        giftMsgRemove: "//div[@class='link-add']/a[2]",
        createAcc_cart: "(//a[@class='checkout-login'])[1]",
        signIn_cart: "(//a[@class='checkout-login'])[2]",

        confirm_Delete: "//button[contains(@class,'cart-delete-confirmation-btn')]",
        removeItem_Cart: "//button[contains(@class,'remove-btn-lg')]",
        miniCart_checkout: "//div[contains(@class,'cart-icon')]//a"
    },

    async openCartPageFromHeader() {
        I.waitForElement(this.locators.headerCartIcon, 10);
        I.click(this.locators.headerCartIcon);
    },

    clickCheckout() {
        I.waitForElement(this.locators.checkoutBtn, 5);
        I.click(this.locators.checkoutBtn);
    },

    clickFlyoutCheckout() {
        I.waitForElement(this.locators.minicartFlyoutCheckout, 5);
        I.click(this.locators.minicartFlyoutCheckout);
    },

    clickFlyoutViewCart() {
        I.waitForElement(this.locators.minicartFlyoutViewCart, 5);
        I.click(this.locators.minicartFlyoutViewCart);
    },
    clickEditItem() {
        I.waitForElement(this.locators.editButton, 5);
        I.click(this.locators.editButton);

    },
    clickSaveForLater() {
        I.waitForElement(this.locators.saveForLaterBtn, 5);
        I.click(this.locators.saveForLaterBtn);
        I.wait(5);
    },

    async clearCart() {
        this.openCartPageFromHeader();
        I.waitForVisible(this.locators.cartPageHeading, 10);

        while (await I.grabNumberOfVisibleElements("//div[contains(@class,'cart-page')]//div[@class='line-item-header']") > 0) {
            I.click("(//button[contains(@class,'remove-product')])[1]");
            I.click("//button[contains(@class, 'cart-delete-confirmation')]");
            I.wait(2);
            I.refreshPage();
        }

        //     for (let k = 0; k < 15; k++) {
        //         let removeBtnCount = await I.grabNumberOfVisibleElements("//div[contains(@class,'cart-page')]//div[@class='line-item-header']");
        //         I.say("Remove button count: " + removeBtnCount);
        //         if (removeBtnCount > 0) {
        //             I.say("INside for loop");
        //             await this.removeItemFromCart("(//button[contains(@class,'remove-product')])[1]");
        //             I.refreshPage();
        //         }
        //     }
    },

    cleanCart() {
        I.click(this.locators.miniCart_checkout)
        I.click(this.locators.removeItem_Cart);
        I.click(this.locators.confirm_Delete)
    },

    async clearSaveForLater() {
        this.openCartPageFromHeader();
        for (let k = 0; k < 10; k++) {
            let removeBtnCount = await I.grabNumberOfVisibleElements("//div[@class='wishlistItemCards']//div[@class='line-item-header']");
            if (removeBtnCount > 0) {
                I.click("(//button[contains(@class,'remove-from-wishlist')])[" + 2 + "]");
                I.refreshPage();
            }
        }
    },

    //  async clearSaveForLater() {
    //     this.openCartPageFromHeader();
    //     let removeBtnCount = await I.grabNumberOfVisibleElements("//div[@class='wishlistItemCards']//div[@class='line-item-header']");
    //     for (let i = 1; i <= removeBtnCount; i++) {
    //         I.click("(//button[contains(@class,'remove-from-wishlist')])[" + (i * 2) + "]");
    //     }
    // },

    removeItemFromCart(lineItemButtonLocator) {
        return new Promise((resolve, reject) => {
            I.waitForClickable(lineItemButtonLocator, 5);
            I.click(lineItemButtonLocator);
            I.wait(1);
            I.click("//button[contains(@class, 'cart-delete-confirmation')]");
            I.wait(2);
            return resolve();
        })
    },

    clickHeaderCartIcon() {
        I.refreshPage();
        I.click(this.locators.headerCartIcon);
        I.wait(5);
    },

    checkEditCart() {
        I.click(this.locators.editButton);
        I.wait(2);
        I.see('Edit Item', this.locators.editFlyoutModal);
    },

    closeEditCart() {
        I.click(this.locators.closeEditCart);
        I.see('Your Cart');
    },

    editCartSelectColor(color) {
        I.click("//div[@id='editProductModal']//button[@title='" + color + "']");
        I.wait(5);
    },

    async verifyProductsColorOnCartPage(color) {
        let productCounts = await I.grabNumberOfVisibleElements("//div[@class='product-card']//p[contains(text(),'Color')]");
        for (let i = 1; i <= productCounts; i++) {
            I.see(color, "(//div[@class='product-card']//p[contains(text(),'Color')])[" + i + "]");
        }
    },

    editCartAddProduct(quantity) {
        for (let i = 0; i < quantity; i++) {
            I.click(this.locators.cartplus);
        }
        I.click(this.locators.updateCart);
    },

    verifyCartPagePrductCount(count) {
        I.waitForText(count, ".number-of-items")
    },

    closeCartFlyOut() {
        I.click(this.locators.minicartFlyoutClose);
    },

    verifyDetailsOnCartFlyOut(itemName) {
        // this.clickFlyoutViewCart();
        // I.see(itemName, this.locators.lineItemText)
        I.see(itemName, this.locators.minicartFlyoutModal);
        I.seeElement(this.locators.minicartFlyoutCheckout);
        I.seeElement(this.locators.minicartFlyoutViewCart);
    },

    verifyProductIDOnCartFlyOut(sku) {
        I.seeElement("//div[@id='flyout-minicart']//img[@title='" + sku + "']");
        I.seeElement(this.locators.minicartFlyoutCheckout);
        I.seeElement(this.locators.minicartFlyoutViewCart);
    },

    addGiftOptions(toPerson, fromPerson, giftMsg) {
        I.click(this.locators.addLinkGiftOptions);
        I.seeElement(this.locators.giftOptionsModal);
        I.fillField(this.locators.giftMsgTo, toPerson);
        I.fillField(this.locators.giftMsgFrom, fromPerson);
        I.fillField(this.locators.giftMsgInput, giftMsg);
        I.click(this.locators.giftSubmitBtn);
    },

    verifyAddedGiftOptions() {
        I.waitForElement(this.locators.giftMsgEdit, 20);
        I.seeElement(this.locators.giftMsgEdit);
        I.seeElement(this.locators.giftMsgRemove);
        I.see("Message Added", this.locators.giftMsgAddedToProduct);

    },

    removeGiftOptions() {
        I.click(this.locators.giftMsgRemove);
        I.wait(2);
    },

    verifyGiftOptionsRemoved() {
        I.waitForElement(this.locators.addLinkGiftOptions, 20);
        I.dontSee("Message Added", this.locators.giftMsgAddedToProduct);
    },

    clickCreateAcc() {
        I.click(this.locators.createAcc_cart);
    },

    clickSignIn() {
        I.click(this.locators.signIn_cart);
    },



}