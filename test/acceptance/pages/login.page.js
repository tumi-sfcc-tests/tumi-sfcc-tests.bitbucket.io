const { utilities, homepage, productDetailsPage } = inject();
const I = actor();

module.exports = {
    locators: {

        myAccountIcon: "//a[@id='myaccount']",
        myAccountIconNotLoggedIn: "//a[@data-target='#requestLoginModal']",
        logicFormSection: "//form[@class='login']",
        homepageAccountIcon: "//a[@id='myaccount']",
        loggedInUsername: ".welcome-user",
        closeSignedInModal: "#signedInModal .close",
        closeGuestModal: "#requestLoginModal .close",
        emailLogin: "#existing-usercheck-form-email",
        emailContinueBtn: ".existingUserCheckForm>button",
        passwordLogin: "//input[@id='login-form-password']",
        loginSubmitBtn: "//button[@id='button-login']",
        forgotPassword: "//button[@id='password-reset']",
        rememberMe: "//input[@id='rememberMe']",

        loginWithGoogle: "//li[@id='janrain-googleplus']",
        googleEmail: "//input[@id='identifierId']",
        googlePassword: "//input[@name='password']",
        googleNext: "//span[text()='Next']",

        loginWithFacebook: "//li[@id='janrain-facebook']",
        loginTab: "//a[@id='login-tab']",
        registerTab: "//a[@id='register-tab']",
        firstName: "//input[@id='registration-form-fname']",
        lastName: "//input[@id='registration-form-lname']",
        emailAdr: "//input[@id='registration-form-email']",
        password: "//input[@id='registration-form-password']",
        addEmailToCommerceList: "//input[@id='add-to-email-list']",
        createAccountBtn: "//button[contains(text(),'Create Account')]",
        registerEmailError: "//form[@class='registration']//div[@id='form-remail-error']",
        verificationEmailMsg: ".verify-your-acount-info",
        registrationBackToSignIn: "#return-to-signin",
        resendVerificationEmail: "#resend-verification-mail",

        forgotPasswordEmail: "//input[@id='reset-password-email']",
        forgotPaswordResetLinkBtn: "//button[@id='submitEmailButton']",
        cancelReturnToLogin: "//button[@class='return-to-login']",
        backToSignin: "//a[@id='backtologin']",
        forgotPasswordModel: "//div[@class='confirmation-password-body']",
        logoutBtn: "//a[@class='signout-button']",
        closeShipping: "//button[@id='closeIconContainer']"
    },

    testDataTemp: {
        registerMail: ""
    },

    loginContinueEmail(email) {
        I.waitForVisible(this.locators.emailLogin, 20);
        I.fillField(this.locators.emailLogin, email);
        I.wait(1);
        I.click(this.locators.emailContinueBtn);
        I.wait(3);
    },

    login(email, password) {
        this.loginContinueEmail(email);
        I.waitForVisible(this.locators.passwordLogin, 20);
        I.fillField(this.locators.passwordLogin, password);
        // click login
        // I.waitForClickable(this.locators.loginSubmitBtn, 20);
        I.click(this.locators.loginSubmitBtn);
        I.wait(5);
    },

    async closeSignedInModal() {
        // I.amOnPage('/');
        // I.click(this.locators.homepageAccountIcon);
        // // I.waitForVisible(this.locators.loggedInUsername, 10);
        // // I.see(username, this.locators.loggedInUsername);
        // // I.click(this.locators.closeSignedInModal);
        await utilities.clickIfVisible(this.locators.closeSignedInModal);
    },

    closeGuestUserLoginModal() {
        I.click(this.locators.closeGuestModal);
    },

    clickRegisterTab() {
        I.click(this.locators.registerTab);
    },

    fillInRegisterForm(fName, lName, email, password) {
        this.testDataTemp.registerMail = email;
        this.loginContinueEmail(email);
        I.waitForVisible(this.locators.firstName, 20);
        I.fillField(this.locators.firstName, fName);
        I.fillField(this.locators.lastName, lName);
        // I.fillField(this.locators.emailAdr, email);
        I.fillField(this.locators.password, password);
    },

    clickCreateAccount() {
        I.click(this.locators.createAccountBtn);
        I.wait(3);
    },

    verifyLoggedInUsername(firstname) {
        I.click(this.locators.homepageAccountIcon);
        I.waitForElement(this.locators.loggedInUsername, 10);
        I.see(firstname, this.locators.loggedInUsername);
    },

    async googleLogin(email, password) {
        I.waitForElement(this.locators.loginWithGoogle);
        I.click(this.locators.loginWithGoogle);
        I.wait(2);
        I.fillField(this.locators.googleEmail, email);
        I.click(this.locators.googleNext);
        I.wait(2);
        I.fillField(this.locators.googlePassword, password);
        I.click(this.locators.googleNext);
    },

    forgotPassword(email) {
        I.wait(2); // Must wait because of modal fade crops the email param off randomly and fails the test
        I.waitForElement(this.locators.forgotPasswordEmail, 5);
        I.fillField(this.locators.forgotPasswordEmail, email);
        I.waitForElement(this.locators.forgotPaswordResetLinkBtn);
        I.forceClick(this.locators.forgotPaswordResetLinkBtn);
        I.wait(3);
    },

    verifyUserOnLoginStep() {
        I.seeElement(this.locators.loginSubmitBtn);
        I.seeElement(this.locators.passwordLogin);
    },

    verifyRegistrationMailSentMessage() {
        I.see("Verify your account.");
        I.see("We've sent an email to " + this.testDataTemp.registerMail + " with a link to verify your account.", this.locators.verificationEmailMsg);
    },
    verifyPasswordReset() {
        I.waitForElement(this.locators.forgotPasswordModel, 5);
        I.see('Please check your email.', this.locators.forgotPasswordModel);
    },

    verifyErrorMessages() {
        I.see("This is required field", ".login #form-email-error");
        I.see("This is required field", ".login #form-password-error");
    },

    verifyInvalidUserErrorMessages() {
        I.see("Invalid login or password", this.locators.logicFormSection);
    },

    verifyCreateAccountErrorMessages() {
        I.see("First Name field is required", "#form-fname-error");
        I.see("Last Name field is required", "#form-lname-error");
        I.see("Password field is required", "#form-rpassword-error");
    },

    logout() {
        I.refreshPage();
        I.click(this.locators.homepageAccountIcon);
        I.waitForElement(this.locators.logoutBtn, 5);
        I.click(this.locators.logoutBtn);
        I.wait(3);
    },

    isUserLogout() {
        I.seeElement(this.locators.myAccountIconNotLoggedIn);
    }
};
