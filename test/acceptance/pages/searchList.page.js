const { data, utilities, searchListPage } = inject();
const I = actor();

module.exports = {
    locators: {
        searchIcon: "//button[@aria-label='Search']",
        searchbox: "//input[@aria-label='Enter Keyword or Item No.']",
        suggestionList: "//div[@class='listofcategories']/ul/li",
        suggestedItemNo1: "//div[@class='listofcategories']/ul/li[1]/a",
        topResultsFirstProduct: "//div[@class='trending-item']/a",
        slpTitle: "//h1[@class='category-title'][1]",
        productList: "//div[contains(@class,'productTileTemplates')]/div",
        showFilter: ".filter",
        collectionFilter: "//div[@id='collection']",
        selectFirstCollectionFilter: "//div[@id='refinement-collection']/ul/li[1]/div",
        plpProductName: "//div[contains(@class,'productTileTemplates')]/div[1]"
    },

    searchProduct(searchedProduct) {
        I.wait(2);
        I.fillField(this.locators.searchbox, searchedProduct);
    },

    async verifyCategorySuggestion(searchedProduct) {
        I.wait(2);
        I.seeElement(this.locators.suggestionList);
        I.see(searchedProduct, this.locators.suggestedItemNo1);
        I.seeElement(topResultsFirstProduct)
    },

}