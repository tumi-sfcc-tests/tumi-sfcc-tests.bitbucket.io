const { I, data, utilities } = inject();

module.exports = {
    locators: {
        checkoutPage: "//div[@id='checkout-main']",
        emailGuest: "(//input[@id='email-guest'])[1]",
        checkoutAsGuestBtn: "//form[@id='guest-customer']//button[contains(@class,'submit-customer')]",

        checkoutAsRegisteredBtn: '.btn.btn-block.btn-primary',
        emailRegistered: '#email',
        password: '#login-form-password',

        fName: "//input[@id='shippingFirstNamedefault']",
        lName: "//input[@id='shippingLastNamedefault']",
        address1: "//input[@id='shippingAddressOnedefault']",
        country: "//select[@id='shippingCountrydefault']",
        state: "//select[@id='shippingStatedefault']",
        city: "//input[@id='shippingAddressCitydefault']",
        zip: "//input[@id='shippingZipCodedefault']",
        phone: "#shippingPhoneNumberdefault",
        shippingHdr: "(//h2[@class='card-header-custom'])[1]",
        signUpEmailChkBox: "//label[@for='flexCheckDefault']",
        shippingSelected: "(//label[@class='form-check-label shipping-method-option'])[1]/span",
        shippingTime: "(//label[@class='form-check-label shipping-method-option'])[1]/div",
        shippingCost: "(//span[@class='shipping-cost'])",
        shippingTypeName: "(//label [contains(@class,'shipping-method-option')])",
        shipLink: "(//a[@class='choose-expedit-shipping-anchor'])[1]",
        addressSuggested1: "//div[@id='edq-verification-suggestion-box']/div[1]",
        shipCost: "(//span[@class='shipping-cost'])",

        billing_fName: "#billingFirstName",
        billing_lName: "#billingLastName",
        billing_address1: "#billingAddressOne",
        billing_country: "#billingCountry",
        billing_state: "#billingState",
        billing_city: "#billingAddressCity",
        billing_zip: "#billingZipCode",

        nextPayment: "//button[contains(@class,'submit-shipping')]",
        giftCardCheckbox: "//input[@id='pay-with-giftcard']/..",
        creditCardTab: "//li[@data-method-id='CREDIT_CARD']",
        payPalTab: "//li[@data-method-id='PayPal']/a",
        selectKlarna: "//li[@data-method-id='klarna_pay_over_time']/a",

        changePaymentMethod: "//a[contains(@class,'change-payment-method')]",

        payPhone_top: "//fieldset[@class='contact-info-block']//input[@id='phoneNumber']",
        // payCard: "//input[@name='credit-card-number']",
        payCard: "//input[@name='number']",
        payName: "(//input[@id='fullName'])[1]",
        payExpMonth: "//select[@id='expirationMonth']",
        payExpYear: "//select[@id='expirationYear']",
        // paySecCode: "//input[@id='securityCode']",
        paySecCode: "//input[@name='securityCode']",
        cvvForSavedCard: "//input[@id='saved-payment-security-code']",
        saveCreditCardCheckbox: "//label[@class='custom-control-label']",

        payEmail: "//fieldset[@class='payment-form-fields']//input[@id='phoneNumber']",
        payPhone_below: "//fieldset[@class='payment-form-fields']//input[@id='phoneNumber']",
        nextplaceOrder: "//button[@value='submit-payment']",

        placeOrderFinal: "//button[@id='submit-order']",
        nextplaceOrderFinal: "//button[normalize-space()='Next: Place Order']",
        payPalSelectAccount: "//select[@id='restPaypalAccountsList']",
        continueShipping: "//button[@value='continue-shipping']",
        continueBilling: "//button[@value='continue-billing']",
        payPalCheckoutBtn: "//div[@id='buttons-container']",
        payPalDabitAndCreditCardBtn: "(//div[@class='paypal-button-label-container'])[2]",

        payPalChangeEmailBtn: "//a[@id='backToInputEmailLink']",
        payPalEmail: "//input[@id='email']",
        paypalNextBtn: "//button[@id='btnNext']",
        payPalPassword: "//input[@id='password']",
        payPalLoginBtn: "//button[@id='btnLogin']",
        payPalContinue: "//button[@id='payment-submit-btn']",
        payPalPayIn4: "//span[text()='Pay in 4']",
        payPalPayIn4CardSelect: "//p[@data-testid='fiName']",
        payPalPayIn4Continue: "//button[@id='submitButton']",
        payPalPayIn4Frame: "//iframe[@data-testid='cap-iframe']",
        payPalPayIn4TermsCheckBox: "//input[@data-testid='repaymentCheckbox']/..",
        payPalCredit: "//span[text()='PayPal Credit']",
        payPalPayWithCard: "//span[text()='CREDIT UNION 1']",

        // Gift card section
        giftCardNumer: "//input[@id='giftcard-balance-form-accountnumber']",
        giftPinNumber: "//input[@id='giftcard-balance-form-pinnumber']",
        giftCardRedeemBtn: "//button[contains(@class,'btn-giftcard-redeem')]",
        giftCardRemove: ".gc-remove>a",
        giftCardDetails: ".gc-detail>p",

        /* Klarna */
        klarnaCheckoutBtn: "//div[@class='continue-billing-klarna']/button",
        klarna_StartContinueBtn: "#start-button",
        klarna_PhoneContinueBtn: "#onContinue",

        klarna_PhoneInput: "//input[@name='phone']",
        klarna_OtpInput: "//input[@id='otp_field']",
        // klarna_PickPlanContinueBtn: "//button[@data-testid='pick-plan']",
        klarn_selectPayNow: "//label[@for='pay_now__label']",
        klarna_SelectPaymentContinueBtn: "//button[@data-testid='pick-plan']",
        klarna_ConfirmAndPay: "//button[@id='buy_button']",
        klarna_skipFasterCheckout: "//button[@data-testid='SmoothCheckoutPopUp:skip']",
        klarna_placeOrder: ".klarna-place-order",

        checkoutStage: '.data-checkout-stage',
        paySecCodeSaved: '.form-control.saved-payment-security-code',
        billingConfirmation: '.addressSelector.form-control',
        shipping_addSelector: '.addressSelector',
        shipping_methodBlock: '.shipping-method-block',
        shipping_methodOptions: '.form-check.col-9.start-lines',

        checkout_shippingSection: '.card.shipping-summary',
        checkout_prefilledShippingInfo: '.addressSelector.form-control',
        checkout_paymentSection: '.card.payment-summary',
        checkout_orderSummary: '.card-body.order-total-summary',
        checkout_orderQantity: "//span[@class='pricing qty-card-quantity-count']",

        homeLink: "//a[@class='logo-home']",
        taxTotal: "//div[@class='checkout-content-right']//p[contains(@class,'tax-total')]",
        cvv: "(//input[@id='saved-payment-security-code'])[2]",
    },

    userIsOnCheckoutPage() {
        I.wait(5);
        I.seeElement(this.locators.checkoutPage, 20);
    },

    fillPersonalDataGuest(email) {
        I.waitForElement(this.locators.emailGuest);
        I.fillField(this.locators.emailGuest, email);
    },

    clickContinueAsGuest() {
        I.click(this.locators.checkoutAsGuestBtn);
        I.wait(2);
    },

    fillShippingInfo(fName, lName, address1, state, city, zipcode, phone) {
        I.waitForVisible(this.locators.fName, 10);
        I.scrollTo(this.locators.fName);
        I.fillField(this.locators.fName, fName);
        I.fillField(this.locators.lName, lName);
        I.fillField(this.locators.address1, address1);
        I.wait(2);
        // I.pressKey("Escape");
        I.click(this.locators.lName);
        I.fillField(this.locators.city, city);
        I.waitForElement(this.locators.state);
        I.selectOption(this.locators.state, state);
        I.wait(5);
        I.fillField(this.locators.zip, zipcode);
        I.wait(2);
        // I.click(this.locators.phone);
        // I.fillField(this.locators.phone, phone);
        //safari handling
        I.executeScript("document.querySelector('" + this.locators.phone + "').value = '" + phone + "';");
    },

    fillBillingAddress(fName, lName, address1, country, state, city, zipcode) {
        I.waitForVisible(this.locators.billing_fName, 10);
        I.scrollTo(this.locators.billing_fName);
        I.fillField(this.locators.billing_fName, fName);
        I.fillField(this.locators.billing_lName, lName);
        I.fillField(this.locators.billing_address1, address1);
        I.selectOption(this.locators.billing_country, country);
        I.waitForElement(this.locators.billing_state);
        I.selectOption(this.locators.billing_state, state);
        I.wait(3);
        I.fillField(this.locators.billing_city, city);
        I.fillField(this.locators.billing_zip, zipcode);
    },

    async completePayPalPayments(email, password) {

        var iframe_locator = "//iframe[@title='PayPal']";
        I.waitForElement(iframe_locator, 10);
        I.switchTo(iframe_locator);

        I.click(this.locators.payPalCheckoutBtn);
        I.wait(5);
        const windows = await I.grabAllWindowHandles();
        console.log('windows handle-- ' + windows);
        // pause();
        if (windows.length > 1) {
            // I.switchToNextTab();
            await I.switchToWindow(windows[0]);
            I.say(I.grabTitle());
            // I.waitForVisible(this.locators.payPalChangeEmailBtn, 5);
            // I.click(this.locators.payPalChangeEmailBtn);
            I.waitForVisible(this.locators.payPalEmail, 30);
            I.fillField(this.locators.payPalEmail, email);
            I.click(this.locators.paypalNextBtn);
            I.waitForVisible(this.locators.payPalPassword, 5);
            I.fillField(this.locators.payPalPassword, password);
            I.click(this.locators.payPalLoginBtn);
            I.wait(10);
            I.waitForClickable(this.locators.payPalContinue, 5);
            I.click(this.locators.payPalContinue);
            I.wait(5);
            await I.switchToWindow(windows[1]);
            I.closeOtherTabs();
            I.wait(10);
        }
    },

    async completePayPalPayIn4(email, password) {

        I.waitForElement(this.locators.payPalTab, 5);
        I.click(this.locators.payPalTab);
        I.selectOption(this.locators.payPalSelectAccount, "newaccount");

        var iframe_locator = "//iframe[@title='PayPal']";
        I.waitForElement(iframe_locator, 10);
        I.switchTo(iframe_locator);

        I.click(this.locators.payPalCheckoutBtn);
        I.wait(5);
        const windows = await I.grabAllWindowHandles();
        console.log('windows handle-- ' + windows.length);
        if (windows.length > 1) {
            I.switchToNextTab();
            // I.waitForVisible(this.locators.payPalChangeEmailBtn, 5);
            //I.click(this.locators.payPalChangeEmailBtn);
            I.waitForVisible(this.locators.payPalEmail, 5);
            I.fillField(this.locators.payPalEmail, email);
            I.click(this.locators.paypalNextBtn);
            I.waitForVisible(this.locators.payPalPassword, 5);
            I.fillField(this.locators.payPalPassword, password);
            I.click(this.locators.payPalLoginBtn);
            I.wait(10);
            I.click(this.locators.payPalPayIn4);
            I.waitForClickable(this.locators.payPalContinue, 5);
            I.click(this.locators.payPalContinue);
            I.switchTo(this.locators.payPalPayIn4Frame);
            I.click(this.locators.payPalPayIn4Continue);
            I.wait(5);
            I.click(this.locators.payPalPayIn4Continue);
            I.click(this.locators.payPalPayIn4CardSelect);
            I.click(this.locators.payPalPayIn4TermsCheckBox);
            I.click(this.locators.payPalPayIn4Continue);
            I.wait(5);
            I.switchTo();
            I.click(this.locators.payPalContinue);
            I.wait(5);
            await I.switchToWindow(windows[0]);
            I.closeOtherTabs();
        }
    },

    async completeKlarnaPayment(phone, otp) {

        I.waitForElement(this.locators.klarnaCheckoutBtn, 10);
        I.click(this.locators.klarnaCheckoutBtn);
        I.wait(5);
        const windows = await I.grabAllWindowHandles();
        I.say('windows handle-- ' + windows.length);
        if (windows.length > 1) {
            // // I.switchToNextTab();
            // pause();
            I.switchToWindow(windows[0]);
            I.waitForVisible(this.locators.klarna_PhoneInput, 30);
            // I.clearField(this.locators.klarna_PhoneInput);
            // I.click(this.locators.klarna_StartContinueBtn);
            // I.fillField(this.locators.klarna_PhoneInput, phone);
            I.click(this.locators.klarna_PhoneContinueBtn);
            I.waitForElement(this.locators.klarna_OtpInput, 20);
            I.fillField(this.locators.klarna_OtpInput, otp);
            // I.waitForElement(this.locators.klarna_PickPlanContinueBtn, 20);
            // I.click(this.locators.klarna_PickPlanContinueBtn);
            I.wait(5);
            I.waitForVisible(this.locators.klarn_selectPayNow, 30);
            I.click(this.locators.klarn_selectPayNow);
            I.click(this.locators.klarna_SelectPaymentContinueBtn);
            I.wait(2);
            I.waitForVisible(this.locators.klarna_ConfirmAndPay, 30);
            I.click(this.locators.klarna_ConfirmAndPay);
            // I.waitForElement(this.locators.klarna_skipFasterCheckout, 10);
            // I.click(this.locators.klarna_skipFasterCheckout);
            I.wait(10);
            await I.switchToWindow(windows[1]);
        }
    },

    verifySavedAdressInShippingDetails(fName, lName, address1, country, state, city, zipcode, phone) {
        // I.waitForVisible(this.locators.fName, 10);
        // I.scrollTo(this.locators.fName);
        // I.see(this.locators.fName, fName);
        // I.fillField(this.locators.lName, lName);
        // I.fillField(this.locators.address1, address1);
        // I.waitForElement(this.locators.country);
        // I.selectOption(this.locators.country, country);
        // I.waitForElement(this.locators.state);
        // I.selectOption(this.locators.state, state);
        // I.wait(3);
        // I.fillField(this.locators.city, city);
        // I.fillField(this.locators.phone, phone);
        // I.fillField(this.locators.zip, zipcode);
    },

    clickNextPayment() {
        I.click(this.locators.nextPayment);
        I.wait(5);
    },

    clickContinueBilling() {
        I.click(this.locators.continueBilling);
        I.wait(5);
        // Recaptcha check
        // I.scrollPageToTop();
        // I.wait(5);
    },

    clickContinueShipping() {
        I.click(this.locators.continueShipping);
        I.wait(5);
    },

    clickPayPalTab() {
        I.click(this.locators.payPalTab);
    },

    selectKlarnaPaymentOption() {
        I.click(this.locators.selectKlarna);
    },

    fillCVVForSavedCC(ccSecCode) {
        I.waitForElement(this.locators.cvvForSavedCard, 10);
        I.fillField(this.locators.cvvForSavedCard, ccSecCode);
    },

    //New Cybersource
    fillCCPaymentInfo(ccName, ccNum, expMonth, expYear, ccSecCode) {
        I.waitForVisible(this.locators.payName, 15);
        I.fillField(this.locators.payName, ccName);
        var iframe_locator = "//div[@id='cardNumber-container']//iframe";

        I.waitForElement(iframe_locator, 10);
        I.switchTo(iframe_locator);
        I.waitForVisible(this.locators.payCard, 15);
        //pause();
        // I.executeScript("document.querySelector('input[name=credit-card-number]').value = '4111 1111 1111 1111';");
        I.fillField(this.locators.payCard, "");
        //for Safari
        I.type(ccNum, 100);
        I.wait(2);
        I.switchTo();

        I.waitForElement(this.locators.payExpMonth, expMonth);
        I.selectOption(this.locators.payExpMonth, expMonth);
        I.waitForElement(this.locators.payExpYear, expYear);
        I.selectOption(this.locators.payExpYear, expYear);

        var iframe_cvv = "//div[@id='securityCode-container']/iframe";
        I.waitForElement(iframe_cvv, 10);
        I.switchTo(iframe_cvv);
        I.waitForElement(this.locators.paySecCode);
        I.fillField(this.locators.paySecCode, ccSecCode);
        I.switchTo();
    },

    //Old cybersource
    // fillCCPaymentInfo(ccName, ccNum, expMonth, expYear, ccSecCode) {
    //     I.waitForVisible(this.locators.payName, 15);
    //     I.fillField(this.locators.payName, ccName);
    //     var iframe_locator = "//div[@id='cardNumber-container']//iframe";

    //     I.waitForElement(iframe_locator, 10);
    //     I.switchTo(iframe_locator);
    //     I.waitForVisible("//input[@name='credit-card-number']", 15);
    //     //pause();
    //     // I.executeScript("document.querySelector('input[name=credit-card-number]').value = '4111 1111 1111 1111';");
    //     I.fillField("//input[@name='credit-card-number']", "");
    //     //for Safari
    //     I.type(ccNum, 100);
    //     I.wait(2);
    //     I.switchTo();

    //     I.waitForElement(this.locators.payExpMonth, expMonth);
    //     I.selectOption(this.locators.payExpMonth, expMonth);
    //     I.waitForElement(this.locators.payExpYear, expYear);
    //     I.selectOption(this.locators.payExpYear, expYear);


    //     I.waitForElement("//input[@id='securityCode']");
    //     I.fillField("//input[@id='securityCode']", ccSecCode);


    //     // I.waitForElement(this.locators.paySecCode);
    //     // I.fillField(this.locators.paySecCode, ccSecCode);
    //     // I.click(this.locators.saveCreditCardCheckbox);
    // },

    fillGiftCardInfo(cardNum, pin) {

        I.scrollPageToTop();
        I.wait(2);
        I.click(this.locators.giftCardCheckbox);
        I.waitForVisible(this.locators.giftCardNumer, 10);
        I.fillField(this.locators.giftCardNumer, cardNum);
        I.fillField(this.locators.giftPinNumber, pin);
        I.click(this.locators.giftCardRedeemBtn);
        I.wait(5);
    },

    removeAppliedGC() {
        I.waitForClickable(this.locators.giftCardRemove, 10);
        I.click(this.locators.giftCardRemove);
    },

    checkRemovedGC() {
        I.wait(5);
        I.dontSeeElement(this.locators.giftCardDetails);
    },

    clickNextPlaceOrder() {
        I.click(this.locators.nextplaceOrder);
        I.wait(5);
    },

    userIsOnCheckoutOrderSummeryPage() {
        I.seeElement(this.locators.checkout_orderSummary, 10);
    },

    verifyCheckoutInfo(fName, lName, add1, city, zip, phone, ccNum, ccExpDate, quantity,
        totalItemPrice, shipping, tax, estimatedTotal) {

        // verify shipping address is correct
        I.waitForVisible(this.locators.checkout_shippingSection, 10);
        I.scrollTo(this.locators.checkout_shippingSection);
        I.see(fName, this.locators.checkout_shippingSection);
        I.see(lName, this.locators.checkout_shippingSection);
        I.see(add1, this.locators.checkout_shippingSection);
        I.see(city, this.locators.checkout_shippingSection);
        I.see(zip, this.locators.checkout_shippingSection);
        I.see(phone, this.locators.checkout_shippingSection);

        // verify billing address is correct
        // I.scrollTo(this.locators.checkout_paymentSection);
        // I.see(fName, this.locators.checkout_paymentSection);
        // I.see(lName, this.locators.checkout_paymentSection);
        // I.see(add1, this.locators.checkout_paymentSection);
        // I.see(city, this.locators.checkout_paymentSection);
        // I.see(zip, this.locators.checkout_paymentSection);
        // I.see(phone, this.locators.checkout_paymentSection);

        // verify payment info is correct
        // Leave the last 4 digits shown; replace everything else with '*'
        // I.see(ccNum.replace(/\d(?=\d{4})/g, '*'), this.locators.checkout_paymentSection);
        //I.see(ccExpDate, this.locators.checkout_paymentSection);

        // verify product info is correct
        // I.scrollTo(this.locators.orderConf_quantity);
        // I.see(quantity, this.locators.orderConf_quantity);

        // I.see(totalItemPrice, this.locators.checkout_orderSummary);
        // I.see(shipping, this.locators.checkout_orderSummary);
        // I.see(tax, this.locators.checkout_orderSummary);
        // I.see(estimatedTotal, this.locators.checkout_orderSummary);
    },

    clickPlaceOrderFinal() {
        I.waitForClickable(this.locators.placeOrderFinal, 10);
        //I.refreshPage();
        I.click(this.locators.placeOrderFinal);
        //I.wait(5);
    },

    clickKlarnaPlaceOrderFinal() {
        I.waitForClickable(this.locators.klarna_placeOrder, 10);
        I.click(this.locators.klarna_placeOrder);
    },

    gotoHomePageFromCheckout() {
        I.click(this.locators.homeLink);
    },
    verifyShippingFields(fieldName) {
        switch (fieldName) {
            case 'first name':
                I.seeElement(this.locators.fName);
                break;
            case 'last name':
                I.seeElement(this.locators.lName);
                break;
            case 'address 1':
                I.seeElement(this.locators.address1);
                break;
            case 'city':
                I.seeElement(this.locators.city);
                break;
            case 'state':
                I.seeElement(this.locators.state);
                break;
            case 'zip code':
                I.seeElement(this.locators.zip);
                break;
            case 'phone':
                I.seeElement(this.locators.phone);
                break;
            case 'email':
                I.seeElement(this.locators.emailGuest);
                break;
        }
    },
    verifyShippingHeader() {
        I.seeElement(this.locators.shippingHdr)
    },
    checkBoxInShipping() {
        I.scrollTo(this.locators.signUpEmailChkBox);
        I.seeElement(this.locators.signUpEmailChkBox);
    },
    verifyShippingMethod() {
        I.see('Standard Ground', this.locators.shippingSelected);
        I.see('5-7 Business Days', this.locators.shippingTime);
        I.see('$0.00', this.locators.shippingCost);
    },
    verifyContinueButton() {
        I.seeElement(this.locators.continueShipping);
    },
    clickChooseShipLink() {
        I.click(this.locators.shipLink);
        I.wait(2);
    },
    seeShippingMethod(methodName, days, charges) {
        switch (methodName) {
            case 'Standard Ground':
                I.see(methodName, this.locators.shippingTypeName + "[1]/span");
                I.see(days, this.locators.shippingTypeName + "[1]/div");
                I.see(charges, this.locators.shipCost + "[1]");
                break;
            case 'Second Day':
                I.see(methodName, this.locators.shippingTypeName + "[2]/span");
                I.see(days, this.locators.shippingTypeName + "[2]/div");
                I.see(charges, this.locators.shipCost + "[2]");
                break;
            case 'Priority':
                I.see(methodName, this.locators.shippingTypeName + "[3]/span");
                I.see(days, this.locators.shippingTypeName + "[3]/div");
                I.see(charges, this.locators.shipCost + "[3]");
                break;
        }
    },
    selectShippingMethod(methodName) {
        switch (methodName) {
            case 'Second Day':
                I.click(this.locators.shippingTypeName + "[2]/span")
                break;
            case 'Priority':
                I.click(this.locators.shippingTypeName + "[3]/span")
                break;
        }
    },
    verifyShippingMethod(methodName, days, charges) {
        //comment the shipping days verification if it is keeps on changing
        switch (methodName) {
            case 'Standard Ground':
                I.see(methodName, this.locators.shippingTypeName + "[1]/span");
                I.see(days, this.locators.shippingTypeName + "[1]/div");
                I.see(charges, this.locators.shipCost + "[1]");
                break;
            case 'Second Day':
                I.see(methodName, this.locators.shippingTypeName + "[2]/span");
                I.see(days, this.locators.shippingTypeName + "[2]/div");
                I.see(charges, this.locators.shipCost + "[2]");
                break;
            case 'Priority':
                I.see(methodName, this.locators.shippingTypeName + "[3]/span");
                I.see(days, this.locators.shippingTypeName + "[3]/div");
                I.see(charges, this.locators.shipCost + "[3]");
                break;
        }

    },
    clickAddressLine1() {
        I.click(this.locators.address1);
    },
    enterAddressTerm(term) {
        I.fillField(this.locators.address1, term);
    },
    verifyAutosuggestion(addressLine) {
        I.see(addressLine, this.locators.addressSuggested1);
    }

};