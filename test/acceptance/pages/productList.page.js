const { I, productDetailsPage, homepage } = inject();

module.exports = {
    locators: {
        productList: "//div[contains(@class,'productTileTemplates')]/div",
        showFilter: ".filter",
        collectionFilter: "//div[@id='collection']",
        selectFirstCollectionFilter: "//div[@id='refinement-collection']/ul/li[1]/div",
        plpProductName: "//div[contains(@class,'productTileTemplates')]/div[1]",
        product: "(//div[contains(@class,'productTileTemplates')]/div)",
        compareCheckbox: "(//div[@class='custom-checkbox'])",
        alternatingHover: "(//div[@class='product-tile-footer'])",
        pdpLink: "(//div[@class='pdp-link'])",
        compareBar: "(//div[contains(@class,'compare-bar')])[1]",
        productTitleCompareBar: "(//p[@class='product-title'])",
        disabledCompareCTA: "//button[@disabled='disabled' and contains(@class,'compare button')]",
        activeCompareCTA: "//button[contains(@class,'compare button active')]",
        compareSelection: "//h1[@class='page-title compare-sections']",
        removeItem: "//a[@class='close']",
        clearAll: "//a[@class='clear-all']",
        //------------------------------Compare Page------------------------------------------//
        comparedSuggestion: "//div[@class='compared-suggestions']",
        comparedSuggestionNotes: "//p[@class='notes']",
        productCompareUrl: "(//div[@class='product-compare-url'])[1]",
        suggestedItemNo_1: "(//div[@class='suggesstion-items'])[1]",
        suggestedItemNameNo_1: "(//div[@class='suggesstion-items'])[1]/div[3]/div/div[2]",
        basicSpecHdr: "//div[@class='content basicspecifications']",
        additionServiceHdr: "//div[@class='content additionalservices']",
        alternateViewsHdr: "//div[@class='content alternateviews']",
        featuresHdr: "//div[@class='content features']",
        removeItemFromCompare: "(//div[@class='remove-item'])",
        addToCartFromCompare: "(//div[@class='cart-and-ipay'])",
        backContinueShopping: "//div[@class='back-link']",
        cartFlyoutHeading: "(//h2[@class='cart-flyout-heading'])[1]",
        productTileOnCartFlyout: "(//span[@class='product-title'])[1]",
        //--------------------------------Facet---------------------------------------------------//
        headerPLP: "//h1[@class='category-title']",
        filterColor: "//button[@aria-controls='refinement-color_group']",
        filterCollection: "//button[@aria-controls='refinement-collection']",
        filterMaterial: "//button[@aria-controls='refinement-material']",
        filterGender: "//button[@aria-controls='refinement-genders']",
        menGenderFacet: "//div[contains(@class, 'Men')]",
        blackColorFacet: "//div[contains(@class, 'Black')]",
        fabricMaterialFacet: "//div[contains(@class, 'Fabric')]",
        alphaNonFacet: "//input[@name='Alpha']",
        footer: "//footer"

    },
    dataSets: {
        product3_onCompareSuggested: "",
        productAddedToCart_fromCompare: "",
        product_1: "",
        product_2: ""
    },
    userOnProductListPage() {
        I.seeElement(this.locators.productList);
    },

    openProductDetails(productName) {
        let locator = "//a[text()='" + productName + "']";
        I.seeElement(locator);
        // I.click(locator);
    },

    async applyCollectionFilter() {
        // I.click(this.locators.showFilter);
        I.waitForVisible(this.locators.collectionFilter, 15);
        I.click(this.locators.collectionFilter);
        // I.click(this.locators.selectFirstCollectionFilter);
        I.checkOption(this.locators.selectFirstCollectionFilter);
        I.wait(5);
        let collectionName = await I.grabAttributeFrom(this.locators.selectFirstCollectionFilter + "/input", 'name');
        // I.see(collectionName, this.locators.productList + "[1]");
        let productCollection = await I.grabTextFrom("(//div[@class='product-tile-title'])[1]");
        I.assertEqualIgnoreCase(productCollection.toLowerCase().trim(), collectionName.toLowerCase().trim());
    },

    wishlistProduct() {
        I.moveCursorTo(this.locators.plpProductName);
    },

    async verifyCompareCheckbox(product) {
        switch (product) {
            case "1st Product":
                I.waitForVisible(this.locators.product + "[1]");
                this.dataSets.product_1 = await I.grabTextFrom(this.locators.pdpLink + "[1]/a");
                // I.moveCursorTo(this.locators.product + "[1]");
                I.scrollIntoView(this.locators.pdpLink + "[1]", { behavior: "smooth", block: "center", inline: "center" });
                I.click(this.locators.alternatingHover + "[1]");
                I.see("Compare", this.locators.compareCheckbox + "[1]");
                break;
            case "2nd Product":
                I.waitForVisible(this.locators.product + "[2]");
                this.dataSets.product_2 = await I.grabTextFrom(this.locators.pdpLink + "[2]/a");
                // I.moveCursorTo(this.locators.product + "[2]");
                I.scrollIntoView(this.locators.pdpLink + "[2]", { behavior: "smooth", block: "center", inline: "center" });
                I.click(this.locators.alternatingHover + "[2]");
                I.see("Compare", this.locators.compareCheckbox + "[2]");
                break;
            case "3rd Product":
                I.waitForVisible(this.locators.product + "[3]");
                // I.moveCursorTo(this.locators.product + "[3]");
                I.scrollIntoView(this.locators.pdpLink + "[3]", { behavior: "smooth", block: "center", inline: "center" });
                I.click(this.locators.alternatingHover + "[3]");
                I.see("Compare", this.locators.compareCheckbox + "[3]");
                break;

            default:
            //code block

        }
    },
    clickCompare(product) {
        switch (product) {
            case "1st Product":
                I.click(this.locators.compareCheckbox + "[1]");
                break;
            case "2nd Product":
                I.click(this.locators.compareCheckbox + "[2]");
                break;
            case "3rd Product":
                I.click(this.locators.compareCheckbox + "[3]");
                break;

            default:
            //code block

        }
    },
    async verifyCompareBar(product) {
        switch (product) {
            case "1st Product":
                let product_1OnPDP = await I.grabTextFrom(this.locators.pdpLink + "[1]/a");
                let product_1OnComparebar = await I.grabTextFrom(this.locators.productTitleCompareBar + "[1]");
                I.say("PDP " + product_1OnPDP);
                I.say("BAR " + product_1OnComparebar);
                I.assertEqual(product_1OnPDP.trim(), product_1OnComparebar.trim());
                break;
            case "2nd Product":
                let product_2OnPDP = await I.grabTextFrom(this.locators.pdpLink + "[2]/a");
                let product_2OnComparebar = await I.grabTextFrom(this.locators.productTitleCompareBar + "[2]");
                I.assertEqual(product_2OnPDP.trim(), product_2OnComparebar.trim());
                break;
            case "3rd Product":
                let product_3OnPDP = await I.grabTextFrom(this.locators.pdpLink + "[3]/a");
                let product_3OnComparebar = await I.grabTextFrom(this.locators.productTitleCompareBar + "[3]");
                I.assertEqual(product_3OnPDP.trim(), product_3OnComparebar.trim());

                break;
            default:
            //code block

        }
    },
    async compareDisable() {
        // let compareState = await I.grabAttributeFromAll(this.locators.compareCTA, 'disabled');
        // console.log(compareState); -> This is sreturning true instead of attribute value
        // I.assertContain(compareState, "disabled");
        I.seeElement(this.locators.disabledCompareCTA);
    },
    compareEnable() {
        I.seeElement(this.locators.activeCompareCTA);
        I.click(this.locators.activeCompareCTA);
    },
    navigateToComparePage() {
        I.waitForElement(this.locators.compareSelection, 10);
        I.see("Compare Selections", this.locators.compareSelection);
    },
    verifyRemoveItemPresence() {
        I.see("Remove Item", this.locators.removeItem);
    },
    clickRemoveItem() {
        I.click(this.locators.removeItem);
    },
    verifyRemoveItemFunction() {
        I.dontSeeElement(this.locators.productTitleCompareBar);
    },
    verifyClearAllPresence() {
        I.see("Clear All", this.locators.clearAll);
    },
    clickClearAll() {
        I.click(this.locators.clearAll);
    },
    verifyClearAllFunction() {
        I.dontSeeElement(this.locators.productTitleCompareBar);
    },
    //--------------------------Comapre Selection Page----------------------------------------------//
    verifyProductOnCompare(product) {
        switch (product) {
            case "product 1":
                I.waitForVisible(this.locators.pdpLink + "[1]/a");
                I.see(this.dataSets.product_2.trim(), this.locators.pdpLink + "[1]/a");
                break;
            case "product 2":
                I.see(this.dataSets.product_1.trim(), this.locators.pdpLink + "[2]/a");
                break;
            default:
            //code block
        }

    },
    verifyContentsOnCompare(content) {
        switch (content) {
            case "Basic Specifications":
                I.see("Basic Specifications", this.locators.basicSpecHdr);
                break;
            case "Additional services":
                I.see("Additional Services", this.locators.additionServiceHdr);
                break;
            case "Alternate Views":
                I.see("Alternate Views", this.locators.alternateViewsHdr);
                break;
            case "Feature":
                I.see("Features", this.locators.featuresHdr);
                break;
            default:
            //code block
        }
    },
    verifyComparedSuggestion() {
        I.seeElement(this.locators.comparedSuggestion);
    },
    notesComparedSuggestion() {
        I.see("Other customers compared with these items, here are some suggestions:", this.locators.comparedSuggestionNotes);
    },
    async addFromComparedSuggestion() {
        this.dataSets.product3_onCompareSuggested = await I.grabTextFrom(this.locators.suggestedItemNameNo_1);
        I.click(this.locators.productCompareUrl);
    },
    async verifyProductAdded() {
        let productAddedToCompareSelection = await I.grabTextFrom(this.locators.pdpLink + "[1]/a");
        I.assertContain(this.dataSets.product3_onCompareSuggested, productAddedToCompareSelection);
    },
    removeAddedProduct() {
        I.click(this.locators.removeItemFromCompare + "[1]");
        I.dontSee(this.dataSets.product3_onCompareSuggested, this.locators.pdpLink + "[1]");
    },
    verifybackButton() {
        I.see("Continue Shopping", this.locators.backContinueShopping);
    },
    clickContinueShopping() {
        I.click(this.locators.backContinueShopping);
    },
    async clickAddToCart() {
        this.dataSets.productAddedToCart_fromCompare = await I.grabTextFrom(this.locators.pdpLink + "[1]");
        I.click(this.locators.addToCartFromCompare);
    },
    verifyCartFlyout() {
        I.waitForVisible(this.locators.cartFlyoutHeading);
        I.seeElement(this.locators.cartFlyoutHeading);
    },
    async productAdded() {
        let productOnCartFlyout = await I.grabTextFrom(this.locators.productTileOnCartFlyout);
        I.assertContain(this.dataSets.product_2, productOnCartFlyout);
    },
    async clickfacetedFilter(facet) {
        switch (facet) {
            case "color":
                I.click(this.locators.filterColor);
                break;
            case "material":
                I.click(this.locators.filterMaterial);
                break;
            case "gender":
                I.click(this.locators.filterGender);
            default:
            //code block
        }
    },
    async clickfilterOption(facet, facetValue) {
        switch (facet) {
            case "color":
                I.waitForVisible(this.locators.blackColorFacet, 10);
                I.checkOption("//div[contains(@class, '" + facetValue + "')]");
                I.wait(5);
                break;
            case "material":
                I.waitForVisible(this.locators.fabricMaterialFacet, 10);
                I.checkOption("//div[contains(@class, '" + facetValue + "')]");
                I.wait(5);
                break;
            case "gender":
                I.waitForVisible(this.locators.menGenderFacet, 10);
                I.checkOption("//div[contains(@class, '" + facetValue + "')]");
                I.wait(5);
            default:
            //code block
        }
    },
    verifyfacetedPageHeader(facetValue) {
        I.see(facetValue, this.locators.headerPLP);
    },
    verifyfacetedPageUrl(facetValue) {
        I.seeInCurrentUrl(String(facetValue).toLowerCase());
    },
    clickNonfacetedFilter() {
        I.click(this.locators.collectionFilter);
        I.click(this.locators.selectFirstCollectionFilter);
        I.wait(5);
    },
    verifyPageTitle(facetValue) {
        I.dontSee(facetValue, this.locators.headerPLP);
    },
    verifyPageUrl(facetValue) {
        I.dontSeeInCurrentUrl(String(facetValue).toLowerCase());
    }
}