const { data } = inject();
const I = actor();

module.exports = {
    locators: {
        consentTrackModal: '.modal-content',
        consentTrackAffirm: '.affirm.btn.btn-primary'
    },
    accept() {
        I.waitForElement(this.locators.consentTrackModal);
        within(this.locators.consentTrackModal, () => {
            I.click(this.locators.consentTrackAffirm);
        });
    },

    closeFreeShippingPopup() {
        const iframe_locator = '#attentive_creative';
        tryTo(() => I.waitForVisible(iframe_locator, 20));
        // tryTo(() => I.waitForVisible("#closeIconContainer", 30));
        I.pressKey("Escape");
        // try {
        //     I.waitForVisible("//a[contains(@id,'bx-close-inside')]", 30);
        //     I.click("//a[contains(@id,'bx-close-inside')]");
        // } catch (error) {
        //     I.say("Popup not found");
        // }
    },

    closeCountrySelectionModal() {
        const closeButton = "//div[@id='geolocationcheckmodal']//button[@class='close']";
        tryTo(() => I.waitForVisible(closeButton, 5));
        tryTo(() => I.forceClick(closeButton));
        I.pressKey("Escape");
        I.wait(1);
    },


    clickToLoadPage(elementSelector, expectedPageUrl) {
        I.click(elementSelector);
        I.wait(1);
        I.seeCurrentUrlEquals(expectedPageUrl);
    },

    navigateToHomepage() {
        I.amOnPage(data.pageURLs.homepage);
    },

    isElementVisible(locator) {
        return new Promise((resolve, reject) => {
            flag = true;
            try {
                I.seeElement(locator);
            } catch (error) {
                flag = false;
                console.log('Skipping operation as element is not visible');
            }
            return resolve(flag);
        })
    },

    randomEmailGenerator() {
        return new Promise((resolve, reject) => {
            var chars = 'abcdefghijklmnopqrstuvwxyz1234567890';
            var string = '';
            for (var ii = 0; ii < 10; ii++) {
                string += chars[Math.floor(Math.random() * chars.length)];
            }
            return resolve(string + '@xyz.com');
        })
    },

    randomCARDGenerator() {
        return new Promise((resolve, reject) => {
            var chars = '1234567890';
            var string = '4444 1111 1234 ';
            for (var ii = 0; ii < 4; ii++) {
                string += chars[Math.floor(Math.random() * chars.length)];
            }
            return resolve(string);
        })
    },

    randomText() {
        return new Promise((resolve, reject) => {
            var chars = 'abcdefghijklmnopqrstuvwxyz1234567890';
            var string = '';
            for (var ii = 0; ii < 10; ii++) {
                string += chars[Math.floor(Math.random() * chars.length)];
            }
            return resolve(string + ' xyz');
        })
    },

    async setGeoLocation(longitude, latitude) {
        const { WebDriver } = this.helpers;
        const { browserContext } = WebDriver;
        await browserContext.setGeolocation({ longitude, latitude });
        await WebDriver.refreshPage();
    },


    async clickIfVisible(locator) {
        try {
            const numVisible = await I.grabNumberOfVisibleElements(locator);
            if (numVisible) {
                return I.click(locator);
            }
            I.pressKey('Escape');
        } catch (err) {
            I.say('Skipping operation as element is not visible');
        }
    },

    // async clickIfVisible(locator) {
    //     const helper = this.helpers['WebDriver'];
    //     try {
    //         const numVisible = await helper.grabNumberOfVisibleElements(locator);
    //         if (numVisible) {
    //             return helper.click(locator);
    //         }
    //     } catch (err) {
    //         console.log('Skipping operation as element is not visible');
    //     }
    // },

    async mouseHoverInSafari(elementSelector) {

        await I.executeScript((selector) => {
            const element = document.querySelector(selector);
            if (element) {
                // Safari-specific mouse hover workaround
                const mouseHoverEvent = new MouseEvent('mouseover', {
                    view: window,
                    bubbles: true,
                    cancelable: true,
                });
                element.dispatchEvent(mouseHoverEvent);
            }
        }, elementSelector);
    }
};

