// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#292b2c';

var regex = /(\w+)(\.\w+)+(?!.*(\w+)(\.\w+)+)/;
var labels = [];
var chartData = { labels: labels, datasets: [] };
var rawData = JSON.parse(window.data);
const CHART_COLORS = [
  { color: 'rgb(255, 99, 132)', 'opacity': 'rgb(255, 99, 132, 0)' },
  { color: 'rgb(75, 192, 192)', 'opacity': 'rgb(75, 192, 192, 0)' },
  { color: 'rgb(255, 205, 86)', 'opacity': 'rgb(255, 205, 86, 0)' },
  { color: 'rgb(54, 162, 235)', 'opacity': 'rgb(54, 162, 235, 0)' }
]

rawData.forEach((entry, index) => {
  entry.forEach((page) => {
    Object.keys(page).forEach((key) => {
      var dataset = {
        label: key,
        data: page[key].map((entry) => { return parseFloat(entry.percentUnused); }),
        borderColor: CHART_COLORS[index].color,
        backgroundColor: CHART_COLORS[index].opacity,
        fill: true
      };
      chartData.datasets.push(dataset);
      page[key].forEach((entry) => {
        var fileBreakDown = entry.url.match(regex);
        if (fileBreakDown && fileBreakDown.length > 0 && labels.indexOf(fileBreakDown[0]) < 0) labels.push(fileBreakDown[0]);
        else console.log(entry.url);
        dataset.data.push()
      });

    });
  });
});


const config = {
  type: 'line',
  data: chartData,
  options: {
    scales: {
      y: {
        stacked: true
      }
    },
    plugins: {
      filler: {
        propagate: false
      },
      'samples-filler-analyser': {
        target: 'chart-analyser'
      }
    },
    interaction: {
      intersect: true,
    },
  },
};

// Area Chart Example
var ctx = document.getElementById("myAreaChart");
var myLineChart = new Chart(ctx, config);

