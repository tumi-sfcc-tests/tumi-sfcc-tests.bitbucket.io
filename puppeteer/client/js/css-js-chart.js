// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#292b2c';

var regex = /(\w+)(\.\w+)+(?!.*(\w+)(\.\w+)+)/;
var labels = [];
var chartData = { labels: labels, datasets: [] };
var rawData = JSON.parse(window.data).charts;
const CHART_COLORS = [
    { color: 'rgb(255, 99, 132)', 'opacity': 'rgb(255, 99, 132, 0)' },
    { color: 'rgb(75, 192, 192)', 'opacity': 'rgb(75, 192, 192, 0)' },
    { color: 'rgb(255, 205, 86)', 'opacity': 'rgb(255, 205, 86, 0)' },
    { color: 'rgb(54, 162, 235)', 'opacity': 'rgb(54, 162, 235, 0)' }
];

const config = {
    type: 'line',
    data: chartData,
    options: {
        responsive: true,
        plugins: {
            legend: {
                position: 'top',
            },
            title: {
                display: true,
                text: 'Chart.js Bubble Chart'
            }
        },
    },
};

rawData.forEach((entry, index) => {
    labels = [];
    chartData = { labels: labels, datasets: [] };
    entry.data.forEach((page, datasetIndex) => {
        Object.keys(page[0]).forEach((key) => {
            var dataset = {
                label: key,
                data: page[0][key].filter((entry, entryIndex) => {
                    var fileBreakDown = entry.url.match(regex);
                    if (fileBreakDown && fileBreakDown.length > 0 && labels.indexOf(fileBreakDown[0]) < 0) {
                        labels.push(fileBreakDown[0]);
                        return entry;
                    }
                }).map((entry) => {
                    return parseFloat(entry.percentUnused);
                }),
                borderColor: CHART_COLORS[datasetIndex].color,
                backgroundColor: CHART_COLORS[datasetIndex].opacity,
                fill: true
            };
            chartData.datasets.push(dataset);

        });
    });
    // Area Chart Example
    config.data = chartData;
    var ctx = document.getElementById(entry.id);
    var myLineChart = new Chart(ctx, config);
});





