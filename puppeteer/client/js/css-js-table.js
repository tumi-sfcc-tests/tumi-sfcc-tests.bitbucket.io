window.addEventListener('DOMContentLoaded', event => {
    // Simple-DataTables
    // https://github.com/fiduswriter/Simple-DataTables/wiki
    $(document).ready(() => {
        $('table[id*="datatablesSimple-"]')
        const datatablesSimple = document.getElementsByClassName('datatablesSimple');
        if (datatablesSimple) {
            let options = {
                searchable: true,
                perPage: 50,
                perPageSelect: [50, ["All", 0]],
                footer: true
            };
            $('table[id*="datatablesSimple-"]').each((_, table) => {
                new simpleDatatables.DataTable(table, options);
            });

        }
    });

});
