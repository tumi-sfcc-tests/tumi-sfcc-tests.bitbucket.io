const puppeteer = require('puppeteer');
const params = require("minimist")(process.argv.slice(2));
const fs = require("fs");
const { log } = require('console');
const projectname = "Tumi";
const queryString = params.q;
const baseURL = `https://${params.e}.tumi.com`;
const pdpURL = `${baseURL}/p/${params.p}?${queryString}`;
const clpURL = `${baseURL}/c/${params.c}?${queryString}`;
const searchURL = `${baseURL}/search?${queryString}&q=${params.s}`;
const urlToTest = [baseURL + `?${queryString}`, pdpURL, clpURL, searchURL];


var coverageObjects = new Array();
//Scroll to end of the page 
const autoScroll = async (page) => {
    await page.evaluate(async () => {
        await new Promise((resolve, reject) => {
            var totalHeight = 0;
            var distance = 100;
            var timer = setInterval(() => {
                var scrollHeight = document.body.scrollHeight;
                window.scrollBy(0, distance);
                totalHeight += distance;

                if (totalHeight >= scrollHeight) {
                    clearInterval(timer);
                    resolve();
                }
            }, 500);
        });
    });
}

const run = async (url) => {
    coverageObjects[url] = [];
    const browser = await puppeteer.launch({
        args: ['--enable-gpu', '--no-sandbox', '--disable-setuid-sandbox', '--user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36 TumiCustomUA']
    });
    const page = await browser.newPage()
    await page.setCacheEnabled(false);
    await page.setViewport({
        width: 1200,
        height: 800
    });

    await Promise.all([
        page.coverage.startJSCoverage(),
        page.coverage.startCSSCoverage()
    ]);
    // Navigate to page
    await page.goto(url, { waitUntil: 'networkidle2', timeout: 60000 });
    await autoScroll(page);

    // Disable both JavaScript and CSS coverage
    const [jsCoverage, cssCoverage] = await Promise.all([
        page.coverage.stopJSCoverage(),
        page.coverage.stopCSSCoverage(),
    ]);

    // Calculates # bytes being used based on the coverage
    const calculateUsedBytes = (type, coverage) =>
        coverage.filter(({ url }) => {
            return (url.indexOf('.js') > 0 || url.indexOf('.css') > 0)
        });
    var jsData = calculateUsedBytes('js', jsCoverage).map(({ url, ranges, text }) => {
        var type = 'JS';
        return { url, ranges, text, type }
    });
    var cssData = calculateUsedBytes('css', cssCoverage).map(({ url, ranges, text }) => {
        var type = 'CSS';
        return { url, ranges, text, type }
    });
    coverageObjects[url] = [...jsData, ...cssData].map(({ url, ranges, text, type }) => {
        let usedBytes = 0;
        ranges.forEach((range) => (usedBytes += range.end - range.start - 1));
        return {
            url,
            type,
            usedBytes,
            totalBytes: text.length,
            percentUnused: (100 - (usedBytes / text.length * 100)).toFixed(2)
        }
    });
    await browser.close();
}

//Start
const start = async () => {
    console.info('Starting JS/CSS use evalution');

    for (let i = 0; i < urlToTest.length; i++) {
        if (params.r !== 'pseudo')
            await run(urlToTest[i])
    }

    require('./src/generateHtmlReport').generateHTML(coverageObjects);

    try {
        if (Boolean(params.t))
            urlToTest.forEach((url) => {
                require('./src/tracingReport').getTracingReport(url);
            });
    } catch (error) {
        log.info(errro);
    }
}
start();