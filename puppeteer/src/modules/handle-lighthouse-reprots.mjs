'use strict';

import * as fs from 'fs';
import * as ejs from 'ejs';

const regenrateIndexPage = async () => {
    var currDate = new Date();
    var folderPath = `${process.cwd()}/puppeteer/reports/lighthouse/${currDate.toLocaleDateString().replace(/\//g, '-')}`;
    await fs.promises.mkdir(folderPath, { recursive: true });
    await fs.cpSync(`${process.cwd()}/.lighthouseci`, folderPath, { recursive: true });
    const linksTemplate = `${process.cwd()}/puppeteer/templates/nav-links.ejs`;
    var cssJsReportFolder = `${process.cwd()}/puppeteer/reports/${currDate.toLocaleDateString().replace(/\//g, '-')}/index.html`;
    var html = await fs.readFileSync(cssJsReportFolder, 'utf8').toString();
    var lightghouseFiles = await fs.readdirSync(`${process.cwd()}/.lighthouseci`).filter((file) => {
        return file.indexOf('.html') > -1
    }).map((item, index) => {
        return {
            id: index + 1,
            url: `../../reports/lighthouse/${currDate.toLocaleDateString().replace(/\//g, '-')}/${item}`
        }
    });
    var auditReportTemplate = ejs.compile(fs.readFileSync(linksTemplate, 'utf8'));
    var links = auditReportTemplate({ "model": lightghouseFiles });
    html = html.replace('<!-- LIGHTHOUSR_REPORTS -->', links);
    await fs.writeFileSync(cssJsReportFolder, html, 'utf8');

};

export default regenrateIndexPage;
regenrateIndexPage();

