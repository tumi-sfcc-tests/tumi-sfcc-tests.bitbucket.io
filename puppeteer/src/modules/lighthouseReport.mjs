'use strict';

import { launch as chromeLauncher } from "chrome-launcher";
import puppeteer from 'puppeteer';
import lighthouse from 'lighthouse';
import fetch from 'node-fetch';
import * as fs from 'fs';
import * as reportGenerator from "lighthouse";
import { default as minimist } from "minimist";

const params = minimist(process.argv.slice(2));

const queryString = params.q;
const baseURL = `https://${params.e}.tumi.com`;
const pdpURL = `${baseURL}/p/${params.p}?${queryString}`;
const clpURL = `${baseURL}/c/${params.c}?${queryString}`;
const searchURL = `${baseURL}/search?${queryString}&q=${params.s}`;
var urlToTest = [baseURL + `?${queryString}`, pdpURL, clpURL, searchURL];
var currDate = new Date();
const _fileName = (_url) => {
    var uPart = _url.split("/").slice(3)[0];
    if (uPart === 'p')
        return 'Product';
    else if (uPart === 'c')
        return 'Collection';
    else if (uPart !== undefined && uPart.indexOf('search') > -1)
        return 'Search';
    else
        return 'Home';
};

var folderPath = `${process.cwd()}/puppeteer/reports/lighthouse/${currDate.toLocaleDateString().replace(/\//g, '-')}`;
// `${process.mainModule.path}/reports/lighthouse/${currDate.toLocaleDateString().replace(/\//g, '-')}`;
await fs.promises.mkdir(folderPath, { recursive: true });

const getLighthouseReport = async (url) => {
    // const url = _url || 'https://chromestatus.com/features';
    const options = {
        logLevel: 'info',
        disableDeviceEmulation: true,
        chromeFlags: ['--disable-mobile-emulation']
    };
    // Launch chrome using chrome-launcher.
    const chrome = await chromeLauncher();
    options.port = chrome.port;
    // Connect to it using puppeteer.connect().

    const resp = await fetch(`http://127.0.0.1:${chrome.port}/json/version`);
    const { webSocketDebuggerUrl } = await resp.json();
    const browser = await puppeteer.connect({ browserWSEndpoint: webSocketDebuggerUrl });
    const page = await browser.newPage();

    var fileName = _fileName(url);
    const { lhr, report } = await lighthouse(url, undefined, undefined, page);
    const html = reportGenerator.generateReport(lhr, 'html');
    await fs.writeFile(`${folderPath}/${fileName}.html`, html, function (err) {
        if (err) throw err;
    });

    console.log(`Lighthouse scores: ${Object.values(lhr.categories).map(c => c.score).join(', ')}`);
    // Run Lighthouse.
    await browser.disconnect();
    await chrome.kill();
    urlToTest = urlToTest.slice(1);
    if (urlToTest.length > 0) getLighthouseReport(urlToTest[0]);
};

const run = async () => {
    getLighthouseReport(urlToTest[0]);
};
run();
export default run;
