'use strict';

module.exports = {
    getTracingReport: async (url) => {
        const puppeteer = require('puppeteer');
        var ejs = require('ejs'),   // library for turning .ejs templates into .html files
            fs = require('fs');
        var currDate = new Date();
        var fileName = ((_url) => {
            var uPart = url.split("/").slice(3)[0];
            switch (uPart) {
                case 'p':
                    return 'Product';
                case 'c':
                    return 'Collection';
                case 'search':
                    return 'Search';
                default:
                    return 'Home';
            }

        })(url);
        var folderPath = `${process.mainModule.path}/reports/tracing/${currDate.toLocaleDateString().replace(/\//g, '-')}`;
        await fs.promises.mkdir(folderPath, { recursive: true });

        (async () => {
            const browser = await puppeteer.launch();
            const page = await browser.newPage();
            // Drag and drop this JSON file to the DevTools Performance panel!
            await page.tracing.start({ path: `${folderPath}/${fileName}.JSON`, screenshots: true });
            await page.goto(url);
            await page.tracing.stop();

            // Extract data from the trace
            try {
                const tracing = JSON.parse(fs.readFileSync(`${folderPath}/${fileName}.JSON`, 'utf8'));
                const traceScreenshots = tracing.traceEvents.filter(x => (
                    x.cat === 'disabled-by-default-devtools.screenshot' &&
                    x.name === 'Screenshot' &&
                    typeof x.args !== 'undefined' &&
                    typeof x.args.snapshot !== 'undefined'
                ));
                traceScreenshots.forEach(function (snap, index) {
                    fs.writeFile(`${folderPath}/trace-screenshot-${index}.png`, snap.args.snapshot, 'base64', function (err) {
                        if (err) {
                            console.log('writeFile error', err);
                        }
                    });
                });
            }
            catch (error) {
                console.info('Error in tracing.traceScreenshots', error);
            }
            await browser.close();
        })();
    }
}