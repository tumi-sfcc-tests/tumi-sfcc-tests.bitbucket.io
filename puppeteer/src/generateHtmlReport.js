'use strict';

const _coverageObject = require('../client/assets/data');

module.exports = {
    generateHTML: async (coverageObjects) => {
        var ejs = require('ejs'),   // library for turning .ejs templates into .html files
            fs = require('fs');    // node.js library for reading and writing file

        const params = require("minimist")(process.argv.slice(2));
        coverageObjects = params.r !== 'pseudo' ? coverageObjects : _coverageObjects;
        var currDate = new Date();
        if (params.r !== 'pseudo')
            await fs.promises.mkdir(`${process.mainModule.path}/reports/${currDate.toLocaleDateString().replace(/\//g, '-')}`, { recursive: true });
        else
            await fs.promises.mkdir(`${process.mainModule.path}/reports/dummyReports/${currDate.toLocaleDateString().replace(/\//g, '-')}`, { recursive: true });

        const reportTemplate = '/templates/auditReportTemplate-new.ejs';
        const layoutTemplate = '/templates/_layout.ejs';
        // make sure EJS is configured to use curly braces for templates
        ejs.open = '{{';
        ejs.close = '}}';

        var auditReportTemplate = ejs.compile(fs.readFileSync(process.mainModule.path + layoutTemplate, 'utf8'));
        // var indexTemplate = ejs.compile(fs.readFileSync(process.mainModule.path + '/templates/index.ejs', 'utf8'));
        var modelItirator = new Array();
        Object.keys(coverageObjects).forEach((key, index) => {
            var model = new Array();
            var obj = new Object();
            obj[['Home', 'PDP', 'CLP', 'Search'][index]] = coverageObjects[key];
            model.push(obj)

            modelItirator.push(model);
        });


        // var html = indexTemplate({ "model": modelItirator, "coverageObjects": coverageObjects });
        // fs.writeFileSync(`${process.mainModule.path}/reports/${params.r === 'pseudo' ? '/dummyReports' : currDate.toLocaleDateString().replace(/\//g, '-')}/index.html`, html, 'utf8');
        var tables = new Array();

        if (!Boolean(params.g)) {
            Object.keys(coverageObjects).forEach((key, index) => {
                var model = {};
                var reportName = `${params.r === 'pseudo' ? '/dummyReports' : currDate.toLocaleDateString().replace(/\//g, '-')}/auditRport-${['Home', 'PDP', 'CLP', 'Search'][index]}-${currDate.toLocaleDateString().replace(/\//g, '-')}`;
                model[key] = coverageObjects[key];
                model[key].id = reportName;
                model[key].data = coverageObjects[key];
                tables.push(model);

                var html = auditReportTemplate({ "model": { tables: tables } });
                fs.writeFileSync(`${process.mainModule.path}/reports/${params.r === 'pseudo' ? '/dummyReports' : currDate.toLocaleDateString().replace(/\//g, '-')}/auditRport-${['Home', 'PDP', 'CLP', 'Search'][index]}-${currDate.toLocaleDateString().replace(/\//g, '-')}.html`, html, 'utf8');
            });
        } else {
            var html = auditReportTemplate({ model: { tables: tables } });
            fs.writeFileSync(`${process.mainModule.path}/reports/${params.r === 'pseudo' ? '/dummyReports' : currDate.toLocaleDateString().replace(/\//g, '-')}/auditRport-${currDate.toLocaleDateString().replace(/\//g, '-')}.html`, html, 'utf8');
        }

        var layoutModel = {
            charts: [{ data: modelItirator, title: 'JS/CSS Unused Data', id: 'js-css-data' }],
            tables: tables
        };

        var indexTemplate = ejs.compile(fs.readFileSync(process.mainModule.path + '/templates/_layout.ejs', 'utf8'));
        var html = indexTemplate({ "model": layoutModel });
        fs.writeFileSync(`${process.mainModule.path}/reports/${params.r === 'pseudo' ? '/dummyReports' : currDate.toLocaleDateString().replace(/\//g, '-')}/index.html`, html, 'utf8');


    }
};