const commonConf = require('./commonConf.js');

var ALLURE_REPORT_PATH = commonConf.allureReportDirSpecialMarket();

exports.config = {
  output: './test/specialMarket/failed-screenshots',
  helpers: {
    WebDriver: {
      url: 'https://stage-specialmarkets.tumi.com/?yottaa-internal-passthru',

      browser: 'chrome',
      // browser: 'safari',
      desiredCapabilities: commonConf.caps_macOSX_latest,

      smartWait: 5000,
      waitForTimeout: 30000,
      windowSize: 'maximize',
    },
    ChaiWrapper: {
      require: "codeceptjs-chai"
    },
    AssertWrapper: {
      "require": "codeceptjs-assert"
    },
    WebDriverHelper: {
      require: './test/specialMarket/customHelperSpecialMarket.js'
    }
  },
  include: commonConf.include,
  mocha: {},
  bootstrap: null,
  timeout: null,
  teardown: null,
  hooks: [],
  gherkin: {
    features: './test/specialMarket/features/*.feature',
    steps: commonConf.gherkin_steps
  },
  plugins: {
    wdio: {
      enabled: true,
      // services: ['selenium-standalone'],
      services: ['browserstack'],
      user: process.env.BROWSERSTACK_USERNAME || 'taushifahamad_QhjVH3',
      key: process.env.BROWSERSTACK_ACCESS_KEY || 'zHvApcqRsLJNXCzQDWNU'
    },
    tryTo: {
      enabled: true
    },
    retryTo: {
      enabled: true,
      pollInterval: 500,
    },
    retryFailedStep: {
      enabled: true,
      retries: 2
    },
    allure: {
      enabled: true,
      require: '@codeceptjs/allure-legacy',
      outputDir: ALLURE_REPORT_PATH
    }
  },
  // tests: './test/acceptance/tests/**/*_test.js',
  name: 'tumi-special-market'
}