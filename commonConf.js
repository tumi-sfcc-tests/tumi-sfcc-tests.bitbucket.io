var date = new Date();
var currentDate = date.getDate() + "_"
    + (date.getMonth() + 1) + "_"
    + date.getFullYear() + "@"
    + date.getHours() + "_"
    + date.getMinutes() + "_"
    + date.getSeconds();

module.exports = {
    allureReportDir() {
        return './test/acceptance/allure-results/allure_result_' + currentDate;
    },

    allureReportDirProd() {
        return './test/prod/allure-results/prod_report_' + currentDate;
    },

    allureReportDirSpecialMarket() {
        return './test/specialMarket/allure-results/special_market_report_' + currentDate;
    },

    caps_macOSX_latest: {
        "os": "OS X",
        "os_version": "Sequoia",
        "browser_version": "latest",
        "browserstack.local": "false",
        // "browserstack.geoLocation": "US",
        // "browserstack.networkLogs": "true",
        "browserstack.idleTimeout": "300"
    },

    android_chrome: {
        "osVersion": "12.0",
        "deviceName": "Samsung Galaxy S22",
        "local": "false",
        "userName": "taushifahamad_QhjVH3",
        "accessKey": "zHvApcqRsLJNXCzQDWNU",
    },

    caps_win11_chrome_latest: {
        "os": "Windows",
        "osVersion": "11",
        "browserVersion": "latest",
        "browserstack.local": "false",
        // "browserstack.networkLogs": "true",
        "browserstack.idleTimeout": "300"
    },

    'mobile_iPhone14_safari16': {
        "osVersion": "16",
        "deviceName": "iPhone 14",
        "local": "false",
        "userName": "taushifahamad_QhjVH3",
        "accessKey": "zHvApcqRsLJNXCzQDWNU",
    },

    macVentura_chrome_latest_for_prod: {
        "os": "OS X",
        "osVersion": "Ventura",
        "browserVersion": "latest",
        "browserstack.local": "false",
        "browserstack.networkLogs": "true",
        "browserstack.idleTimeout": "300",
        "browserstack.geoLocation": "US",
        "chromeOptions": {
            "args": ['--user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 13_0_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36 TumiCustomUA']
        }
    },

    macVentura_safari_latest_for_prod: {
        "os": "OS X",
        "osVersion": "Ventura",
        "browserVersion": "latest",
        "local": "false",
        "browserstack.local": "false",
        "browserstack.networkLogs": "true",
        "browserstack.idleTimeout": "300",
        "browserstack.geoLocation": "US",
        "alwaysMatch": {
            "safari.useragent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/16.4 Safari/605.1.15 TumiCustomUA"
        }
    },

    yottaaWhitelistURL: "/?yottaa-internal-passthru",


    environment: {
        devUSURL: "https://dev-us.tumi.com/?yottaa-internal-passthru",
        stageUSURL: "https://stage-us.tumi.com/?yottaa-internal-passthru",
        devCAURL: "https://dev-ca.tumi.com/?yottaa-internal-passthru",
        stageCAURL: "https://stage-ca.tumi.com/?yottaa-internal-passthru",
        prodUSURL: "https://www.tumi.com/?no-geo",
        prodCAURL: "https://ca.tumi.com/"
    },

    include: {
        data: "./test/acceptance/testDataUS.js",
        utilities: "./test/utils/utilities.js",
        emailUtils: "./test/utils/emailUtils.js",
        loginPage: "./test/acceptance/pages/login.page.js",
        homepage: "./test/acceptance/pages/home.page.js",
        cartPage: "./test/acceptance/pages/cart.page.js",
        checkoutPage: "./test/acceptance/pages/checkout.page.js",
        productDetailsPage: "./test/acceptance/pages/productDetails.page.js",
        embroideryPage: "./test/acceptance/pages/embroidery.page.js",
        productListPage: "./test/acceptance/pages/productList.page.js",
        searchListPage: "./test/acceptance/pages/searchList.page.js",
        saveForLaterPage: "./test/acceptance/pages/saveForLater.page.js",
        faqPage: "./test/acceptance/pages/staticPage/faq.page.js",
        termsPage: "./test/acceptance/pages/staticPage/termAndCon.page.js",
        corpContactPage: "./test/acceptance/pages/staticPage/corpContact.page.js",
        shippingPage: "./test/acceptance/pages/staticPage/shipping.page.js",
        returnsPage: "./test/acceptance/pages/staticPage/returns.page.js",
        productWarranty: "./test/acceptance/pages/staticPage/productWarranty.page.js",
        repairCenterPage: "./test/acceptance/pages/staticPage/repairCenter.page.js",
        serviceAndRepair: "./test/acceptance/pages/staticPage/serviceAndRepair.page.js",
        privacyPolicyPage: "./test/acceptance/pages/staticPage/privacyPolicy.page.js",
        accountPage: "./test/acceptance/pages/account.page.js",
        webAccessibilityPage: "./test/acceptance/pages/staticPage/webAccessibility.page.js",
        modernSlaveryPage: "./test/acceptance/pages/staticPage/modernSlavery.page.js",
        transparencySupply: "./test/acceptance/pages/staticPage/transparencySupply.page.js",
        tumiLockPage: "./test/acceptance/pages/staticPage/tumiLock.page.js",
        orderConfPage: "./test/acceptance/pages/orderConfirmation.page.js",
        giftCardPage: "./test/acceptance/pages/giftCard.page.js",
        myPurchasePage: "./test/acceptance/pages/myPurchase.page.js",
        //Prod Pages
        tumiPage: "./test/prod/pages/tumi.page.js",
        //Special Market Pages
        testDataSpecialMarket: "./test/specialMarket/testDataSpecialMarket.js",
        inquiryFormPage: "./test/specialMarket/pages/inquiryForm.page.js",
        commonPage: "./test/specialMarket/pages/common.page.js"
    },

    gherkin_steps: [
        "./test/acceptance/steps/loginPage/createAccount.steps.js",
        "./test/acceptance/steps/loginPage/loginUser.steps.js",
        "./test/acceptance/steps/loginPage/forgotPassword.steps.js",
        "./test/acceptance/steps/checkoutPage/checkoutPayment.steps.js",
        "./test/acceptance/steps/utilities.steps.js",
        "./test/acceptance/steps/homepage/footer.steps.js",
        "./test/acceptance/steps/homepage/homepage.steps.js",
        "./test/acceptance/steps/cartPage/cartFlyout.steps.js",
        "./test/acceptance/steps/cartPage/cartPage.steps.js",
        "./test/acceptance/steps/productDetailsPage/productDetails.steps.js",
        "./test/acceptance/steps/productDetailsPage/pdpComponents.steps.js",
        "./test/acceptance/steps/productDetailsPage/accents.steps.js",
        "./test/acceptance/steps/productDetailsPage/monogram.steps.js",
        "./test/acceptance/steps/productDetailsPage/saveForLater.steps.js",
        "./test/acceptance/steps/productDetailsPage/embroidery.steps.js",
        "./test/acceptance/steps/homepage/header.steps.js",
        "./test/acceptance/steps/accountPage/savedAddress.steps.js",
        "./test/acceptance/steps/accountPage/savedPayment.steps.js",
        "./test/acceptance/steps/accountPage/myAccount.steps.js",
        "./test/acceptance/steps/accountPage/mySettings.steps.js",
        "./test/acceptance/steps/staticPage/faq.steps.js",
        "./test/acceptance/steps/staticPage/termAndCon.steps.js",
        "./test/acceptance/steps/staticPage/corpContact.steps.js",
        "./test/acceptance/steps/staticPage/shipping.steps.js",
        "./test/acceptance/steps/staticPage/returns.steps.js",
        "./test/acceptance/steps/staticPage/repairCenter.steps.js",
        "./test/acceptance/steps/staticPage/productWarranty.steps.js",
        "./test/acceptance/steps/staticPage/privacyPolicy.steps.js",
        "./test/acceptance/steps/staticPage/webAccessibility.steps.js",
        "./test/acceptance/steps/staticPage/modernSlavery.steps.js",
        "./test/acceptance/steps/staticPage/transparencySupply.steps.js",
        "./test/acceptance/steps/staticPage/tumiLock.steps.js",
        "./test/acceptance/steps/staticPage/_staticPage.steps.js",
        "./test/acceptance/steps/productListPage/productList.steps.js",
        "./test/acceptance/steps/productListPage/searchList.steps.js",
        "./test/acceptance/steps/productDetailsPage/giftCard.steps.js",
        "./test/acceptance/steps/checkoutPage/checkoutShipping.steps.js",
        "./test/acceptance/steps/checkoutPage/orderConfirmation.steps.js",
        //Prod steps
        "./test/acceptance/steps/accountPage/myPurchase.steps.js",
        "./test/prod/steps/prodPDPs.steps.js",
        "./test/prod/steps/categoryPLPs.steps.js",
        //Special Market steps
        "./test/specialMarket/steps/inquiryForm.steps.js",
        "./test/specialMarket/steps/common.steps.js"
    ]

}
